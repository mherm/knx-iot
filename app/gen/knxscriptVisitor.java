// Generated from C:/Users/Manu/AndroidStudioProjects/RoomDetect/app/src/main/antlr\knxscript.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link knxscriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface knxscriptVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(knxscriptParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHeader(knxscriptParser.HeaderContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#title}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTitle(knxscriptParser.TitleContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#description}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescription(knxscriptParser.DescriptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#activation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActivation(knxscriptParser.ActivationContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#active_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActive_block(knxscriptParser.Active_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilter(knxscriptParser.FilterContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#environment_filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnvironment_filter(knxscriptParser.Environment_filterContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#timerange_filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimerange_filter(knxscriptParser.Timerange_filterContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRule(knxscriptParser.RuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#trigger_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrigger_block(knxscriptParser.Trigger_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrigger(knxscriptParser.TriggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#enter_room_trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnter_room_trigger(knxscriptParser.Enter_room_triggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#room_id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoom_id(knxscriptParser.Room_idContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#leave_room_trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeave_room_trigger(knxscriptParser.Leave_room_triggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#alert_trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlert_trigger(knxscriptParser.Alert_triggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#statement_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement_block(knxscriptParser.Statement_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(knxscriptParser.StmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#simple_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimple_stmt(knxscriptParser.Simple_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#current_room_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurrent_room_stmt(knxscriptParser.Current_room_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#wait_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWait_stmt(knxscriptParser.Wait_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#notify_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotify_stmt(knxscriptParser.Notify_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#http_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHttp_stmt(knxscriptParser.Http_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#method}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod(knxscriptParser.MethodContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#http_content}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHttp_content(knxscriptParser.Http_contentContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#http_header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHttp_header(knxscriptParser.Http_headerContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(knxscriptParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#parallel_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParallel_block(knxscriptParser.Parallel_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#if_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stmt(knxscriptParser.If_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(knxscriptParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#ask_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsk_stmt(knxscriptParser.Ask_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#switch_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_block(knxscriptParser.Switch_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#switch_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_option(knxscriptParser.Switch_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp(knxscriptParser.OpContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#knx_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKnx_stmt(knxscriptParser.Knx_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#knx_read_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKnx_read_stmt(knxscriptParser.Knx_read_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link knxscriptParser#kmx_write_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKmx_write_stmt(knxscriptParser.Kmx_write_stmtContext ctx);
}