// Generated from C:/Users/Manu/AndroidStudioProjects/RoomDetect/app/src/main/antlr\KnxScript.g4 by ANTLR 4.5.3
package de.hsrm.knx.knxbrowser.script.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link KnxScriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface KnxScriptVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(KnxScriptParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHeader(KnxScriptParser.HeaderContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#title}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTitle(KnxScriptParser.TitleContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#description}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescription(KnxScriptParser.DescriptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#activation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActivation(KnxScriptParser.ActivationContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#active_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActive_block(KnxScriptParser.Active_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilter(KnxScriptParser.FilterContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#environment_filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnvironment_filter(KnxScriptParser.Environment_filterContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#timerange_filter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimerange_filter(KnxScriptParser.Timerange_filterContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRule(KnxScriptParser.RuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#trigger_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrigger_block(KnxScriptParser.Trigger_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrigger(KnxScriptParser.TriggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#enter_room_trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnter_room_trigger(KnxScriptParser.Enter_room_triggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#room_id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoom_id(KnxScriptParser.Room_idContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#leave_room_trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeave_room_trigger(KnxScriptParser.Leave_room_triggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#alert_trigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlert_trigger(KnxScriptParser.Alert_triggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#statement_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement_block(KnxScriptParser.Statement_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(KnxScriptParser.StmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#simple_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimple_stmt(KnxScriptParser.Simple_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#current_room_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurrent_room_stmt(KnxScriptParser.Current_room_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#wait_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWait_stmt(KnxScriptParser.Wait_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#notify_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotify_stmt(KnxScriptParser.Notify_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#http_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHttp_stmt(KnxScriptParser.Http_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#method}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod(KnxScriptParser.MethodContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#http_content}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHttp_content(KnxScriptParser.Http_contentContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#http_header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHttp_header(KnxScriptParser.Http_headerContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(KnxScriptParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#parallel_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParallel_block(KnxScriptParser.Parallel_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#if_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stmt(KnxScriptParser.If_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(KnxScriptParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#ask_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsk_stmt(KnxScriptParser.Ask_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#switch_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_block(KnxScriptParser.Switch_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#switch_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_option(KnxScriptParser.Switch_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp(KnxScriptParser.OpContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#knx_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKnx_stmt(KnxScriptParser.Knx_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#knx_read_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKnx_read_stmt(KnxScriptParser.Knx_read_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link KnxScriptParser#kmx_write_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKmx_write_stmt(KnxScriptParser.Kmx_write_stmtContext ctx);
}