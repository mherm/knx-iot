// Generated from C:/Users/Manu/AndroidStudioProjects/RoomDetect/app/src/main/antlr\KnxScript.g4 by ANTLR 4.5.3
package de.hsrm.knx.knxbrowser.script.grammar;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class KnxScriptLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, TIME=27, URL=28, DTP=29, BLOCK_START=30, BLOCK_END=31, 
		WS=32, ENV_ID_TYPE=33, STRING=34, INTEGER=35, REAL=36, BOOL=37, GE=38, 
		EQ=39, LE=40, NEQ=41, GT=42, LT=43, NEWLINE=44, ALPHANUM=45, ID=46;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24", 
		"T__25", "TIME", "URL", "DTP", "BLOCK_START", "BLOCK_END", "WS", "ENV_ID_TYPE", 
		"NAME", "SSID", "ANY", "NUM", "DOT", "COLON", "MINUS", "STRING", "INTEGER", 
		"REAL", "BOOL", "TRUE", "FALSE", "GE", "EQ", "LE", "NEQ", "GT", "LT", 
		"COMMENT", "NEWLINE", "ALPHANUM", "ID"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'script'", "'description'", "'active'", "'environment'", "'time between'", 
		"'on'", "'do'", "'enter room '", "'leave room'", "'time'", "'current room'", 
		"'wait'", "'notify '", "'http'", "'header='", "'get'", "'put'", "'post'", 
		"'delete'", "'parallel'", "'if '", "'else '", "'ask'", "':'", "'read '", 
		"'write '", null, null, null, null, null, null, null, null, null, null, 
		null, "'>='", "'='", "'<='", "'!='", "'>'", "'<'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, "TIME", "URL", "DTP", "BLOCK_START", "BLOCK_END", "WS", 
		"ENV_ID_TYPE", "STRING", "INTEGER", "REAL", "BOOL", "GE", "EQ", "LE", 
		"NEQ", "GT", "LT", "NEWLINE", "ALPHANUM", "ID"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public KnxScriptLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "KnxScript.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 40:
			STRING_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void STRING_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:

			     String s = getText();
			     s = s.substring(1, s.length() - 1); // strip the leading and trailing quotes
			     s = s.replace("\"\"", "\""); // replace all double quotes with single quotes
			     setText(s);
			   
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\60\u01d9\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23"+
		"\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30"+
		"\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\5\35\u0138\n\35\3\35\3\35\3\35\3\35\3\35\3\35\6\35"+
		"\u0140\n\35\r\35\16\35\u0141\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3"+
		"\36\3\36\3\36\3\36\3\36\3\36\3\37\5\37\u0153\n\37\3\37\3\37\7\37\u0157"+
		"\n\37\f\37\16\37\u015a\13\37\3\37\3\37\3 \3 \7 \u0160\n \f \16 \u0163"+
		"\13 \3 \5 \u0166\n \3!\6!\u0169\n!\r!\16!\u016a\3!\3!\3\"\3\"\5\"\u0171"+
		"\n\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3%\3%\3%\3%\3&\3&\3\'\3\'\3(\3(\3)"+
		"\3)\3*\3*\3*\3*\7*\u018d\n*\f*\16*\u0190\13*\3*\3*\3*\3+\6+\u0196\n+\r"+
		"+\16+\u0197\3,\3,\3,\3,\3-\3-\5-\u01a0\n-\3.\3.\3.\3.\3.\3/\3/\3/\3/\3"+
		"/\3/\3\60\3\60\3\60\3\61\3\61\3\62\3\62\3\62\3\63\3\63\3\63\3\64\3\64"+
		"\3\65\3\65\3\66\3\66\7\66\u01be\n\66\f\66\16\66\u01c1\13\66\3\66\5\66"+
		"\u01c4\n\66\3\66\3\66\3\67\3\67\5\67\u01ca\n\67\3\67\6\67\u01cd\n\67\r"+
		"\67\16\67\u01ce\38\38\58\u01d3\n8\39\69\u01d6\n9\r9\169\u01d7\3\u01bf"+
		"\2:\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36"+
		";\37= ?!A\"C#E\2G\2I\2K\2M\2O\2Q\2S$U%W&Y\'[\2]\2_(a)c*e+g,i-k\2m.o/q"+
		"\60\3\2\13\3\2\62\64\3\2\62\67\4\2\'\'\61\61\4\2..aa\5\2\13\13\16\16\""+
		"\"\3\2\62;\4\2$$^^\4\2\f\f\17\17\4\2C\\c|\u01e6\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2"+
		"\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2"+
		"m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\3s\3\2\2\2\5z\3\2\2\2\7\u0086\3\2\2\2"+
		"\t\u008d\3\2\2\2\13\u0099\3\2\2\2\r\u00a6\3\2\2\2\17\u00a9\3\2\2\2\21"+
		"\u00ac\3\2\2\2\23\u00b8\3\2\2\2\25\u00c3\3\2\2\2\27\u00c8\3\2\2\2\31\u00d5"+
		"\3\2\2\2\33\u00da\3\2\2\2\35\u00e2\3\2\2\2\37\u00e7\3\2\2\2!\u00ef\3\2"+
		"\2\2#\u00f3\3\2\2\2%\u00f7\3\2\2\2\'\u00fc\3\2\2\2)\u0103\3\2\2\2+\u010c"+
		"\3\2\2\2-\u0110\3\2\2\2/\u0116\3\2\2\2\61\u011a\3\2\2\2\63\u011c\3\2\2"+
		"\2\65\u0122\3\2\2\2\67\u0129\3\2\2\29\u0137\3\2\2\2;\u0143\3\2\2\2=\u0152"+
		"\3\2\2\2?\u015d\3\2\2\2A\u0168\3\2\2\2C\u0170\3\2\2\2E\u0172\3\2\2\2G"+
		"\u0177\3\2\2\2I\u017c\3\2\2\2K\u0180\3\2\2\2M\u0182\3\2\2\2O\u0184\3\2"+
		"\2\2Q\u0186\3\2\2\2S\u0188\3\2\2\2U\u0195\3\2\2\2W\u0199\3\2\2\2Y\u019f"+
		"\3\2\2\2[\u01a1\3\2\2\2]\u01a6\3\2\2\2_\u01ac\3\2\2\2a\u01af\3\2\2\2c"+
		"\u01b1\3\2\2\2e\u01b4\3\2\2\2g\u01b7\3\2\2\2i\u01b9\3\2\2\2k\u01bb\3\2"+
		"\2\2m\u01cc\3\2\2\2o\u01d2\3\2\2\2q\u01d5\3\2\2\2st\7u\2\2tu\7e\2\2uv"+
		"\7t\2\2vw\7k\2\2wx\7r\2\2xy\7v\2\2y\4\3\2\2\2z{\7f\2\2{|\7g\2\2|}\7u\2"+
		"\2}~\7e\2\2~\177\7t\2\2\177\u0080\7k\2\2\u0080\u0081\7r\2\2\u0081\u0082"+
		"\7v\2\2\u0082\u0083\7k\2\2\u0083\u0084\7q\2\2\u0084\u0085\7p\2\2\u0085"+
		"\6\3\2\2\2\u0086\u0087\7c\2\2\u0087\u0088\7e\2\2\u0088\u0089\7v\2\2\u0089"+
		"\u008a\7k\2\2\u008a\u008b\7x\2\2\u008b\u008c\7g\2\2\u008c\b\3\2\2\2\u008d"+
		"\u008e\7g\2\2\u008e\u008f\7p\2\2\u008f\u0090\7x\2\2\u0090\u0091\7k\2\2"+
		"\u0091\u0092\7t\2\2\u0092\u0093\7q\2\2\u0093\u0094\7p\2\2\u0094\u0095"+
		"\7o\2\2\u0095\u0096\7g\2\2\u0096\u0097\7p\2\2\u0097\u0098\7v\2\2\u0098"+
		"\n\3\2\2\2\u0099\u009a\7v\2\2\u009a\u009b\7k\2\2\u009b\u009c\7o\2\2\u009c"+
		"\u009d\7g\2\2\u009d\u009e\7\"\2\2\u009e\u009f\7d\2\2\u009f\u00a0\7g\2"+
		"\2\u00a0\u00a1\7v\2\2\u00a1\u00a2\7y\2\2\u00a2\u00a3\7g\2\2\u00a3\u00a4"+
		"\7g\2\2\u00a4\u00a5\7p\2\2\u00a5\f\3\2\2\2\u00a6\u00a7\7q\2\2\u00a7\u00a8"+
		"\7p\2\2\u00a8\16\3\2\2\2\u00a9\u00aa\7f\2\2\u00aa\u00ab\7q\2\2\u00ab\20"+
		"\3\2\2\2\u00ac\u00ad\7g\2\2\u00ad\u00ae\7p\2\2\u00ae\u00af\7v\2\2\u00af"+
		"\u00b0\7g\2\2\u00b0\u00b1\7t\2\2\u00b1\u00b2\7\"\2\2\u00b2\u00b3\7t\2"+
		"\2\u00b3\u00b4\7q\2\2\u00b4\u00b5\7q\2\2\u00b5\u00b6\7o\2\2\u00b6\u00b7"+
		"\7\"\2\2\u00b7\22\3\2\2\2\u00b8\u00b9\7n\2\2\u00b9\u00ba\7g\2\2\u00ba"+
		"\u00bb\7c\2\2\u00bb\u00bc\7x\2\2\u00bc\u00bd\7g\2\2\u00bd\u00be\7\"\2"+
		"\2\u00be\u00bf\7t\2\2\u00bf\u00c0\7q\2\2\u00c0\u00c1\7q\2\2\u00c1\u00c2"+
		"\7o\2\2\u00c2\24\3\2\2\2\u00c3\u00c4\7v\2\2\u00c4\u00c5\7k\2\2\u00c5\u00c6"+
		"\7o\2\2\u00c6\u00c7\7g\2\2\u00c7\26\3\2\2\2\u00c8\u00c9\7e\2\2\u00c9\u00ca"+
		"\7w\2\2\u00ca\u00cb\7t\2\2\u00cb\u00cc\7t\2\2\u00cc\u00cd\7g\2\2\u00cd"+
		"\u00ce\7p\2\2\u00ce\u00cf\7v\2\2\u00cf\u00d0\7\"\2\2\u00d0\u00d1\7t\2"+
		"\2\u00d1\u00d2\7q\2\2\u00d2\u00d3\7q\2\2\u00d3\u00d4\7o\2\2\u00d4\30\3"+
		"\2\2\2\u00d5\u00d6\7y\2\2\u00d6\u00d7\7c\2\2\u00d7\u00d8\7k\2\2\u00d8"+
		"\u00d9\7v\2\2\u00d9\32\3\2\2\2\u00da\u00db\7p\2\2\u00db\u00dc\7q\2\2\u00dc"+
		"\u00dd\7v\2\2\u00dd\u00de\7k\2\2\u00de\u00df\7h\2\2\u00df\u00e0\7{\2\2"+
		"\u00e0\u00e1\7\"\2\2\u00e1\34\3\2\2\2\u00e2\u00e3\7j\2\2\u00e3\u00e4\7"+
		"v\2\2\u00e4\u00e5\7v\2\2\u00e5\u00e6\7r\2\2\u00e6\36\3\2\2\2\u00e7\u00e8"+
		"\7j\2\2\u00e8\u00e9\7g\2\2\u00e9\u00ea\7c\2\2\u00ea\u00eb\7f\2\2\u00eb"+
		"\u00ec\7g\2\2\u00ec\u00ed\7t\2\2\u00ed\u00ee\7?\2\2\u00ee \3\2\2\2\u00ef"+
		"\u00f0\7i\2\2\u00f0\u00f1\7g\2\2\u00f1\u00f2\7v\2\2\u00f2\"\3\2\2\2\u00f3"+
		"\u00f4\7r\2\2\u00f4\u00f5\7w\2\2\u00f5\u00f6\7v\2\2\u00f6$\3\2\2\2\u00f7"+
		"\u00f8\7r\2\2\u00f8\u00f9\7q\2\2\u00f9\u00fa\7u\2\2\u00fa\u00fb\7v\2\2"+
		"\u00fb&\3\2\2\2\u00fc\u00fd\7f\2\2\u00fd\u00fe\7g\2\2\u00fe\u00ff\7n\2"+
		"\2\u00ff\u0100\7g\2\2\u0100\u0101\7v\2\2\u0101\u0102\7g\2\2\u0102(\3\2"+
		"\2\2\u0103\u0104\7r\2\2\u0104\u0105\7c\2\2\u0105\u0106\7t\2\2\u0106\u0107"+
		"\7c\2\2\u0107\u0108\7n\2\2\u0108\u0109\7n\2\2\u0109\u010a\7g\2\2\u010a"+
		"\u010b\7n\2\2\u010b*\3\2\2\2\u010c\u010d\7k\2\2\u010d\u010e\7h\2\2\u010e"+
		"\u010f\7\"\2\2\u010f,\3\2\2\2\u0110\u0111\7g\2\2\u0111\u0112\7n\2\2\u0112"+
		"\u0113\7u\2\2\u0113\u0114\7g\2\2\u0114\u0115\7\"\2\2\u0115.\3\2\2\2\u0116"+
		"\u0117\7c\2\2\u0117\u0118\7u\2\2\u0118\u0119\7m\2\2\u0119\60\3\2\2\2\u011a"+
		"\u011b\7<\2\2\u011b\62\3\2\2\2\u011c\u011d\7t\2\2\u011d\u011e\7g\2\2\u011e"+
		"\u011f\7c\2\2\u011f\u0120\7f\2\2\u0120\u0121\7\"\2\2\u0121\64\3\2\2\2"+
		"\u0122\u0123\7y\2\2\u0123\u0124\7t\2\2\u0124\u0125\7k\2\2\u0125\u0126"+
		"\7v\2\2\u0126\u0127\7g\2\2\u0127\u0128\7\"\2\2\u0128\66\3\2\2\2\u0129"+
		"\u012a\t\2\2\2\u012a\u012b\5K&\2\u012b\u012c\7<\2\2\u012c\u012d\t\3\2"+
		"\2\u012d\u012e\5K&\2\u012e8\3\2\2\2\u012f\u0130\7j\2\2\u0130\u0131\7v"+
		"\2\2\u0131\u0132\7v\2\2\u0132\u0133\7r\2\2\u0133\u0134\7<\2\2\u0134\u0135"+
		"\7\61\2\2\u0135\u0138\7\61\2\2\u0136\u0138\7\61\2\2\u0137\u012f\3\2\2"+
		"\2\u0137\u0136\3\2\2\2\u0138\u013f\3\2\2\2\u0139\u0140\5o8\2\u013a\u0140"+
		"\5M\'\2\u013b\u0140\t\4\2\2\u013c\u0140\5Q)\2\u013d\u0140\5O(\2\u013e"+
		"\u0140\t\5\2\2\u013f\u0139\3\2\2\2\u013f\u013a\3\2\2\2\u013f\u013b\3\2"+
		"\2\2\u013f\u013c\3\2\2\2\u013f\u013d\3\2\2\2\u013f\u013e\3\2\2\2\u0140"+
		"\u0141\3\2\2\2\u0141\u013f\3\2\2\2\u0141\u0142\3\2\2\2\u0142:\3\2\2\2"+
		"\u0143\u0144\7m\2\2\u0144\u0145\7p\2\2\u0145\u0146\7z\2\2\u0146\u0147"+
		"\7<\2\2\u0147\u0148\7F\2\2\u0148\u0149\7R\2\2\u0149\u014a\7U\2\2\u014a"+
		"\u014b\7V\2\2\u014b\u014c\7/\2\2\u014c\u014d\3\2\2\2\u014d\u014e\5K&\2"+
		"\u014e\u014f\5Q)\2\u014f\u0150\5U+\2\u0150<\3\2\2\2\u0151\u0153\5m\67"+
		"\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0158"+
		"\7}\2\2\u0155\u0157\5A!\2\u0156\u0155\3\2\2\2\u0157\u015a\3\2\2\2\u0158"+
		"\u0156\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015b\3\2\2\2\u015a\u0158\3\2"+
		"\2\2\u015b\u015c\5m\67\2\u015c>\3\2\2\2\u015d\u0161\7\177\2\2\u015e\u0160"+
		"\5A!\2\u015f\u015e\3\2\2\2\u0160\u0163\3\2\2\2\u0161\u015f\3\2\2\2\u0161"+
		"\u0162\3\2\2\2\u0162\u0165\3\2\2\2\u0163\u0161\3\2\2\2\u0164\u0166\5m"+
		"\67\2\u0165\u0164\3\2\2\2\u0165\u0166\3\2\2\2\u0166@\3\2\2\2\u0167\u0169"+
		"\t\6\2\2\u0168\u0167\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u0168\3\2\2\2\u016a"+
		"\u016b\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016d\b!\2\2\u016dB\3\2\2\2\u016e"+
		"\u0171\5E#\2\u016f\u0171\5G$\2\u0170\u016e\3\2\2\2\u0170\u016f\3\2\2\2"+
		"\u0171D\3\2\2\2\u0172\u0173\7p\2\2\u0173\u0174\7c\2\2\u0174\u0175\7o\2"+
		"\2\u0175\u0176\7g\2\2\u0176F\3\2\2\2\u0177\u0178\7u\2\2\u0178\u0179\7"+
		"u\2\2\u0179\u017a\7k\2\2\u017a\u017b\7f\2\2\u017bH\3\2\2\2\u017c\u017d"+
		"\7c\2\2\u017d\u017e\7p\2\2\u017e\u017f\7{\2\2\u017fJ\3\2\2\2\u0180\u0181"+
		"\t\7\2\2\u0181L\3\2\2\2\u0182\u0183\7\60\2\2\u0183N\3\2\2\2\u0184\u0185"+
		"\7<\2\2\u0185P\3\2\2\2\u0186\u0187\7/\2\2\u0187R\3\2\2\2\u0188\u018e\7"+
		"$\2\2\u0189\u018d\n\b\2\2\u018a\u018b\7^\2\2\u018b\u018d\13\2\2\2\u018c"+
		"\u0189\3\2\2\2\u018c\u018a\3\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2"+
		"\2\2\u018e\u018f\3\2\2\2\u018f\u0191\3\2\2\2\u0190\u018e\3\2\2\2\u0191"+
		"\u0192\7$\2\2\u0192\u0193\b*\3\2\u0193T\3\2\2\2\u0194\u0196\5K&\2\u0195"+
		"\u0194\3\2\2\2\u0196\u0197\3\2\2\2\u0197\u0195\3\2\2\2\u0197\u0198\3\2"+
		"\2\2\u0198V\3\2\2\2\u0199\u019a\5U+\2\u019a\u019b\7\60\2\2\u019b\u019c"+
		"\5U+\2\u019cX\3\2\2\2\u019d\u01a0\5[.\2\u019e\u01a0\5]/\2\u019f\u019d"+
		"\3\2\2\2\u019f\u019e\3\2\2\2\u01a0Z\3\2\2\2\u01a1\u01a2\7v\2\2\u01a2\u01a3"+
		"\7t\2\2\u01a3\u01a4\7w\2\2\u01a4\u01a5\7g\2\2\u01a5\\\3\2\2\2\u01a6\u01a7"+
		"\7h\2\2\u01a7\u01a8\7c\2\2\u01a8\u01a9\7n\2\2\u01a9\u01aa\7u\2\2\u01aa"+
		"\u01ab\7g\2\2\u01ab^\3\2\2\2\u01ac\u01ad\7@\2\2\u01ad\u01ae\7?\2\2\u01ae"+
		"`\3\2\2\2\u01af\u01b0\7?\2\2\u01b0b\3\2\2\2\u01b1\u01b2\7>\2\2\u01b2\u01b3"+
		"\7?\2\2\u01b3d\3\2\2\2\u01b4\u01b5\7#\2\2\u01b5\u01b6\7?\2\2\u01b6f\3"+
		"\2\2\2\u01b7\u01b8\7@\2\2\u01b8h\3\2\2\2\u01b9\u01ba\7>\2\2\u01baj\3\2"+
		"\2\2\u01bb\u01bf\7%\2\2\u01bc\u01be\n\t\2\2\u01bd\u01bc\3\2\2\2\u01be"+
		"\u01c1\3\2\2\2\u01bf\u01c0\3\2\2\2\u01bf\u01bd\3\2\2\2\u01c0\u01c3\3\2"+
		"\2\2\u01c1\u01bf\3\2\2\2\u01c2\u01c4\7\17\2\2\u01c3\u01c2\3\2\2\2\u01c3"+
		"\u01c4\3\2\2\2\u01c4\u01c5\3\2\2\2\u01c5\u01c6\7\f\2\2\u01c6l\3\2\2\2"+
		"\u01c7\u01cd\5k\66\2\u01c8\u01ca\7\17\2\2\u01c9\u01c8\3\2\2\2\u01c9\u01ca"+
		"\3\2\2\2\u01ca\u01cb\3\2\2\2\u01cb\u01cd\7\f\2\2\u01cc\u01c7\3\2\2\2\u01cc"+
		"\u01c9\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce\u01cc\3\2\2\2\u01ce\u01cf\3\2"+
		"\2\2\u01cfn\3\2\2\2\u01d0\u01d3\t\n\2\2\u01d1\u01d3\5K&\2\u01d2\u01d0"+
		"\3\2\2\2\u01d2\u01d1\3\2\2\2\u01d3p\3\2\2\2\u01d4\u01d6\5o8\2\u01d5\u01d4"+
		"\3\2\2\2\u01d6\u01d7\3\2\2\2\u01d7\u01d5\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8"+
		"r\3\2\2\2\27\2\u0137\u013f\u0141\u0152\u0158\u0161\u0165\u016a\u0170\u018c"+
		"\u018e\u0197\u019f\u01bf\u01c3\u01c9\u01cc\u01ce\u01d2\u01d7\4\b\2\2\3"+
		"*\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}