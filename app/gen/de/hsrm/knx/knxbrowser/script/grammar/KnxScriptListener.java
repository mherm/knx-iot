// Generated from C:/Users/Manu/AndroidStudioProjects/RoomDetect/app/src/main/antlr\KnxScript.g4 by ANTLR 4.5.3
package de.hsrm.knx.knxbrowser.script.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link KnxScriptParser}.
 */
public interface KnxScriptListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(KnxScriptParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(KnxScriptParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#header}.
	 * @param ctx the parse tree
	 */
	void enterHeader(KnxScriptParser.HeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#header}.
	 * @param ctx the parse tree
	 */
	void exitHeader(KnxScriptParser.HeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#title}.
	 * @param ctx the parse tree
	 */
	void enterTitle(KnxScriptParser.TitleContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#title}.
	 * @param ctx the parse tree
	 */
	void exitTitle(KnxScriptParser.TitleContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#description}.
	 * @param ctx the parse tree
	 */
	void enterDescription(KnxScriptParser.DescriptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#description}.
	 * @param ctx the parse tree
	 */
	void exitDescription(KnxScriptParser.DescriptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#activation}.
	 * @param ctx the parse tree
	 */
	void enterActivation(KnxScriptParser.ActivationContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#activation}.
	 * @param ctx the parse tree
	 */
	void exitActivation(KnxScriptParser.ActivationContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#active_block}.
	 * @param ctx the parse tree
	 */
	void enterActive_block(KnxScriptParser.Active_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#active_block}.
	 * @param ctx the parse tree
	 */
	void exitActive_block(KnxScriptParser.Active_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#filter}.
	 * @param ctx the parse tree
	 */
	void enterFilter(KnxScriptParser.FilterContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#filter}.
	 * @param ctx the parse tree
	 */
	void exitFilter(KnxScriptParser.FilterContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#environment_filter}.
	 * @param ctx the parse tree
	 */
	void enterEnvironment_filter(KnxScriptParser.Environment_filterContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#environment_filter}.
	 * @param ctx the parse tree
	 */
	void exitEnvironment_filter(KnxScriptParser.Environment_filterContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#timerange_filter}.
	 * @param ctx the parse tree
	 */
	void enterTimerange_filter(KnxScriptParser.Timerange_filterContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#timerange_filter}.
	 * @param ctx the parse tree
	 */
	void exitTimerange_filter(KnxScriptParser.Timerange_filterContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#rule}.
	 * @param ctx the parse tree
	 */
	void enterRule(KnxScriptParser.RuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#rule}.
	 * @param ctx the parse tree
	 */
	void exitRule(KnxScriptParser.RuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#trigger_block}.
	 * @param ctx the parse tree
	 */
	void enterTrigger_block(KnxScriptParser.Trigger_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#trigger_block}.
	 * @param ctx the parse tree
	 */
	void exitTrigger_block(KnxScriptParser.Trigger_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#trigger}.
	 * @param ctx the parse tree
	 */
	void enterTrigger(KnxScriptParser.TriggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#trigger}.
	 * @param ctx the parse tree
	 */
	void exitTrigger(KnxScriptParser.TriggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#enter_room_trigger}.
	 * @param ctx the parse tree
	 */
	void enterEnter_room_trigger(KnxScriptParser.Enter_room_triggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#enter_room_trigger}.
	 * @param ctx the parse tree
	 */
	void exitEnter_room_trigger(KnxScriptParser.Enter_room_triggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#room_id}.
	 * @param ctx the parse tree
	 */
	void enterRoom_id(KnxScriptParser.Room_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#room_id}.
	 * @param ctx the parse tree
	 */
	void exitRoom_id(KnxScriptParser.Room_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#leave_room_trigger}.
	 * @param ctx the parse tree
	 */
	void enterLeave_room_trigger(KnxScriptParser.Leave_room_triggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#leave_room_trigger}.
	 * @param ctx the parse tree
	 */
	void exitLeave_room_trigger(KnxScriptParser.Leave_room_triggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#alert_trigger}.
	 * @param ctx the parse tree
	 */
	void enterAlert_trigger(KnxScriptParser.Alert_triggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#alert_trigger}.
	 * @param ctx the parse tree
	 */
	void exitAlert_trigger(KnxScriptParser.Alert_triggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#statement_block}.
	 * @param ctx the parse tree
	 */
	void enterStatement_block(KnxScriptParser.Statement_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#statement_block}.
	 * @param ctx the parse tree
	 */
	void exitStatement_block(KnxScriptParser.Statement_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(KnxScriptParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(KnxScriptParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#simple_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSimple_stmt(KnxScriptParser.Simple_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#simple_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSimple_stmt(KnxScriptParser.Simple_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#current_room_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCurrent_room_stmt(KnxScriptParser.Current_room_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#current_room_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCurrent_room_stmt(KnxScriptParser.Current_room_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#wait_stmt}.
	 * @param ctx the parse tree
	 */
	void enterWait_stmt(KnxScriptParser.Wait_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#wait_stmt}.
	 * @param ctx the parse tree
	 */
	void exitWait_stmt(KnxScriptParser.Wait_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#notify_stmt}.
	 * @param ctx the parse tree
	 */
	void enterNotify_stmt(KnxScriptParser.Notify_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#notify_stmt}.
	 * @param ctx the parse tree
	 */
	void exitNotify_stmt(KnxScriptParser.Notify_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#http_stmt}.
	 * @param ctx the parse tree
	 */
	void enterHttp_stmt(KnxScriptParser.Http_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#http_stmt}.
	 * @param ctx the parse tree
	 */
	void exitHttp_stmt(KnxScriptParser.Http_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#method}.
	 * @param ctx the parse tree
	 */
	void enterMethod(KnxScriptParser.MethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#method}.
	 * @param ctx the parse tree
	 */
	void exitMethod(KnxScriptParser.MethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#http_content}.
	 * @param ctx the parse tree
	 */
	void enterHttp_content(KnxScriptParser.Http_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#http_content}.
	 * @param ctx the parse tree
	 */
	void exitHttp_content(KnxScriptParser.Http_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#http_header}.
	 * @param ctx the parse tree
	 */
	void enterHttp_header(KnxScriptParser.Http_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#http_header}.
	 * @param ctx the parse tree
	 */
	void exitHttp_header(KnxScriptParser.Http_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(KnxScriptParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(KnxScriptParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#parallel_block}.
	 * @param ctx the parse tree
	 */
	void enterParallel_block(KnxScriptParser.Parallel_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#parallel_block}.
	 * @param ctx the parse tree
	 */
	void exitParallel_block(KnxScriptParser.Parallel_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_stmt(KnxScriptParser.If_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_stmt(KnxScriptParser.If_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(KnxScriptParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(KnxScriptParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#ask_stmt}.
	 * @param ctx the parse tree
	 */
	void enterAsk_stmt(KnxScriptParser.Ask_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#ask_stmt}.
	 * @param ctx the parse tree
	 */
	void exitAsk_stmt(KnxScriptParser.Ask_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#switch_block}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_block(KnxScriptParser.Switch_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#switch_block}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_block(KnxScriptParser.Switch_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#switch_option}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_option(KnxScriptParser.Switch_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#switch_option}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_option(KnxScriptParser.Switch_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#op}.
	 * @param ctx the parse tree
	 */
	void enterOp(KnxScriptParser.OpContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#op}.
	 * @param ctx the parse tree
	 */
	void exitOp(KnxScriptParser.OpContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#knx_stmt}.
	 * @param ctx the parse tree
	 */
	void enterKnx_stmt(KnxScriptParser.Knx_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#knx_stmt}.
	 * @param ctx the parse tree
	 */
	void exitKnx_stmt(KnxScriptParser.Knx_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#knx_read_stmt}.
	 * @param ctx the parse tree
	 */
	void enterKnx_read_stmt(KnxScriptParser.Knx_read_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#knx_read_stmt}.
	 * @param ctx the parse tree
	 */
	void exitKnx_read_stmt(KnxScriptParser.Knx_read_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link KnxScriptParser#kmx_write_stmt}.
	 * @param ctx the parse tree
	 */
	void enterKmx_write_stmt(KnxScriptParser.Kmx_write_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link KnxScriptParser#kmx_write_stmt}.
	 * @param ctx the parse tree
	 */
	void exitKmx_write_stmt(KnxScriptParser.Kmx_write_stmtContext ctx);
}