// Generated from C:/Users/Manu/AndroidStudioProjects/RoomDetect/app/src/main/antlr\knxscript.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link knxscriptParser}.
 */
public interface knxscriptListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(knxscriptParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(knxscriptParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#header}.
	 * @param ctx the parse tree
	 */
	void enterHeader(knxscriptParser.HeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#header}.
	 * @param ctx the parse tree
	 */
	void exitHeader(knxscriptParser.HeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#title}.
	 * @param ctx the parse tree
	 */
	void enterTitle(knxscriptParser.TitleContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#title}.
	 * @param ctx the parse tree
	 */
	void exitTitle(knxscriptParser.TitleContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#description}.
	 * @param ctx the parse tree
	 */
	void enterDescription(knxscriptParser.DescriptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#description}.
	 * @param ctx the parse tree
	 */
	void exitDescription(knxscriptParser.DescriptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#activation}.
	 * @param ctx the parse tree
	 */
	void enterActivation(knxscriptParser.ActivationContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#activation}.
	 * @param ctx the parse tree
	 */
	void exitActivation(knxscriptParser.ActivationContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#active_block}.
	 * @param ctx the parse tree
	 */
	void enterActive_block(knxscriptParser.Active_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#active_block}.
	 * @param ctx the parse tree
	 */
	void exitActive_block(knxscriptParser.Active_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#filter}.
	 * @param ctx the parse tree
	 */
	void enterFilter(knxscriptParser.FilterContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#filter}.
	 * @param ctx the parse tree
	 */
	void exitFilter(knxscriptParser.FilterContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#environment_filter}.
	 * @param ctx the parse tree
	 */
	void enterEnvironment_filter(knxscriptParser.Environment_filterContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#environment_filter}.
	 * @param ctx the parse tree
	 */
	void exitEnvironment_filter(knxscriptParser.Environment_filterContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#timerange_filter}.
	 * @param ctx the parse tree
	 */
	void enterTimerange_filter(knxscriptParser.Timerange_filterContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#timerange_filter}.
	 * @param ctx the parse tree
	 */
	void exitTimerange_filter(knxscriptParser.Timerange_filterContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#rule}.
	 * @param ctx the parse tree
	 */
	void enterRule(knxscriptParser.RuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#rule}.
	 * @param ctx the parse tree
	 */
	void exitRule(knxscriptParser.RuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#trigger_block}.
	 * @param ctx the parse tree
	 */
	void enterTrigger_block(knxscriptParser.Trigger_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#trigger_block}.
	 * @param ctx the parse tree
	 */
	void exitTrigger_block(knxscriptParser.Trigger_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#trigger}.
	 * @param ctx the parse tree
	 */
	void enterTrigger(knxscriptParser.TriggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#trigger}.
	 * @param ctx the parse tree
	 */
	void exitTrigger(knxscriptParser.TriggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#enter_room_trigger}.
	 * @param ctx the parse tree
	 */
	void enterEnter_room_trigger(knxscriptParser.Enter_room_triggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#enter_room_trigger}.
	 * @param ctx the parse tree
	 */
	void exitEnter_room_trigger(knxscriptParser.Enter_room_triggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#room_id}.
	 * @param ctx the parse tree
	 */
	void enterRoom_id(knxscriptParser.Room_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#room_id}.
	 * @param ctx the parse tree
	 */
	void exitRoom_id(knxscriptParser.Room_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#leave_room_trigger}.
	 * @param ctx the parse tree
	 */
	void enterLeave_room_trigger(knxscriptParser.Leave_room_triggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#leave_room_trigger}.
	 * @param ctx the parse tree
	 */
	void exitLeave_room_trigger(knxscriptParser.Leave_room_triggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#alert_trigger}.
	 * @param ctx the parse tree
	 */
	void enterAlert_trigger(knxscriptParser.Alert_triggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#alert_trigger}.
	 * @param ctx the parse tree
	 */
	void exitAlert_trigger(knxscriptParser.Alert_triggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#statement_block}.
	 * @param ctx the parse tree
	 */
	void enterStatement_block(knxscriptParser.Statement_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#statement_block}.
	 * @param ctx the parse tree
	 */
	void exitStatement_block(knxscriptParser.Statement_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(knxscriptParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(knxscriptParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#simple_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSimple_stmt(knxscriptParser.Simple_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#simple_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSimple_stmt(knxscriptParser.Simple_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#current_room_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCurrent_room_stmt(knxscriptParser.Current_room_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#current_room_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCurrent_room_stmt(knxscriptParser.Current_room_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#wait_stmt}.
	 * @param ctx the parse tree
	 */
	void enterWait_stmt(knxscriptParser.Wait_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#wait_stmt}.
	 * @param ctx the parse tree
	 */
	void exitWait_stmt(knxscriptParser.Wait_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#notify_stmt}.
	 * @param ctx the parse tree
	 */
	void enterNotify_stmt(knxscriptParser.Notify_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#notify_stmt}.
	 * @param ctx the parse tree
	 */
	void exitNotify_stmt(knxscriptParser.Notify_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#http_stmt}.
	 * @param ctx the parse tree
	 */
	void enterHttp_stmt(knxscriptParser.Http_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#http_stmt}.
	 * @param ctx the parse tree
	 */
	void exitHttp_stmt(knxscriptParser.Http_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#method}.
	 * @param ctx the parse tree
	 */
	void enterMethod(knxscriptParser.MethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#method}.
	 * @param ctx the parse tree
	 */
	void exitMethod(knxscriptParser.MethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#http_content}.
	 * @param ctx the parse tree
	 */
	void enterHttp_content(knxscriptParser.Http_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#http_content}.
	 * @param ctx the parse tree
	 */
	void exitHttp_content(knxscriptParser.Http_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#http_header}.
	 * @param ctx the parse tree
	 */
	void enterHttp_header(knxscriptParser.Http_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#http_header}.
	 * @param ctx the parse tree
	 */
	void exitHttp_header(knxscriptParser.Http_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(knxscriptParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(knxscriptParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#parallel_block}.
	 * @param ctx the parse tree
	 */
	void enterParallel_block(knxscriptParser.Parallel_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#parallel_block}.
	 * @param ctx the parse tree
	 */
	void exitParallel_block(knxscriptParser.Parallel_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_stmt(knxscriptParser.If_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_stmt(knxscriptParser.If_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(knxscriptParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(knxscriptParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#ask_stmt}.
	 * @param ctx the parse tree
	 */
	void enterAsk_stmt(knxscriptParser.Ask_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#ask_stmt}.
	 * @param ctx the parse tree
	 */
	void exitAsk_stmt(knxscriptParser.Ask_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#switch_block}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_block(knxscriptParser.Switch_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#switch_block}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_block(knxscriptParser.Switch_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#switch_option}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_option(knxscriptParser.Switch_optionContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#switch_option}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_option(knxscriptParser.Switch_optionContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#op}.
	 * @param ctx the parse tree
	 */
	void enterOp(knxscriptParser.OpContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#op}.
	 * @param ctx the parse tree
	 */
	void exitOp(knxscriptParser.OpContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#knx_stmt}.
	 * @param ctx the parse tree
	 */
	void enterKnx_stmt(knxscriptParser.Knx_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#knx_stmt}.
	 * @param ctx the parse tree
	 */
	void exitKnx_stmt(knxscriptParser.Knx_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#knx_read_stmt}.
	 * @param ctx the parse tree
	 */
	void enterKnx_read_stmt(knxscriptParser.Knx_read_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#knx_read_stmt}.
	 * @param ctx the parse tree
	 */
	void exitKnx_read_stmt(knxscriptParser.Knx_read_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link knxscriptParser#kmx_write_stmt}.
	 * @param ctx the parse tree
	 */
	void enterKmx_write_stmt(knxscriptParser.Kmx_write_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link knxscriptParser#kmx_write_stmt}.
	 * @param ctx the parse tree
	 */
	void exitKmx_write_stmt(knxscriptParser.Kmx_write_stmtContext ctx);
}