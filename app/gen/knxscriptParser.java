// Generated from C:/Users/Manu/AndroidStudioProjects/RoomDetect/app/src/main/antlr\knxscript.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class knxscriptParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, TIME=27, URL=28, DTP=29, BLOCK_START=30, BLOCK_END=31, 
		WS=32, ENV_ID_TYPE=33, STRING=34, INTEGER=35, REAL=36, BOOL=37, GE=38, 
		EQ=39, LE=40, NEQ=41, GT=42, LT=43, NEWLINE=44, ALPHANUM=45, ID=46, DOT=47, 
		MINUS=48;
	public static final int
		RULE_start = 0, RULE_header = 1, RULE_title = 2, RULE_description = 3, 
		RULE_activation = 4, RULE_active_block = 5, RULE_filter = 6, RULE_environment_filter = 7, 
		RULE_timerange_filter = 8, RULE_rule = 9, RULE_trigger_block = 10, RULE_trigger = 11, 
		RULE_enter_room_trigger = 12, RULE_room_id = 13, RULE_leave_room_trigger = 14, 
		RULE_alert_trigger = 15, RULE_statement_block = 16, RULE_stmt = 17, RULE_simple_stmt = 18, 
		RULE_current_room_stmt = 19, RULE_wait_stmt = 20, RULE_notify_stmt = 21, 
		RULE_http_stmt = 22, RULE_method = 23, RULE_http_content = 24, RULE_http_header = 25, 
		RULE_literal = 26, RULE_parallel_block = 27, RULE_if_stmt = 28, RULE_expression = 29, 
		RULE_ask_stmt = 30, RULE_switch_block = 31, RULE_switch_option = 32, RULE_op = 33, 
		RULE_knx_stmt = 34, RULE_knx_read_stmt = 35, RULE_kmx_write_stmt = 36;
	public static final String[] ruleNames = {
		"start", "header", "title", "description", "activation", "active_block", 
		"filter", "environment_filter", "timerange_filter", "rule", "trigger_block", 
		"trigger", "enter_room_trigger", "room_id", "leave_room_trigger", "alert_trigger", 
		"statement_block", "stmt", "simple_stmt", "current_room_stmt", "wait_stmt", 
		"notify_stmt", "http_stmt", "method", "http_content", "http_header", "literal", 
		"parallel_block", "if_stmt", "expression", "ask_stmt", "switch_block", 
		"switch_option", "op", "knx_stmt", "knx_read_stmt", "kmx_write_stmt"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'script'", "'description'", "'active'", "'environment'", "'time between'", 
		"'on'", "'do'", "'enter room '", "'leave room'", "'time'", "'current room'", 
		"'wait'", "'notify '", "'http'", "'header='", "'get'", "'put'", "'post'", 
		"'delete'", "'parallel'", "'if '", "'else '", "'ask'", "':'", "'read '", 
		"'write '", null, null, null, null, null, null, null, null, null, null, 
		null, "'>='", "'='", "'<='", "'!='", "'>'", "'<'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, "TIME", "URL", "DTP", "BLOCK_START", "BLOCK_END", "WS", 
		"ENV_ID_TYPE", "STRING", "INTEGER", "REAL", "BOOL", "GE", "EQ", "LE", 
		"NEQ", "GT", "LT", "NEWLINE", "ALPHANUM", "ID", "DOT", "MINUS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "knxscript.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public knxscriptParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StartContext extends ParserRuleContext {
		public HeaderContext header() {
			return getRuleContext(HeaderContext.class,0);
		}
		public RuleContext rule() {
			return getRuleContext(RuleContext.class,0);
		}
		public TerminalNode EOF() { return getToken(knxscriptParser.EOF, 0); }
		public ActivationContext activation() {
			return getRuleContext(ActivationContext.class,0);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			header();
			setState(76);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(75);
				activation();
				}
			}

			setState(78);
			rule();
			setState(79);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HeaderContext extends ParserRuleContext {
		public TitleContext title() {
			return getRuleContext(TitleContext.class,0);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(knxscriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(knxscriptParser.NEWLINE, i);
		}
		public DescriptionContext description() {
			return getRuleContext(DescriptionContext.class,0);
		}
		public HeaderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterHeader(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitHeader(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitHeader(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HeaderContext header() throws RecognitionException {
		HeaderContext _localctx = new HeaderContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_header);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(T__0);
			setState(82);
			title();
			setState(83);
			match(NEWLINE);
			setState(88);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(84);
				match(T__1);
				setState(85);
				description();
				setState(86);
				match(NEWLINE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TitleContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public TitleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_title; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterTitle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitTitle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitTitle(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TitleContext title() throws RecognitionException {
		TitleContext _localctx = new TitleContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_title);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DescriptionContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public DescriptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_description; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterDescription(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitDescription(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitDescription(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DescriptionContext description() throws RecognitionException {
		DescriptionContext _localctx = new DescriptionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_description);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActivationContext extends ParserRuleContext {
		public Active_blockContext active_block() {
			return getRuleContext(Active_blockContext.class,0);
		}
		public ActivationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_activation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterActivation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitActivation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitActivation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActivationContext activation() throws RecognitionException {
		ActivationContext _localctx = new ActivationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_activation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94);
			match(T__2);
			setState(95);
			active_block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Active_blockContext extends ParserRuleContext {
		public TerminalNode BLOCK_START() { return getToken(knxscriptParser.BLOCK_START, 0); }
		public TerminalNode BLOCK_END() { return getToken(knxscriptParser.BLOCK_END, 0); }
		public List<FilterContext> filter() {
			return getRuleContexts(FilterContext.class);
		}
		public FilterContext filter(int i) {
			return getRuleContext(FilterContext.class,i);
		}
		public Active_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_active_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterActive_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitActive_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitActive_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Active_blockContext active_block() throws RecognitionException {
		Active_blockContext _localctx = new Active_blockContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_active_block);
		int _la;
		try {
			setState(106);
			switch (_input.LA(1)) {
			case BLOCK_START:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(97);
				match(BLOCK_START);
				setState(99); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(98);
					filter();
					}
					}
					setState(101); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__3 || _la==T__4 );
				setState(103);
				match(BLOCK_END);
				}
				}
				break;
			case T__3:
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				setState(105);
				filter();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilterContext extends ParserRuleContext {
		public TerminalNode NEWLINE() { return getToken(knxscriptParser.NEWLINE, 0); }
		public Environment_filterContext environment_filter() {
			return getRuleContext(Environment_filterContext.class,0);
		}
		public Timerange_filterContext timerange_filter() {
			return getRuleContext(Timerange_filterContext.class,0);
		}
		public FilterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterFilter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitFilter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitFilter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FilterContext filter() throws RecognitionException {
		FilterContext _localctx = new FilterContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_filter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			switch (_input.LA(1)) {
			case T__3:
				{
				setState(108);
				environment_filter();
				}
				break;
			case T__4:
				{
				setState(109);
				timerange_filter();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(112);
			match(NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Environment_filterContext extends ParserRuleContext {
		public TerminalNode ENV_ID_TYPE() { return getToken(knxscriptParser.ENV_ID_TYPE, 0); }
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public Environment_filterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_environment_filter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterEnvironment_filter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitEnvironment_filter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitEnvironment_filter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Environment_filterContext environment_filter() throws RecognitionException {
		Environment_filterContext _localctx = new Environment_filterContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_environment_filter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(T__3);
			setState(115);
			match(ENV_ID_TYPE);
			setState(116);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Timerange_filterContext extends ParserRuleContext {
		public List<TerminalNode> TIME() { return getTokens(knxscriptParser.TIME); }
		public TerminalNode TIME(int i) {
			return getToken(knxscriptParser.TIME, i);
		}
		public Timerange_filterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timerange_filter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterTimerange_filter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitTimerange_filter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitTimerange_filter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Timerange_filterContext timerange_filter() throws RecognitionException {
		Timerange_filterContext _localctx = new Timerange_filterContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_timerange_filter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			match(T__4);
			setState(119);
			match(TIME);
			setState(120);
			match(TIME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RuleContext extends ParserRuleContext {
		public Trigger_blockContext trigger_block() {
			return getRuleContext(Trigger_blockContext.class,0);
		}
		public Statement_blockContext statement_block() {
			return getRuleContext(Statement_blockContext.class,0);
		}
		public RuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RuleContext rule() throws RecognitionException {
		RuleContext _localctx = new RuleContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_rule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122);
			match(T__5);
			setState(123);
			trigger_block();
			setState(124);
			match(T__6);
			setState(125);
			statement_block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Trigger_blockContext extends ParserRuleContext {
		public TerminalNode BLOCK_START() { return getToken(knxscriptParser.BLOCK_START, 0); }
		public TerminalNode BLOCK_END() { return getToken(knxscriptParser.BLOCK_END, 0); }
		public List<TriggerContext> trigger() {
			return getRuleContexts(TriggerContext.class);
		}
		public TriggerContext trigger(int i) {
			return getRuleContext(TriggerContext.class,i);
		}
		public Trigger_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trigger_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterTrigger_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitTrigger_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitTrigger_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Trigger_blockContext trigger_block() throws RecognitionException {
		Trigger_blockContext _localctx = new Trigger_blockContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_trigger_block);
		int _la;
		try {
			setState(136);
			switch (_input.LA(1)) {
			case BLOCK_START:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(127);
				match(BLOCK_START);
				setState(129); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(128);
					trigger();
					}
					}
					setState(131); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__8) | (1L << T__9))) != 0) );
				setState(133);
				match(BLOCK_END);
				}
				}
				break;
			case T__7:
			case T__8:
			case T__9:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(135);
				trigger();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerContext extends ParserRuleContext {
		public TerminalNode NEWLINE() { return getToken(knxscriptParser.NEWLINE, 0); }
		public Enter_room_triggerContext enter_room_trigger() {
			return getRuleContext(Enter_room_triggerContext.class,0);
		}
		public Leave_room_triggerContext leave_room_trigger() {
			return getRuleContext(Leave_room_triggerContext.class,0);
		}
		public Alert_triggerContext alert_trigger() {
			return getRuleContext(Alert_triggerContext.class,0);
		}
		public TriggerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trigger; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterTrigger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitTrigger(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitTrigger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerContext trigger() throws RecognitionException {
		TriggerContext _localctx = new TriggerContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_trigger);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			switch (_input.LA(1)) {
			case T__7:
				{
				setState(138);
				enter_room_trigger();
				}
				break;
			case T__8:
				{
				setState(139);
				leave_room_trigger();
				}
				break;
			case T__9:
				{
				setState(140);
				alert_trigger();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(143);
			match(NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enter_room_triggerContext extends ParserRuleContext {
		public Room_idContext room_id() {
			return getRuleContext(Room_idContext.class,0);
		}
		public Enter_room_triggerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enter_room_trigger; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterEnter_room_trigger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitEnter_room_trigger(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitEnter_room_trigger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Enter_room_triggerContext enter_room_trigger() throws RecognitionException {
		Enter_room_triggerContext _localctx = new Enter_room_triggerContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_enter_room_trigger);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			match(T__7);
			setState(146);
			room_id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Room_idContext extends ParserRuleContext {
		public TerminalNode REAL() { return getToken(knxscriptParser.REAL, 0); }
		public TerminalNode INTEGER() { return getToken(knxscriptParser.INTEGER, 0); }
		public List<TerminalNode> ALPHANUM() { return getTokens(knxscriptParser.ALPHANUM); }
		public TerminalNode ALPHANUM(int i) {
			return getToken(knxscriptParser.ALPHANUM, i);
		}
		public List<TerminalNode> DOT() { return getTokens(knxscriptParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(knxscriptParser.DOT, i);
		}
		public List<TerminalNode> MINUS() { return getTokens(knxscriptParser.MINUS); }
		public TerminalNode MINUS(int i) {
			return getToken(knxscriptParser.MINUS, i);
		}
		public Room_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_room_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterRoom_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitRoom_id(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitRoom_id(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Room_idContext room_id() throws RecognitionException {
		Room_idContext _localctx = new Room_idContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_room_id);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			switch (_input.LA(1)) {
			case ALPHANUM:
			case DOT:
			case MINUS:
				{
				setState(149); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(148);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ALPHANUM) | (1L << DOT) | (1L << MINUS))) != 0)) ) {
					_errHandler.recoverInline(this);
					} else {
						consume();
					}
					}
					}
					setState(151); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ALPHANUM) | (1L << DOT) | (1L << MINUS))) != 0) );
				}
				break;
			case REAL:
				{
				setState(153);
				match(REAL);
				}
				break;
			case INTEGER:
				{
				setState(154);
				match(INTEGER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Leave_room_triggerContext extends ParserRuleContext {
		public Room_idContext room_id() {
			return getRuleContext(Room_idContext.class,0);
		}
		public Leave_room_triggerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_leave_room_trigger; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterLeave_room_trigger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitLeave_room_trigger(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitLeave_room_trigger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Leave_room_triggerContext leave_room_trigger() throws RecognitionException {
		Leave_room_triggerContext _localctx = new Leave_room_triggerContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_leave_room_trigger);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(T__8);
			setState(158);
			room_id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alert_triggerContext extends ParserRuleContext {
		public TerminalNode TIME() { return getToken(knxscriptParser.TIME, 0); }
		public Alert_triggerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alert_trigger; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterAlert_trigger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitAlert_trigger(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitAlert_trigger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alert_triggerContext alert_trigger() throws RecognitionException {
		Alert_triggerContext _localctx = new Alert_triggerContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_alert_trigger);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(T__9);
			setState(161);
			match(TIME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Statement_blockContext extends ParserRuleContext {
		public TerminalNode BLOCK_START() { return getToken(knxscriptParser.BLOCK_START, 0); }
		public TerminalNode BLOCK_END() { return getToken(knxscriptParser.BLOCK_END, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public Statement_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterStatement_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitStatement_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitStatement_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Statement_blockContext statement_block() throws RecognitionException {
		Statement_blockContext _localctx = new Statement_blockContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_statement_block);
		int _la;
		try {
			setState(172);
			switch (_input.LA(1)) {
			case BLOCK_START:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(163);
				match(BLOCK_START);
				setState(165); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(164);
					stmt();
					}
					}
					setState(167); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__19) | (1L << T__20) | (1L << T__22) | (1L << T__24) | (1L << T__25) | (1L << STRING) | (1L << INTEGER) | (1L << REAL) | (1L << BOOL))) != 0) );
				setState(169);
				match(BLOCK_END);
				}
				}
				break;
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__19:
			case T__20:
			case T__22:
			case T__24:
			case T__25:
			case STRING:
			case INTEGER:
			case REAL:
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(171);
				stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public If_stmtContext if_stmt() {
			return getRuleContext(If_stmtContext.class,0);
		}
		public Parallel_blockContext parallel_block() {
			return getRuleContext(Parallel_blockContext.class,0);
		}
		public Ask_stmtContext ask_stmt() {
			return getRuleContext(Ask_stmtContext.class,0);
		}
		public Simple_stmtContext simple_stmt() {
			return getRuleContext(Simple_stmtContext.class,0);
		}
		public TerminalNode NEWLINE() { return getToken(knxscriptParser.NEWLINE, 0); }
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterStmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitStmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			switch (_input.LA(1)) {
			case T__20:
				{
				setState(174);
				if_stmt();
				}
				break;
			case T__19:
				{
				setState(175);
				parallel_block();
				}
				break;
			case T__22:
				{
				setState(176);
				ask_stmt();
				}
				break;
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__24:
			case T__25:
			case STRING:
			case INTEGER:
			case REAL:
			case BOOL:
				{
				setState(177);
				simple_stmt();
				setState(178);
				match(NEWLINE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Simple_stmtContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public Http_stmtContext http_stmt() {
			return getRuleContext(Http_stmtContext.class,0);
		}
		public Knx_stmtContext knx_stmt() {
			return getRuleContext(Knx_stmtContext.class,0);
		}
		public Notify_stmtContext notify_stmt() {
			return getRuleContext(Notify_stmtContext.class,0);
		}
		public Wait_stmtContext wait_stmt() {
			return getRuleContext(Wait_stmtContext.class,0);
		}
		public Current_room_stmtContext current_room_stmt() {
			return getRuleContext(Current_room_stmtContext.class,0);
		}
		public Simple_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simple_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterSimple_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitSimple_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitSimple_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Simple_stmtContext simple_stmt() throws RecognitionException {
		Simple_stmtContext _localctx = new Simple_stmtContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_simple_stmt);
		try {
			setState(188);
			switch (_input.LA(1)) {
			case STRING:
			case INTEGER:
			case REAL:
			case BOOL:
				enterOuterAlt(_localctx, 1);
				{
				setState(182);
				literal();
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 2);
				{
				setState(183);
				http_stmt();
				}
				break;
			case T__24:
			case T__25:
				enterOuterAlt(_localctx, 3);
				{
				setState(184);
				knx_stmt();
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 4);
				{
				setState(185);
				notify_stmt();
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 5);
				{
				setState(186);
				wait_stmt();
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 6);
				{
				setState(187);
				current_room_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Current_room_stmtContext extends ParserRuleContext {
		public Current_room_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_current_room_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterCurrent_room_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitCurrent_room_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitCurrent_room_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Current_room_stmtContext current_room_stmt() throws RecognitionException {
		Current_room_stmtContext _localctx = new Current_room_stmtContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_current_room_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Wait_stmtContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(knxscriptParser.INTEGER, 0); }
		public Wait_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wait_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterWait_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitWait_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitWait_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Wait_stmtContext wait_stmt() throws RecognitionException {
		Wait_stmtContext _localctx = new Wait_stmtContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_wait_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(T__11);
			setState(193);
			match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Notify_stmtContext extends ParserRuleContext {
		public List<TerminalNode> STRING() { return getTokens(knxscriptParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(knxscriptParser.STRING, i);
		}
		public Notify_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notify_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterNotify_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitNotify_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitNotify_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Notify_stmtContext notify_stmt() throws RecognitionException {
		Notify_stmtContext _localctx = new Notify_stmtContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_notify_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195);
			match(T__12);
			setState(196);
			match(STRING);
			setState(198);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(197);
				match(STRING);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Http_stmtContext extends ParserRuleContext {
		public MethodContext method() {
			return getRuleContext(MethodContext.class,0);
		}
		public TerminalNode URL() { return getToken(knxscriptParser.URL, 0); }
		public Http_contentContext http_content() {
			return getRuleContext(Http_contentContext.class,0);
		}
		public Http_headerContext http_header() {
			return getRuleContext(Http_headerContext.class,0);
		}
		public Http_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_http_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterHttp_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitHttp_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitHttp_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Http_stmtContext http_stmt() throws RecognitionException {
		Http_stmtContext _localctx = new Http_stmtContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_http_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			match(T__13);
			setState(201);
			method();
			setState(202);
			match(URL);
			setState(204);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(203);
				http_content();
				}
				break;
			}
			setState(208);
			_la = _input.LA(1);
			if (_la==T__14) {
				{
				setState(206);
				match(T__14);
				setState(207);
				http_header();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodContext extends ParserRuleContext {
		public MethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitMethod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitMethod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodContext method() throws RecognitionException {
		MethodContext _localctx = new MethodContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_method);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Http_contentContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public Http_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_http_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterHttp_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitHttp_content(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitHttp_content(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Http_contentContext http_content() throws RecognitionException {
		Http_contentContext _localctx = new Http_contentContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_http_content);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Http_headerContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public Http_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_http_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterHttp_header(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitHttp_header(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitHttp_header(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Http_headerContext http_header() throws RecognitionException {
		Http_headerContext _localctx = new Http_headerContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_http_header);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public TerminalNode INTEGER() { return getToken(knxscriptParser.INTEGER, 0); }
		public TerminalNode REAL() { return getToken(knxscriptParser.REAL, 0); }
		public TerminalNode BOOL() { return getToken(knxscriptParser.BOOL, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(216);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STRING) | (1L << INTEGER) | (1L << REAL) | (1L << BOOL))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parallel_blockContext extends ParserRuleContext {
		public TerminalNode BLOCK_START() { return getToken(knxscriptParser.BLOCK_START, 0); }
		public TerminalNode BLOCK_END() { return getToken(knxscriptParser.BLOCK_END, 0); }
		public List<Simple_stmtContext> simple_stmt() {
			return getRuleContexts(Simple_stmtContext.class);
		}
		public Simple_stmtContext simple_stmt(int i) {
			return getRuleContext(Simple_stmtContext.class,i);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(knxscriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(knxscriptParser.NEWLINE, i);
		}
		public Parallel_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallel_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterParallel_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitParallel_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitParallel_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parallel_blockContext parallel_block() throws RecognitionException {
		Parallel_blockContext _localctx = new Parallel_blockContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_parallel_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218);
			match(T__19);
			setState(219);
			match(BLOCK_START);
			setState(223); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(220);
				simple_stmt();
				setState(221);
				match(NEWLINE);
				}
				}
				setState(225); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__24) | (1L << T__25) | (1L << STRING) | (1L << INTEGER) | (1L << REAL) | (1L << BOOL))) != 0) );
			setState(227);
			match(BLOCK_END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_stmtContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<Statement_blockContext> statement_block() {
			return getRuleContexts(Statement_blockContext.class);
		}
		public Statement_blockContext statement_block(int i) {
			return getRuleContext(Statement_blockContext.class,i);
		}
		public If_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterIf_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitIf_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitIf_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_stmtContext if_stmt() throws RecognitionException {
		If_stmtContext _localctx = new If_stmtContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_if_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			match(T__20);
			setState(230);
			expression();
			setState(231);
			statement_block();
			setState(234);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(232);
				match(T__21);
				setState(233);
				statement_block();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public List<Simple_stmtContext> simple_stmt() {
			return getRuleContexts(Simple_stmtContext.class);
		}
		public Simple_stmtContext simple_stmt(int i) {
			return getRuleContext(Simple_stmtContext.class,i);
		}
		public OpContext op() {
			return getRuleContext(OpContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(236);
			simple_stmt();
			setState(237);
			op();
			setState(238);
			simple_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ask_stmtContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public Switch_blockContext switch_block() {
			return getRuleContext(Switch_blockContext.class,0);
		}
		public Ask_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ask_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterAsk_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitAsk_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitAsk_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ask_stmtContext ask_stmt() throws RecognitionException {
		Ask_stmtContext _localctx = new Ask_stmtContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_ask_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(240);
			match(T__22);
			setState(241);
			match(STRING);
			setState(242);
			switch_block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_blockContext extends ParserRuleContext {
		public TerminalNode BLOCK_START() { return getToken(knxscriptParser.BLOCK_START, 0); }
		public TerminalNode BLOCK_END() { return getToken(knxscriptParser.BLOCK_END, 0); }
		public List<Switch_optionContext> switch_option() {
			return getRuleContexts(Switch_optionContext.class);
		}
		public Switch_optionContext switch_option(int i) {
			return getRuleContext(Switch_optionContext.class,i);
		}
		public Switch_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterSwitch_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitSwitch_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitSwitch_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_blockContext switch_block() throws RecognitionException {
		Switch_blockContext _localctx = new Switch_blockContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_switch_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(244);
			match(BLOCK_START);
			setState(246); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(245);
				switch_option();
				}
				}
				setState(248); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==STRING );
			setState(250);
			match(BLOCK_END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_optionContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(knxscriptParser.STRING, 0); }
		public Simple_stmtContext simple_stmt() {
			return getRuleContext(Simple_stmtContext.class,0);
		}
		public TerminalNode NEWLINE() { return getToken(knxscriptParser.NEWLINE, 0); }
		public TerminalNode BLOCK_START() { return getToken(knxscriptParser.BLOCK_START, 0); }
		public TerminalNode BLOCK_END() { return getToken(knxscriptParser.BLOCK_END, 0); }
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public Switch_optionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_option; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterSwitch_option(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitSwitch_option(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitSwitch_option(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_optionContext switch_option() throws RecognitionException {
		Switch_optionContext _localctx = new Switch_optionContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_switch_option);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(252);
			match(STRING);
			setState(265);
			switch (_input.LA(1)) {
			case BLOCK_START:
				{
				{
				setState(253);
				match(BLOCK_START);
				setState(255); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(254);
					stmt();
					}
					}
					setState(257); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__19) | (1L << T__20) | (1L << T__22) | (1L << T__24) | (1L << T__25) | (1L << STRING) | (1L << INTEGER) | (1L << REAL) | (1L << BOOL))) != 0) );
				setState(259);
				match(BLOCK_END);
				}
				}
				break;
			case T__23:
				{
				setState(261);
				match(T__23);
				setState(262);
				simple_stmt();
				setState(263);
				match(NEWLINE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OpContext extends ParserRuleContext {
		public TerminalNode EQ() { return getToken(knxscriptParser.EQ, 0); }
		public TerminalNode GT() { return getToken(knxscriptParser.GT, 0); }
		public TerminalNode LT() { return getToken(knxscriptParser.LT, 0); }
		public TerminalNode GE() { return getToken(knxscriptParser.GE, 0); }
		public TerminalNode LE() { return getToken(knxscriptParser.LE, 0); }
		public TerminalNode NEQ() { return getToken(knxscriptParser.NEQ, 0); }
		public OpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OpContext op() throws RecognitionException {
		OpContext _localctx = new OpContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_op);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << GE) | (1L << EQ) | (1L << LE) | (1L << NEQ) | (1L << GT) | (1L << LT))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Knx_stmtContext extends ParserRuleContext {
		public Knx_read_stmtContext knx_read_stmt() {
			return getRuleContext(Knx_read_stmtContext.class,0);
		}
		public Kmx_write_stmtContext kmx_write_stmt() {
			return getRuleContext(Kmx_write_stmtContext.class,0);
		}
		public Knx_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_knx_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterKnx_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitKnx_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitKnx_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Knx_stmtContext knx_stmt() throws RecognitionException {
		Knx_stmtContext _localctx = new Knx_stmtContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_knx_stmt);
		try {
			setState(271);
			switch (_input.LA(1)) {
			case T__24:
				enterOuterAlt(_localctx, 1);
				{
				setState(269);
				knx_read_stmt();
				}
				break;
			case T__25:
				enterOuterAlt(_localctx, 2);
				{
				setState(270);
				kmx_write_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Knx_read_stmtContext extends ParserRuleContext {
		public TerminalNode DTP() { return getToken(knxscriptParser.DTP, 0); }
		public TerminalNode URL() { return getToken(knxscriptParser.URL, 0); }
		public Knx_read_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_knx_read_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterKnx_read_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitKnx_read_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitKnx_read_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Knx_read_stmtContext knx_read_stmt() throws RecognitionException {
		Knx_read_stmtContext _localctx = new Knx_read_stmtContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_knx_read_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(273);
			match(T__24);
			setState(274);
			match(DTP);
			setState(275);
			match(URL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Kmx_write_stmtContext extends ParserRuleContext {
		public TerminalNode DTP() { return getToken(knxscriptParser.DTP, 0); }
		public TerminalNode URL() { return getToken(knxscriptParser.URL, 0); }
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public Kmx_write_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kmx_write_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).enterKmx_write_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof knxscriptListener ) ((knxscriptListener)listener).exitKmx_write_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof knxscriptVisitor ) return ((knxscriptVisitor<? extends T>)visitor).visitKmx_write_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Kmx_write_stmtContext kmx_write_stmt() throws RecognitionException {
		Kmx_write_stmtContext _localctx = new Kmx_write_stmtContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_kmx_write_stmt);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(277);
			match(T__25);
			setState(278);
			match(DTP);
			setState(279);
			match(URL);
			setState(281); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(280);
					literal();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(283); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\62\u0120\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\3\2\3\2\5\2O\n\2\3\2\3\2\3\2\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\5\3[\n\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\7\3\7\6"+
		"\7f\n\7\r\7\16\7g\3\7\3\7\3\7\5\7m\n\7\3\b\3\b\5\bq\n\b\3\b\3\b\3\t\3"+
		"\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\f\3\f\6\f\u0084"+
		"\n\f\r\f\16\f\u0085\3\f\3\f\3\f\5\f\u008b\n\f\3\r\3\r\3\r\5\r\u0090\n"+
		"\r\3\r\3\r\3\16\3\16\3\16\3\17\6\17\u0098\n\17\r\17\16\17\u0099\3\17\3"+
		"\17\5\17\u009e\n\17\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\6\22\u00a8"+
		"\n\22\r\22\16\22\u00a9\3\22\3\22\3\22\5\22\u00af\n\22\3\23\3\23\3\23\3"+
		"\23\3\23\3\23\5\23\u00b7\n\23\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00bf"+
		"\n\24\3\25\3\25\3\26\3\26\3\26\3\27\3\27\3\27\5\27\u00c9\n\27\3\30\3\30"+
		"\3\30\3\30\5\30\u00cf\n\30\3\30\3\30\5\30\u00d3\n\30\3\31\3\31\3\32\3"+
		"\32\3\33\3\33\3\34\3\34\3\35\3\35\3\35\3\35\3\35\6\35\u00e2\n\35\r\35"+
		"\16\35\u00e3\3\35\3\35\3\36\3\36\3\36\3\36\3\36\5\36\u00ed\n\36\3\37\3"+
		"\37\3\37\3\37\3 \3 \3 \3 \3!\3!\6!\u00f9\n!\r!\16!\u00fa\3!\3!\3\"\3\""+
		"\3\"\6\"\u0102\n\"\r\"\16\"\u0103\3\"\3\"\3\"\3\"\3\"\3\"\5\"\u010c\n"+
		"\"\3#\3#\3$\3$\5$\u0112\n$\3%\3%\3%\3%\3&\3&\3&\3&\6&\u011c\n&\r&\16&"+
		"\u011d\3&\2\2\'\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62"+
		"\64\668:<>@BDFHJ\2\6\4\2//\61\62\3\2\22\25\3\2$\'\3\2(-\u011a\2L\3\2\2"+
		"\2\4S\3\2\2\2\6\\\3\2\2\2\b^\3\2\2\2\n`\3\2\2\2\fl\3\2\2\2\16p\3\2\2\2"+
		"\20t\3\2\2\2\22x\3\2\2\2\24|\3\2\2\2\26\u008a\3\2\2\2\30\u008f\3\2\2\2"+
		"\32\u0093\3\2\2\2\34\u009d\3\2\2\2\36\u009f\3\2\2\2 \u00a2\3\2\2\2\"\u00ae"+
		"\3\2\2\2$\u00b6\3\2\2\2&\u00be\3\2\2\2(\u00c0\3\2\2\2*\u00c2\3\2\2\2,"+
		"\u00c5\3\2\2\2.\u00ca\3\2\2\2\60\u00d4\3\2\2\2\62\u00d6\3\2\2\2\64\u00d8"+
		"\3\2\2\2\66\u00da\3\2\2\28\u00dc\3\2\2\2:\u00e7\3\2\2\2<\u00ee\3\2\2\2"+
		">\u00f2\3\2\2\2@\u00f6\3\2\2\2B\u00fe\3\2\2\2D\u010d\3\2\2\2F\u0111\3"+
		"\2\2\2H\u0113\3\2\2\2J\u0117\3\2\2\2LN\5\4\3\2MO\5\n\6\2NM\3\2\2\2NO\3"+
		"\2\2\2OP\3\2\2\2PQ\5\24\13\2QR\7\2\2\3R\3\3\2\2\2ST\7\3\2\2TU\5\6\4\2"+
		"UZ\7.\2\2VW\7\4\2\2WX\5\b\5\2XY\7.\2\2Y[\3\2\2\2ZV\3\2\2\2Z[\3\2\2\2["+
		"\5\3\2\2\2\\]\7$\2\2]\7\3\2\2\2^_\7$\2\2_\t\3\2\2\2`a\7\5\2\2ab\5\f\7"+
		"\2b\13\3\2\2\2ce\7 \2\2df\5\16\b\2ed\3\2\2\2fg\3\2\2\2ge\3\2\2\2gh\3\2"+
		"\2\2hi\3\2\2\2ij\7!\2\2jm\3\2\2\2km\5\16\b\2lc\3\2\2\2lk\3\2\2\2m\r\3"+
		"\2\2\2nq\5\20\t\2oq\5\22\n\2pn\3\2\2\2po\3\2\2\2qr\3\2\2\2rs\7.\2\2s\17"+
		"\3\2\2\2tu\7\6\2\2uv\7#\2\2vw\7$\2\2w\21\3\2\2\2xy\7\7\2\2yz\7\35\2\2"+
		"z{\7\35\2\2{\23\3\2\2\2|}\7\b\2\2}~\5\26\f\2~\177\7\t\2\2\177\u0080\5"+
		"\"\22\2\u0080\25\3\2\2\2\u0081\u0083\7 \2\2\u0082\u0084\5\30\r\2\u0083"+
		"\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u0083\3\2\2\2\u0085\u0086\3\2"+
		"\2\2\u0086\u0087\3\2\2\2\u0087\u0088\7!\2\2\u0088\u008b\3\2\2\2\u0089"+
		"\u008b\5\30\r\2\u008a\u0081\3\2\2\2\u008a\u0089\3\2\2\2\u008b\27\3\2\2"+
		"\2\u008c\u0090\5\32\16\2\u008d\u0090\5\36\20\2\u008e\u0090\5 \21\2\u008f"+
		"\u008c\3\2\2\2\u008f\u008d\3\2\2\2\u008f\u008e\3\2\2\2\u0090\u0091\3\2"+
		"\2\2\u0091\u0092\7.\2\2\u0092\31\3\2\2\2\u0093\u0094\7\n\2\2\u0094\u0095"+
		"\5\34\17\2\u0095\33\3\2\2\2\u0096\u0098\t\2\2\2\u0097\u0096\3\2\2\2\u0098"+
		"\u0099\3\2\2\2\u0099\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009e\3\2"+
		"\2\2\u009b\u009e\7&\2\2\u009c\u009e\7%\2\2\u009d\u0097\3\2\2\2\u009d\u009b"+
		"\3\2\2\2\u009d\u009c\3\2\2\2\u009e\35\3\2\2\2\u009f\u00a0\7\13\2\2\u00a0"+
		"\u00a1\5\34\17\2\u00a1\37\3\2\2\2\u00a2\u00a3\7\f\2\2\u00a3\u00a4\7\35"+
		"\2\2\u00a4!\3\2\2\2\u00a5\u00a7\7 \2\2\u00a6\u00a8\5$\23\2\u00a7\u00a6"+
		"\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa"+
		"\u00ab\3\2\2\2\u00ab\u00ac\7!\2\2\u00ac\u00af\3\2\2\2\u00ad\u00af\5$\23"+
		"\2\u00ae\u00a5\3\2\2\2\u00ae\u00ad\3\2\2\2\u00af#\3\2\2\2\u00b0\u00b7"+
		"\5:\36\2\u00b1\u00b7\58\35\2\u00b2\u00b7\5> \2\u00b3\u00b4\5&\24\2\u00b4"+
		"\u00b5\7.\2\2\u00b5\u00b7\3\2\2\2\u00b6\u00b0\3\2\2\2\u00b6\u00b1\3\2"+
		"\2\2\u00b6\u00b2\3\2\2\2\u00b6\u00b3\3\2\2\2\u00b7%\3\2\2\2\u00b8\u00bf"+
		"\5\66\34\2\u00b9\u00bf\5.\30\2\u00ba\u00bf\5F$\2\u00bb\u00bf\5,\27\2\u00bc"+
		"\u00bf\5*\26\2\u00bd\u00bf\5(\25\2\u00be\u00b8\3\2\2\2\u00be\u00b9\3\2"+
		"\2\2\u00be\u00ba\3\2\2\2\u00be\u00bb\3\2\2\2\u00be\u00bc\3\2\2\2\u00be"+
		"\u00bd\3\2\2\2\u00bf\'\3\2\2\2\u00c0\u00c1\7\r\2\2\u00c1)\3\2\2\2\u00c2"+
		"\u00c3\7\16\2\2\u00c3\u00c4\7%\2\2\u00c4+\3\2\2\2\u00c5\u00c6\7\17\2\2"+
		"\u00c6\u00c8\7$\2\2\u00c7\u00c9\7$\2\2\u00c8\u00c7\3\2\2\2\u00c8\u00c9"+
		"\3\2\2\2\u00c9-\3\2\2\2\u00ca\u00cb\7\20\2\2\u00cb\u00cc\5\60\31\2\u00cc"+
		"\u00ce\7\36\2\2\u00cd\u00cf\5\62\32\2\u00ce\u00cd\3\2\2\2\u00ce\u00cf"+
		"\3\2\2\2\u00cf\u00d2\3\2\2\2\u00d0\u00d1\7\21\2\2\u00d1\u00d3\5\64\33"+
		"\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3/\3\2\2\2\u00d4\u00d5"+
		"\t\3\2\2\u00d5\61\3\2\2\2\u00d6\u00d7\7$\2\2\u00d7\63\3\2\2\2\u00d8\u00d9"+
		"\7$\2\2\u00d9\65\3\2\2\2\u00da\u00db\t\4\2\2\u00db\67\3\2\2\2\u00dc\u00dd"+
		"\7\26\2\2\u00dd\u00e1\7 \2\2\u00de\u00df\5&\24\2\u00df\u00e0\7.\2\2\u00e0"+
		"\u00e2\3\2\2\2\u00e1\u00de\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e1\3\2"+
		"\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\u00e6\7!\2\2\u00e6"+
		"9\3\2\2\2\u00e7\u00e8\7\27\2\2\u00e8\u00e9\5<\37\2\u00e9\u00ec\5\"\22"+
		"\2\u00ea\u00eb\7\30\2\2\u00eb\u00ed\5\"\22\2\u00ec\u00ea\3\2\2\2\u00ec"+
		"\u00ed\3\2\2\2\u00ed;\3\2\2\2\u00ee\u00ef\5&\24\2\u00ef\u00f0\5D#\2\u00f0"+
		"\u00f1\5&\24\2\u00f1=\3\2\2\2\u00f2\u00f3\7\31\2\2\u00f3\u00f4\7$\2\2"+
		"\u00f4\u00f5\5@!\2\u00f5?\3\2\2\2\u00f6\u00f8\7 \2\2\u00f7\u00f9\5B\""+
		"\2\u00f8\u00f7\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa\u00f8\3\2\2\2\u00fa\u00fb"+
		"\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fd\7!\2\2\u00fdA\3\2\2\2\u00fe\u010b"+
		"\7$\2\2\u00ff\u0101\7 \2\2\u0100\u0102\5$\23\2\u0101\u0100\3\2\2\2\u0102"+
		"\u0103\3\2\2\2\u0103\u0101\3\2\2\2\u0103\u0104\3\2\2\2\u0104\u0105\3\2"+
		"\2\2\u0105\u0106\7!\2\2\u0106\u010c\3\2\2\2\u0107\u0108\7\32\2\2\u0108"+
		"\u0109\5&\24\2\u0109\u010a\7.\2\2\u010a\u010c\3\2\2\2\u010b\u00ff\3\2"+
		"\2\2\u010b\u0107\3\2\2\2\u010cC\3\2\2\2\u010d\u010e\t\5\2\2\u010eE\3\2"+
		"\2\2\u010f\u0112\5H%\2\u0110\u0112\5J&\2\u0111\u010f\3\2\2\2\u0111\u0110"+
		"\3\2\2\2\u0112G\3\2\2\2\u0113\u0114\7\33\2\2\u0114\u0115\7\37\2\2\u0115"+
		"\u0116\7\36\2\2\u0116I\3\2\2\2\u0117\u0118\7\34\2\2\u0118\u0119\7\37\2"+
		"\2\u0119\u011b\7\36\2\2\u011a\u011c\5\66\34\2\u011b\u011a\3\2\2\2\u011c"+
		"\u011d\3\2\2\2\u011d\u011b\3\2\2\2\u011d\u011e\3\2\2\2\u011eK\3\2\2\2"+
		"\32NZglp\u0085\u008a\u008f\u0099\u009d\u00a9\u00ae\u00b6\u00be\u00c8\u00ce"+
		"\u00d2\u00e3\u00ec\u00fa\u0103\u010b\u0111\u011d";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}