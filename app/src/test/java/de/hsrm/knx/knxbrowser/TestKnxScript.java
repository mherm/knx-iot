package de.hsrm.knx.knxbrowser;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import de.hsrm.knx.knxbrowser.script.ast.AstVisitor;
import de.hsrm.knx.knxbrowser.script.ast.KnxScript;
import de.hsrm.knx.knxbrowser.script.ast.KnxScriptWriter;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class TestKnxScript  {
    @Test
    public void testScript() {
        try {
            final InputStream inputStream = new FileInputStream(new File("C:\\Users\\Manu\\AndroidStudioProjects\\RoomDetect\\app\\src\\main\\res\\raw\\hello_knx_world.knxsc"));
            KnxScript script = KnxScript.read(inputStream);
            AstVisitor visitor = new KnxScriptWriter(System.out);
            visitor.visit(script);
            //script.writeOut(new SourceCodePrintStream(System.out));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}