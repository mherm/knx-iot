package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 12.10.2016.
 */

public interface Entity {
    public void setId(int id);
    public int getId();
}
