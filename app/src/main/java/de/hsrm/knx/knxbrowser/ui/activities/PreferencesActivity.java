package de.hsrm.knx.knxbrowser.ui.activities;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import de.hsrm.knx.knxbrowser.R;

/**
 * Created by Manuel Hermenau on 28.09.2016.
 */
public class PreferencesActivity extends PreferenceActivity
        implements Preference.OnPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        return false;
    }
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
