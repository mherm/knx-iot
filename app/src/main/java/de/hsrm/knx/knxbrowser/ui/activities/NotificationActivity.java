package de.hsrm.knx.knxbrowser.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.script.ast.AskCallbackReceiver;

/**
 * Created by Manuel Hermenau on 17.10.2016.
 */
public class NotificationActivity extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    public static final String INTENT_NOTIFICATION_ACTIVITY = "de.hsrm.knx.knxbrowser.intent.NOTIFICATION_ACTIVITY";
    public static final String EXTRA_NOTIFICATION_TITLE     = "de.hsrm.knx.knxbrowser.extra.NOTIFICAITON_TITLE";
    public static final String EXTRA_NOTIFICATION_OPTIONS   = "de.hsrm.knx.knxbrowser.extra.NOTIFICATION_OPTIONS";
    public static final String EXTRA_CALLBACK_ID            = "de.hsrm.knx.knxbrowser.extra.CALLBACK_ID";

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transparent);
        Intent intent = getIntent();
        String title = intent.getStringExtra(EXTRA_NOTIFICATION_TITLE);
        String[] options = intent.getStringArrayExtra(EXTRA_NOTIFICATION_OPTIONS);
        int id = intent.getIntExtra(EXTRA_CALLBACK_ID, -1);
        if(id == -1)
            finish();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(options,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        Intent intent = new Intent(AskCallbackReceiver.ASK_CALLBACK_INTENT);
                        intent.putExtra(AskCallbackReceiver.EXTRA_ANSWER_TEXT, options[which]);
                        intent.putExtra(AskCallbackReceiver.EXTRA_CALLBACK_ID, id);
                        sendBroadcast(intent);
                        finish();
                    }
                });
        builder.setOnDismissListener((d)->finish());
        builder.create().show();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
