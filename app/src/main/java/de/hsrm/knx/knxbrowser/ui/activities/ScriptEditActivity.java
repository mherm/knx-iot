package de.hsrm.knx.knxbrowser.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import org.jdeferred.DeferredManager;
import org.jdeferred.android.AndroidDeferredManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.db.DbHelper;

import static butterknife.ButterKnife.findById;

/**
 * Created by Manuel Hermenau on 15.10.2016.
 */
public class ScriptEditActivity extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================
    public static final String EXTRA_SCRIPT_ID = "de.hsrm.knx.knxbrowser.script.EXTRA_SCRIPT_ID";
    private static final String KEY_SCRIPT_TEXT = "mScriptText";
    private static final String EMPTY_SCRIPT = "script \"Unnamed\"\ndescription \"What does this script do?\"\n\non {\n\n}\ndo {\n\n}";

    // ===========================================================
    // Fields
    // ===========================================================

    @BindView(R.id.editText) EditText mEditText;
    private MenuItem mSaveMenuItem;
    private MenuItem mRestoreMenuItem;


    private String mScriptText;
    private DeferredManager mDm = new AndroidDeferredManager();
    private DbHelper mDb;
    private int mScriptId;

    //private final Stack<EditCommand> mUndoStack = new Stack<>();

    // ===========================================================
    // Constructors
    // ===========================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScriptId = getIntent().getIntExtra(EXTRA_SCRIPT_ID, -1);
        mDb = DbHelper.getInstance(this);
        initUI();
        initScript();
        initCallbacks();
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_SCRIPT_TEXT, mScriptText);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mScriptText = savedInstanceState.getString(KEY_SCRIPT_TEXT, "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_script_edit, menu);
        mSaveMenuItem = menu.findItem(R.id.action_save_script);
        mSaveMenuItem.setVisible(false);

        mRestoreMenuItem = menu.findItem(R.id.action_restore_script);
        mRestoreMenuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item == mSaveMenuItem){
            saveScript();
            return true;
        }
        if(item == mRestoreMenuItem){
            reloadScript();
            return true;
        }
        return false;
    }

    private void saveScript() {
        final String text = mEditText.getText().toString();
        mDm
                .when(() -> {
                    return mDb.saveScriptText(mScriptId, text);
                })
                .then((success)->{
                    if(success.getNewId() != null){
                        mScriptId = success.getNewId().intValue();
                    }
                   if(success.isSuccess()){
                       this.mScriptText = text;
                       Snackbar
                               .make(findViewById(android.R.id.content),
                                       "Script saved",
                                       Snackbar.LENGTH_LONG)
                               .show();
                       mSaveMenuItem.setVisible(false);
                       mRestoreMenuItem.setVisible(false);
                   }else{
                       Snackbar.make(
                               findViewById(android.R.id.content),
                               "Could not save script.",
                               Snackbar.LENGTH_LONG)
                               .show();
                   }
                });
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    private void initUI() {
        setContentView(R.layout.activity_edit_scripts);
        ButterKnife.bind(this);

        InputFilter filter = new InputFilter() {
            String quoteChars = "\u201D\u201E\u201C\u00BB\u00AB";
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source != null && source.length() > 0 && quoteChars.indexOf(source.charAt(0)) >= 0) {
                    return "\"";
                }
                return null;
            }
        };
        mEditText.setFilters(new InputFilter[]{filter});


    }

    private void reloadScript(){
        initScript();
        mSaveMenuItem.setVisible(false);
        mRestoreMenuItem.setVisible(false);
    }

    private void initScript() {
        if(mScriptId == -1)
            setText(EMPTY_SCRIPT);
        else
            mDm.when(()->mDb.getScriptText(mScriptId)).<String>then((str)->setText(str));
    }

    private void initCallbacks() {
        mEditText.addTextChangedListener(mTextWatcher);
    }
    public void setText(String text) {
        mScriptText = text;
        mEditText.setText(Html.fromHtml(Prettifier.toHtml(mScriptText)));
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private final TextWatcher mTextWatcher = new TextWatcher() {
        CharSequence stash;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //stash = s.subSequence(start, start + count);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //mUndoStack.push(new EditCommand(s, start, count, stash));
            stash = null;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(mSaveMenuItem != null) {
                mSaveMenuItem.setVisible(true);
                mRestoreMenuItem.setVisible(true);
            }
        }
    };
}
