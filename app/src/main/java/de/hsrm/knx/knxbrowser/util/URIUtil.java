package de.hsrm.knx.knxbrowser.util;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Manuel Hermenau on 20.09.2016.
 */
public class URIUtil {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public static URL makeURL(String url){
        if(!url.endsWith("/"))
            url = url +"/";
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static URL makeURL(URL base, String path){
        try {
            return new URL(base, path);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
