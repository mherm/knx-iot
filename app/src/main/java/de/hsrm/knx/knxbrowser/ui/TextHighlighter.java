/*
 * Copyright 2016 AKSHAY NAIK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
*/

package de.hsrm.knx.knxbrowser.ui;

import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by akshay on 14/08/16.
 * https://github.com/nakshay/TextHighlighter
 */

public class TextHighlighter {

    public String defaultColor="black";
    private HashMap<String,String> colorMap= new HashMap<>();
    private HashMap<String,String> styleMap=new HashMap<>();

    //pre-defined Styles
    public static final String NORMAL="NORMAL";
    public static final String BOLD="BOLD";
    public static final String ITALIC="ITALIC";
    public static final String UNDERLINE="UNDERLINE";
    public static final String SUPERSCRIPT="SUPERSCRIPT";
    public static final String SUBSCRIPT="SUBSCRIPT";

    public static final String BRACK_OPEN="{";
    public static final String BRACK_CLOSE="}";

    public static final String NEWLINE="\n";
    public static final String TAB="\t";
    public static final String INDENT=TAB;
    {
        styleMap.put(NEWLINE, NEWLINE);
        styleMap.put(TAB, TAB);
        styleMap.put(BRACK_OPEN, BRACK_OPEN);
        styleMap.put(BRACK_CLOSE, BRACK_CLOSE);
    }

    private String indent = "";
    private int indentDepth =0;
    private boolean inQuotes = false;

    public String getHighlightedText(String stringToBeHighlighted) {
        StringBuilder highlightedText= new StringBuilder("");
        String color="";
        String style;
        String myToken="";

        StringTokenizer tokenizer=new StringTokenizer(stringToBeHighlighted," \t\n\r\f", true);

        while (tokenizer.hasMoreTokens()) {
            myToken = tokenizer.nextToken();
            if(myToken.charAt(0) == '"')
                inQuotes = true;
            if(inQuotes) {
                highlightedText.append(myToken);
            }else{
                color = getColor(myToken);
                style = getStyle(myToken);
                final String str = styleTheToken(colorTheToken(myToken, color), style);
                highlightedText.append(str);
            }
            if (myToken.charAt(myToken.length()-1) == '"')
                inQuotes = false;
        }

        return highlightedText.toString();
    }

    private String getColor(String myToken) {

        String color=colorMap.get(myToken.toLowerCase());

        if(color==null) {
            color=defaultColor;
        }


        colorTheToken(myToken,color);

        return color;
    }

    private String colorTheToken(String token, String color) {
        return new StringBuilder("<font color='").append(color).append("'> ").append(token).append(" </font>").toString();
    }

    public void setColorForTheToken(String token, String color) {

        token=token.trim();
        color=color.trim();
        colorMap.put(token.toLowerCase(),color);
    }

    public void setColorForTheToken(String[] tokenArray,String color) {

        for (String token:tokenArray) {
            setColorForTheToken(token,color);
        }
    }

    public String[] getColorForTheToken(String[] tokenArray) {

        String[] color=new String[tokenArray.length];

        for(int index=0;index<tokenArray.length;index++) {
            color[index]=getColorForTheToken(tokenArray[index]);
        }

        return color;

    }

    public String getColorForTheToken(String token) {
        token=token.trim();
        String color="";

        color=colorMap.get(token.toLowerCase());

        if(color==null) {
            return defaultColor;
        }
        return color;

    }


    public String[] getStyleForTheToken(String[] tokenArray) {

        String[] style=new String[tokenArray.length];
        for(int index=0;index<tokenArray.length;index++) {
            style[index]=getStyleForTheToken(tokenArray[index]);
        }

        return style;
    }

    private String getStyle(String myToken) {

        String style=styleMap.get(myToken.toLowerCase());

        if(style==null) {
            style=NORMAL;
        }

        return style;
    }

    private String styleTheToken(String token,String style) {

        String taggedText=token;
        StringBuilder sb = new StringBuilder();
        switch (style) {
            case NORMAL :
                sb.append(token);
                break;

            case BOLD:
                taggedText= "<b> "+token+" </b>";
                sb.append("<b>").append(token).append("</b>");
                break;

            case ITALIC:
                sb.append("<i> ").append(token).append(" </i>");
                break;

            case UNDERLINE:
                sb.append("<u> ").append(token).append(" </u>");
                break;

            case SUPERSCRIPT:
                sb.append("<sup> ").append(token).append(" </sup>");
                break;

            case SUBSCRIPT:
                sb.append("<sub> ").append(token).append(" </sub>");
                break;
            case NEWLINE:
                sb.append("<br>").append(NEWLINE);
                for (int i=0; i<indentDepth;i++)
                    sb.append(INDENT);
                break;
            case TAB:
                sb.append("&emsp;");
                break;
            case BRACK_OPEN:
                indentDepth++;
                sb.append(token);
                break;
            case BRACK_CLOSE:
                indentDepth--;
                sb.append(token);
                break;
            default:
                sb.append(token);

        }
        return sb.toString();
    }

    public void setStyleForTheToken(String token,String style) {

        token=token.trim();
        style=style.trim();
        styleMap.put(token.toLowerCase(),style);

    }

    public void setStyleForTheToken(String[] tokenArray,String style) {

        for(String token:tokenArray) {
            setStyleForTheToken(token,style);
        }
    }

    public String getStyleForTheToken(String token) {
        token=token.trim();
        String style="";

        style=styleMap.get(token.toLowerCase());

        if (style==null) {
            return NORMAL;
        }
        return style;
    }

}