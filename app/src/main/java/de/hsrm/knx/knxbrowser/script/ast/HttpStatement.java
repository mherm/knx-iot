package de.hsrm.knx.knxbrowser.script.ast;

import java.util.regex.Pattern;

/**
 * Created by Manuel Hermenau on 07.10.2016.
 */
public class HttpStatement implements SimpleStatement {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = HttpStatement.class.getName();

    private final static Pattern UserPassPattern = Pattern.compile("^.*?:\\/\\/(([^:]*?):(.*?)@)?.*");
    // ===========================================================
    // Fields
    // ===========================================================
    String mUrl;
    Method mMethod;
    String mContent;
    public String mHeaders;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces

    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static enum Method {
        Get, Post, Put, Delete;

        public static Method lookup(String method) {
            for (Method m : Method.values()) {
                if (m.name().equalsIgnoreCase(method)) {
                    return m;
                }
            }
            return null;
        }
    }
}
