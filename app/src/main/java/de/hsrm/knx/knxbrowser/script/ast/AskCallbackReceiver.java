package de.hsrm.knx.knxbrowser.script.ast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by Manuel Hermenau on 16.10.2016.
 */
public class AskCallbackReceiver extends BroadcastReceiver implements Callable<String> {
    private static int nextIntentId = 1;

    public static final String ASK_CALLBACK_INTENT = "de.hsrm.knxbrowser.intent.ASK_CALLBACK";
    public static final String EXTRA_CALLBACK_ID = "de.hsrm.knxbrowser.extra.ASK_CALLBACK_ID";
    public static final String EXTRA_ANSWER_TEXT = "de.hsrm.knxbrowser.extra.ASK_CALLBACK_ASNWER";
    private static Random random = new Random();

    private final Context mContext;
    private final Object lock = new Object();
    private final int myIntentId;
    String result = null;

    public AskCallbackReceiver(Context context, int id) {
        this.mContext = context;
        myIntentId = id;
    }

    public AskCallbackReceiver register(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ASK_CALLBACK_INTENT);
        mContext.registerReceiver(this, intentFilter);
        return this;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getIntExtra(EXTRA_CALLBACK_ID, -1);
        if(id == myIntentId) {
            mContext.unregisterReceiver(this);
            synchronized (lock) {
                result = intent.getStringExtra(EXTRA_ANSWER_TEXT);
                lock.notifyAll();
            }
        }
    }

    @Override
    public String call() throws Exception {
        try {
            synchronized (lock) {
                lock.wait(60*1000);
                if(result == null)
                    throw new RuntimeException("The User did not respond in time.");
                return result;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("The User did not respond in time.");
        }
    }
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public static synchronized int getNextIntentId() {
        return random.nextInt(0xF00BAA);
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
