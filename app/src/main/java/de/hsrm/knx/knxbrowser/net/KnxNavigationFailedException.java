package de.hsrm.knx.knxbrowser.net;

import obix.Obj;

/**
 * Created by Manuel Hermenau on 27.09.2016.
 */
public class KnxNavigationFailedException extends RuntimeException {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private final Obj obj;
    // ===========================================================
    // Constructors
    // ===========================================================
    public KnxNavigationFailedException(Obj target){
        super();
        obj = target;
    }
    public KnxNavigationFailedException(Obj target, Throwable reason){
        super(reason);
        obj = target;
    }
    public KnxNavigationFailedException(Obj target, String message){
        super(message);
        obj = target;
    }
    public KnxNavigationFailedException(Obj target, String message, Throwable reason){
        super(message,reason);
        obj = target;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
