package de.hsrm.knx.knxbrowser.maps;

import android.graphics.Path;

import java.util.Map;

import de.hsrm.knx.knxbrowser.BeaconId;

/**
 * Created by Manuel Hermenau on 30.08.2016.
 */
public class MapBasedOutlineProvider implements OutlineProvider {
    private static final Path EMPTY_PATH = new Path();

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final Map<BeaconId, Path> outlines;

    // ===========================================================
    // Constructors
    // ===========================================================

    public MapBasedOutlineProvider(Map<BeaconId, Path> map){
        this.outlines = map;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public Path getOutline(BeaconId id) {
        Path result = outlines.get(id);
        if(result == null)
            result = EMPTY_PATH;
        return result;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
