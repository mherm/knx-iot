package de.hsrm.knx.knxbrowser;

import android.app.Application;
import android.content.Context;

import com.github.pedrovgs.lynx.LynxShakeDetector;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.config.ACRAConfiguration;
import org.acra.config.ConfigurationBuilder;

/**
 * Created by Manuel Hermenau on 29.06.2016.
 */
@ReportsCrashes(
        formUri = "",
        mailTo = "mail@manuelhermenau.de",
        mode = ReportingInteractionMode.TOAST,
        resToastText = de.hsrm.knx.knxbrowser.R.string.app_crash_toast
)
public class KnxBrowserApplication extends Application
{

        @Override
        public void onCreate() {
            super.onCreate();
            //To have an Activity that shows Logcat output, uncomment these lines.
            //Shaking the device will open the Activity
            //LynxShakeDetector lynxShakeDetector = new LynxShakeDetector(this);
            //lynxShakeDetector.init();
        }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        // Create an ConfigurationBuilder. It is prepopulated with values specified via annotation.
        // Set any additional value of the builder and then use it to construct an ACRAConfiguration.
        final ACRAConfiguration config = new ConfigurationBuilder(this)
                .build();

        // Initialise ACRA
        ACRA.init(this, config);
    }

}
