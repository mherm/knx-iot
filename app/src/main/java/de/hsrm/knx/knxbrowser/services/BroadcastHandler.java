package de.hsrm.knx.knxbrowser.services;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.hsrm.knx.knxbrowser.db.DbHelper;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class BroadcastHandler extends BroadcastReceiver  {

    public static final String TAG = BroadcastHandler.class.getName();

    private static final List<IntentConsumer> mConsumers = new LinkedList<>();

    static{
        mConsumers.addAll(Arrays.asList(
                new WifiIntentConsumer(),
                new ScriptIntentConsumer()
        ));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //TODO wakeful receiver
        for(IntentConsumer c : mConsumers)
            if(c.consume(context, intent))
                return;
    }

    private static class WifiIntentConsumer implements IntentConsumer {
        public boolean consume(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {

                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info != null) {
                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                    Intent i = new Intent(context, KnxBackgroundMgmtService.class);
                    intent.putExtra(WifiManager.EXTRA_NETWORK_INFO, info);
                    if (info.isConnected()) {
                        String ssid = wifiInfo.getSSID();
                        Log.d(TAG, "Connected to "+ssid);
                        i.setAction(WifiChangeReceiver.INTENT_WIFI_CONNECTED);
                        intent.putExtra(WifiManager.EXTRA_WIFI_INFO, wifiInfo);
                    } else {
                        Log.d(TAG, "Disconnected from WiFi");
                        i.setAction(WifiChangeReceiver.INTENT_WIFI_DISCONNECTED);
                    }
                    context.startService(i);
                }
                return true;
            }
            return false;
        }
    }
    private static class ScriptIntentConsumer implements IntentConsumer {
        public boolean consume(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(DbHelper.INTENT_SCRIPT_ENABLED)
                    || action.equals(DbHelper.INTENT_SCRIPT_DISABLED)) {
                    Intent i = new Intent(intent);
                    i.setComponent(new ComponentName(context, KnxBackgroundMgmtService.class));
                    context.startService(i);
                    return true;
                }
            return false;
        }
    }

    private interface IntentConsumer{
    /**
     *
     * @param context
     * @param intent
     * @return true, if the method consumed the intent and it should not be processed any further, false otherwise
     */
        public boolean consume(Context context, Intent intent);
    }

}
