package de.hsrm.knx.knxbrowser.script.ast;

import org.joda.time.DateTime;

import de.hsrm.knx.knxbrowser.Utils;

/**
 * Created by Manuel Hermenau on 07.10.2016.
 */
public class TimeTrigger implements Trigger {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    DateTime mTime;
    int mTimeWithoutDate;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public void setTime(DateTime time){
        mTime = time;
        mTimeWithoutDate = Utils.eraseDate(time);
    }
    public void setTimeWithoutDate(int time){
        mTimeWithoutDate = time;
    }

    public int getTimeWithoutDate(){
        return mTimeWithoutDate;
    }
    public DateTime getTime(){
        return mTime;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public boolean equals(Object t){
        if(t == null)
            return false;
        if(t.getClass() != this.getClass())
            return false;
        TimeTrigger tt = (TimeTrigger)t;
        return mTimeWithoutDate == ((TimeTrigger) t).mTimeWithoutDate;
    }

    public int hashCode(){
        return mTimeWithoutDate;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
