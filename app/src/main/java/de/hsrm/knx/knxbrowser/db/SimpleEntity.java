package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 12.10.2016.
 */
public abstract class SimpleEntity implements Entity {
    private int mId = -1;

    @Override
    public void setId(int id) {
        mId = id;
    }

    @Override
    public int getId() {
        return mId;
    }

}
