package de.hsrm.knx.knxbrowser.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import static android.R.attr.action;

/**
 * Created by Manuel Hermenau on 12.10.2016.
 */
public class WifiChangeReceiver extends BroadcastReceiver {
    public static final String INTENT_WIFI_CONNECTED    = "de.hsrm.knx.knxbrowser.trigger.WIFI_CONNECTED";
    public static final String INTENT_WIFI_DISCONNECTED = "de.hsrm.knx.knxbrowser.trigger.WIFI_DISCONNECTED";
    private final WifiChangeListener mListener;

    public WifiChangeReceiver(WifiChangeListener listener){
        mListener = listener;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        if(INTENT_WIFI_CONNECTED.equals(action))
            handleWifiConnected(intent);
        else if(INTENT_WIFI_DISCONNECTED.equals(action))
            handleWifiDisconnected(intent);

        //TODO WakefulReceiver?
    }

    private final void handleWifiDisconnected(Intent intent) {
        mListener.onWifiDisconnected(intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO));
    }

    final void handleWifiConnected(Intent intent){
        final NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        final WifiInfo wifi = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
        mListener.onWifiConnected(networkInfo, wifi);
    }

    /**
     * Registers this receiver to the given context
     * @param context
     */
    public void register(Context context){
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiChangeReceiver.INTENT_WIFI_CONNECTED);
        filter.addAction(WifiChangeReceiver.INTENT_WIFI_DISCONNECTED);
        context.registerReceiver(this, filter);
    }
}
