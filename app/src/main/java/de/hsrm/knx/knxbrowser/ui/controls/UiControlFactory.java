package de.hsrm.knx.knxbrowser.ui.controls;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import de.hsrm.knx.knxbrowser.model.Datapoint;
import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.util.KnxBrowserUtil;
import obix.Obj;
import obix.Uri;
import obix.Val;

/**
 * Created by Manuel Hermenau on 20.09.2016.
 */
public class UiControlFactory implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = UiControlFactory.class.getName();
    private final Context mContext;
    private final Handler mHandler;
    private KnxObixBrowser mKnxObixBrowser;

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================
    public UiControlFactory(Context context) {
        this.mContext = context;
        this.mKnxObixBrowser = KnxObixBrowser.fromContext(context);
        this.mHandler = new Handler(context.getMainLooper());
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    public View make(Obj obj, View view){
        try {
            if(obj.is(Datapoint.ContractUri)){
                Datapoint dtp = fromObj(obj);
                if(dtp.getGroup() == Datapoint.GROUP_B1) {
                    final int minor = dtp.getDtp();
                    if(minor < 5)
                        return makeToggleButton(dtp, obj, view);
                    else if(minor == 15 || minor == 16)
                        return makeUnaryButton(dtp, obj, view);
                    else
                        return makeBinaryButton(dtp, obj, view);
                }

                if(dtp.getGroup() == Datapoint.GROUP_U8){
                    return makeSlider(dtp,obj,view);
                }

                return makeDataTextView(dtp, obj, view);
            }
            if(obj.is("/knx/Manufacturer")){
                getTitle(obj, view);
            }
        }catch (Exception e){
            Log.w(TAG, e);
        }
        return null;
    }

    private void getTitle(Obj obj, View view) {
        buildControl(Datapoint.DTP_1_001, obj, view, (v,o,d,p)->null);
    }

    ;

    private View makeSlider(Datapoint dtp, Obj obj, View view) {
       SliderCallback callback = new SliderCallback(UnitConverter.Percent, mContext, mKnxObixBrowser);
        return buildControl(dtp,obj,view,callback);
    }

    InitControlCallback createDataTextView = new InitControlCallback() {
        @Override
        public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent) {
            if(value != null) {
                TextView view = new TextView(mContext);
                String val = value.encodeJava();
                view.setText(val);
                return view;
            }
            return null;
        }
    };
    private View makeDataTextView(Datapoint dtp, Obj obj, View view) {
        return buildControl(dtp, obj, view, createDataTextView);
    }

    private View makeUnaryButton(Datapoint dtp, Obj obj, View view) {
        return buildControl(dtp, obj, view, createUnaryButton);
    }
    private View makeBinaryButton(Datapoint dtp, Obj obj, View view) {
        return buildControl(dtp, obj, view, createBinaryButton);
    }

    interface InitControlCallback {
        public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent);
    }

    InitControlCallback createToggleButton = new InitControlCallback() {
        @Override
        public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent) {
            CompoundButton tb = new SwitchCompat(mContext);
            tb.setChecked(value.getBool());
            tb.setVisibility(View.VISIBLE);
            if(obj.isWritable()) {
                tb.setOnCheckedChangeListener(new ToggleSwitchListener(obj, mKnxObixBrowser, dtp));
            }else{
                tb.setEnabled(false);
            }
            return tb;
        }
    };

    InitControlCallback createUnaryButton = new InitControlCallback() {
        @Override
        public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent) {
            Button btn = new Button(mContext);
            btn.setVisibility(View.VISIBLE);
            if(obj.isWritable()) {
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        obj.get(dtp.objName()).setBool(true);
                        btn.setEnabled(false);
                        mKnxObixBrowser.submit(obj).<Obj>then((o)->btn.setEnabled(true));
                    }
                });
            }else{
                btn.setEnabled(false);
            }
            return btn;
        }
    };
    InitControlCallback createBinaryButton = new InitControlCallback() {
        @Override
        public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent) {
            if(obj.isWritable()) {
                LinearLayout layout = new LinearLayout(mContext);
                ToggleButton btn0 = new ToggleButton(mContext);
                ToggleButton btn1 = new ToggleButton(mContext);
                layout.addView(btn0);
                layout.addView(btn1);
                btn0.setTextOn(dtp.getEncodingEnum()[0]);
                btn0.setTextOff(dtp.getEncodingEnum()[0]);
                btn1.setTextOn(dtp.getEncodingEnum()[1]);
                btn1.setTextOff(dtp.getEncodingEnum()[1]);
                btn0.setChecked(false);
                btn1.setChecked(false);
                btn0.setOnClickListener(new BinaryButtonListener(false,obj,layout,btn1,mKnxObixBrowser,dtp));
                btn1.setOnClickListener(new BinaryButtonListener(true,obj,layout,btn0,mKnxObixBrowser,dtp));
                return layout;
            }else{
                //layout.setEnabled(false);
                return null;
            }
        }
    };


    private View makeToggleButton(Datapoint dtp, Obj obj, View view) {
        InitControlCallback toggleOrBinary = new InitControlCallback() {
            @Override
            public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent) {

                if(obj.isReadable())
                    return createToggleButton.initControl(value,obj, dtp, parent);
                else
                    return createBinaryButton.initControl(value,obj,dtp,parent);
            }
        };
        return buildControl(dtp, obj, view, toggleOrBinary);
    }

    @NonNull
    private View buildControl(Datapoint dtp, Obj obj, View view, InitControlCallback initCallback) {
        Log.d(TAG, "Building Control for "+obj.toString());
        RelativeLayout rl = new RelativeLayout(mContext);
        final ProgressBar pb = new ProgressBar(mContext);
        pb.setIndeterminate(true);
        rl.addView(pb);
        Log.d(TAG, "Getting current value of "+obj.toString());
        mKnxObixBrowser
                .navigate(obj)
                .<Obj>then( (o)-> {
                            Val value = (Val) o.get(dtp.objName());
                            Log.d(TAG, "Current value of "+obj.toString()+" is "+value);
                            View initializedBtn = initCallback.initControl(value, o, dtp, rl);
                            if(initializedBtn != null) {
                                rl.addView(initializedBtn);
                                initializedBtn.setVisibility(View.VISIBLE);
                            }
                            pb.setVisibility(View.GONE);

                            //Updating Display text with correct case
                            String newText = KnxBrowserUtil.trimContent(o.getDisplayName());
                            String display = o.getDisplay();
                            if(display != null && !display.trim().isEmpty())
                                newText += " "+display;
                            ((TextView)view.findViewById(R.id.obj_list_name))
                                    .setText(newText);
                        }
                )
                .fail((ex) -> {
                    //TODO Do something?
                });
        return rl;
    }

    private Datapoint fromObj(Obj obj) {
        final Uri[] contracts = obj.getIs().list();
        for (Uri uri : contracts) {
            if (uri.get().startsWith("knx:DPST"))
                return Datapoint.parse(uri.get());
        }
        return null;
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(mContext.getString(R.string.preference_gatewayip_key))){
            mKnxObixBrowser = KnxObixBrowser.fromContext(mContext);
        }
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
