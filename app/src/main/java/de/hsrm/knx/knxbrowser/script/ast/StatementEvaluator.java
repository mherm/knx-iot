package de.hsrm.knx.knxbrowser.script.ast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.github.kevinsawicki.http.HttpRequest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hsrm.knx.knxbrowser.model.Datapoint;
import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.ui.activities.LocationServiceConnector;
import de.hsrm.knx.knxbrowser.ui.activities.NotificationActivity;
import obix.Bool;
import obix.Int;
import obix.Obj;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class StatementEvaluator extends AstVisitor<Literal> {
    private final static Pattern UserPassPattern = Pattern.compile("^.*?:\\/\\/(([^:]*?):(.*?)@)?.*");
    private static final String TAG = StatementEvaluator.class.getName();
    private final Context mContext;
    private final ExecutorService mExecutor = Executors.newFixedThreadPool(5); //For ParallelBlocks
    private final NotificationManager mNotificationManager ;

    private ThreadLocal<KnxObixBrowser> mBrowser =  new ThreadLocal<KnxObixBrowser>() {
        @Override protected KnxObixBrowser initialValue() {
            return KnxObixBrowser.fromContext(mContext);
        }
    };


    public StatementEvaluator(Context context) {
        mContext = context;
        mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public Literal visit(AskStatement stmt) {
        Log.d(TAG, "Ask: "+stmt.message);
        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 2000 milliseconds
        v.vibrate(2000);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        builder
                // Show controls on lock screen even when user hides sensitive content.
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.drawable.ic_speaker_notes_white_24dp);

        int callbackId = AskCallbackReceiver.getNextIntentId();

        int i=0;
        for(String option : stmt.switchNode.switchNodes.keySet()){
            PendingIntent pi = createAskCallbackPendingIntent(option, callbackId, i++);
            builder.addAction(R.drawable.placeholder_1, option, pi);
        }
        Notification notification = builder.setContentTitle("KNX Script requires action")
        .setContentText(stmt.message)
                .setContentIntent(createAskStatementContentIntent(stmt, callbackId))
        .build();

        Future<String> future = mExecutor.submit(new AskCallbackReceiver(mContext, callbackId).register());
        String answer;
        final int notificationId = 0xC0CAC01A | callbackId;
        mNotificationManager.notify(notificationId, notification);
        try {
            answer = future.get();
        }catch (Exception e){
            mNotificationManager.cancel(notificationId);
            throw new RuntimeException(e);
        }
        mNotificationManager.cancel(notificationId);
        Log.d(TAG, "Answer was:"+ answer);
        return visit(stmt.switchNode.switchNodes.get(answer));
    }

    private PendingIntent createAskStatementContentIntent(AskStatement stmt, int callBackId) {
        Intent intent = new Intent(mContext, NotificationActivity.class);
        final Random random = new Random();
        intent.setAction(String.valueOf(random.nextInt(543254)));
        intent.putExtra(NotificationActivity.EXTRA_CALLBACK_ID, callBackId);
        intent.putExtra(NotificationActivity.EXTRA_NOTIFICATION_TITLE, stmt.message);
        final Set<String> keys = stmt.switchNode.switchNodes.keySet();
        String[] options = keys.toArray(new String[keys.size()]);
        intent.putExtra(NotificationActivity.EXTRA_NOTIFICATION_OPTIONS, options);

        return PendingIntent.getActivity(mContext, new Random().nextInt(543254), intent, PendingIntent.FLAG_ONE_SHOT);
    }

    private PendingIntent createAskCallbackPendingIntent(String value, int callbackId, int pos) {
        Intent intent = new Intent(AskCallbackReceiver.ASK_CALLBACK_INTENT);
        intent.putExtra(AskCallbackReceiver.EXTRA_CALLBACK_ID, callbackId);
        intent.putExtra(AskCallbackReceiver.EXTRA_ANSWER_TEXT, value);
        return PendingIntent.getBroadcast(mContext, callbackId+pos, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @Override
    public Literal visit(Block block) {
        Literal result = Literal.TRUE;
        for(Statement stmt : block.mStatements){
            result = visit(stmt);
        }
        return result;
    }

    public Literal visit(CurrentRoomStatement smt){
        BlockingQueue<String> result = new ArrayBlockingQueue(1);
        new LocationServiceConnector(mContext).requestCurrentRoom(currentRoom ->
                result.add(currentRoom.represent())
        );
        try {
            return new Literal<>(result.take());
        }catch (InterruptedException e){
            return new Literal<>(BeaconId.NONE.represent());
        }
    }

    @Override
    public Literal visit(Expression stmt) {
        final Literal left = visit(stmt.left);
        final Literal right = visit(stmt.right);
        boolean result = stmt.op.evaluate(left, right);
        return new Literal<>(result);
    }

    @Override
    public Literal visit(EnvironmentFilter stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }

    @Override
    public Literal visit(HttpStatement stmt) {
        Matcher matcher = UserPassPattern.matcher(stmt.mUrl);
        boolean withAuth = false;
        String user="",pass="";
        String url = stmt.mUrl;
        if(matcher.find()){
            withAuth = true;
            user = matcher.group(2);
            pass = matcher.group(3);
            url = new StringBuilder(stmt.mUrl).delete(matcher.start(1), matcher.end(1)).toString();

            Log.d(TAG, "Using Basic auth");
            Log.d(TAG, "User: "+user);
            Log.d(TAG, "Password: ******");
        }

        Log.d(TAG, stmt.mMethod+" "+url);

        HttpRequest req = new HttpRequest(url, stmt.mMethod.name().toUpperCase());

        if(stmt.mHeaders != null)
            req.headers(parseHeaders(stmt.mHeaders));

        if(withAuth)
            req.basic(user, pass);

        if(stmt.mMethod == HttpStatement.Method.Post || stmt.mMethod == HttpStatement.Method.Put)
            req.send(stmt.mContent);

        return new Literal<>(req.body());
    }

    private Map<String, String> parseHeaders(String headers) {
        Map<String, String> map = new HashMap<>();
        for(String line :  headers.split("\\r?\\n")){
            String[] parts = line.split(":");
            map.put(parts[0], parts[1]);
        }
        return map;
    }

    @Override
    public Literal visit(IfStatement stmt) {
        final Literal<Boolean> exprResult = (Literal<Boolean>) visit(stmt.mExpression);
        if(exprResult.get())
            return visit(stmt.mIfBlock);
        else
            return visit(stmt.mElseBlock);
    }

    @Override
    public Literal visit(KnxScript stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }

    @Override
    public Literal visit(KnxReadStatement stmt) {
        final AtomicReference<Obj> result = new AtomicReference<>();
        mBrowser.get()
                .navigate(stmt.target, false)
                .then(o -> {
                    Obj resultObj = o.get(stmt.mDatapoint.objName());
                    result.set(resultObj);
                    Log.d(TAG, "Result in callback: " + result + " - Thread: " + Thread.currentThread());
                })
                .fail((t) -> {
                    throw new RuntimeException(t);
                });
        Log.d(TAG, "Result outside callback: "+result+" - Thread: "+Thread.currentThread());
        return toLiteral(result.get());
    }


    @Override
    public Literal visit(KnxWriteStatement stmt) {
        final AtomicReference<Obj> result = new AtomicReference<>();
        try {
            setLiteral(stmt.mDatapoint, stmt.target, stmt.literals);
            mBrowser.get()
                    .submit(stmt.target, false)
                    .<Obj>then(o -> result.set(o))
                    .fail((t)->{throw new RuntimeException(t);})
                    .waitSafely();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        return toLiteral(result.get());
    }

    // ===========================================================
    // Methods
    // ===========================================================

    protected Literal toLiteral(Obj obj) {
        if(obj == null){
            return null;
        }
        if(obj.isBool()){
            return new Literal<>(obj.getBool());
        }
        if(obj.isReal()){
            return new Literal<>(obj.getReal());

        }
        if(obj.isInt()){
            return new Literal<>(obj.getInt());
        }
        if(obj.isStr()){
            return new Literal<>(obj.getStr());
        }
        return null;
    }

    private Obj setLiteral(Datapoint datapoint, Obj obj, List<Literal> literal) {
        final int group = datapoint.getGroup();
        if(group == Datapoint.GROUP_B1){
            Literal<Boolean> val = (Literal<Boolean>) literal.get(0);
            return setBool(datapoint, obj, val);
        }
        else if(group == Datapoint.GROUP_B1U3){
            Literal<Boolean> boolVal = (Literal<Boolean>) literal.get(0);
            Literal<Integer> intVal = (Literal<Integer>) literal.get(0);
            setBool(datapoint, obj, boolVal);
            setInt(datapoint, obj, intVal);
            return obj;
        }
        throw new RuntimeException("Not yet implemented: write Datapoint "+datapoint.objName());
    }

    private Obj setBool(Datapoint datapoint, Obj obj, Literal<Boolean> val) {
        //TODO Obj klonen? Script-Objekt wird veändert!!
        Obj existingObj = obj.get(datapoint.objName());
        if(existingObj != null)
            existingObj.setBool(val.get());
        else
            obj.add(new Bool(datapoint.objName(), val.get()));
        return obj;
    }
    private Obj setInt(Datapoint datapoint, Obj obj, Literal<Integer> val) {
        //TODO Obj klonen? Script-Objekt wird veändert!!
        Obj existingObj = obj.get(datapoint.objName());
        if(existingObj != null)
            existingObj.setInt(val.get());
        else
            obj.add(new Int(datapoint.objName(), val.get()));
        return obj;
    }

    @Override
    public Literal visit(Literal stmt) {
        return stmt;
    }

    @Override
    public Literal visit(NotifyStatement stmt) {
        //        NotificationCompat.Builder builder;
        final String title = stmt.title == null ? "Hey there" : stmt.title;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext)
                        .setSmallIcon(R.drawable.ic_speaker_notes_white_24dp)
                        .setContentTitle(title)
                        .setContentText(stmt.message);
// Creates an explicit intent for an Activity in your app
        mNotificationManager.notify(0x600DDEED, mBuilder.build());
// mId allows you to update the notification later on.
        Log.d("NotifyStatement", stmt.message);
        return Literal.TRUE;
    }

    @Override
    public Literal visit(Op stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }

    @Override
    public Literal visit(ParallelBlock stmt) {
        try {
            List<Future> futures = new LinkedList<>();
            for(Statement statement : stmt.mStatements)
                futures.add(mExecutor.submit(()->visit(statement)));

            //Wait till all Threads complete, max 60 seconds.
            long now = System.currentTimeMillis();
            long breakOn = now + 60 * 1000;
            for(Future f : futures){
                long maxExecutionTime = breakOn - System.currentTimeMillis();
                if(maxExecutionTime > 0)
                    f.get(maxExecutionTime, TimeUnit.MILLISECONDS);
            }
            return Literal.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Literal visit(RoomTrigger stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }

    @Override
    public Literal visit(Rule stmt) {
        return visit(stmt.mRunBlock);
    }

    @Override
    public Literal visit(SwitchNode stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }

    @Override
    public Literal visit(TimerangeFilter stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }

    @Override
    public Literal visit(TimeTrigger stmt) {
        throw new RuntimeException("Not a Statement: "+stmt.toString());
    }
    @Override
    public Literal visit(WaitStatement stmt) {
        try {
            Thread.sleep(stmt.milliseconds);
        } catch (InterruptedException e) {
            Log.e(TAG, "Sleep interrupted",e);
        }
        return Literal.TRUE;
    }
}
