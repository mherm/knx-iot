package de.hsrm.knx.knxbrowser.script.ast;

import android.content.Context;

import org.joda.time.DateTime;

import de.hsrm.knx.knxbrowser.Utils;

/**
 * Created by Manuel Hermenau on 07.10.2016.
 */
public class TimerangeFilter implements ActivationFilter{
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    DateTime mStartTime;
    DateTime mEndTime;
    // ===========================================================
    // Constructors
    // ===========================================================
    public TimerangeFilter(DateTime startTime, DateTime endTime){
        mStartTime = startTime;
        mEndTime = endTime;
    }

    @Override
    public boolean matches(Context context) {
        int currentTime = Utils.eraseDate(new DateTime());
        return currentTime > Utils.eraseDate(mStartTime) && currentTime < Utils.eraseDate(mEndTime);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
