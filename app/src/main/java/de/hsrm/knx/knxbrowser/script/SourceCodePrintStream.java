package de.hsrm.knx.knxbrowser.script;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import de.hsrm.knx.knxbrowser.script.ast.AstVisitor;
import de.hsrm.knx.knxbrowser.script.ast.ComplexStatement;
import de.hsrm.knx.knxbrowser.script.ast.ScriptElement;

/**
 * Created by Manuel Hermenau on 08.10.2016.
 */
public class SourceCodePrintStream extends PrintStream {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final int TAB_WIDTH = 5;
    private final String indent = "     ";
    private int indentDepth = 0;

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================
    public SourceCodePrintStream(File file) throws FileNotFoundException {
        super(file);
    }

    public SourceCodePrintStream(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    public SourceCodePrintStream(OutputStream out) {
        super(out);
    }

    public SourceCodePrintStream(OutputStream out, boolean autoFlush) {
        super(out, autoFlush);
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    public synchronized void print(String str){
        super.print(str);
    }

    /**
     * Prints a newline.
     */
    public void println() {
        newline();
    }

    public synchronized void println(String str) {
        print(str);
        newline();
    }

    /**
     * Put the line separator String onto the print stream.
     */
    private void newline() {
        print(System.lineSeparator());
        printIndent();
    }


    // ===========================================================
    // Methods
    // ===========================================================
    public void indent(){
        indentDepth++;
    }
    public void unIndent(){
        indentDepth--;
    }
    protected void printIndent() {
        for(int i=0; i<indentDepth;i++)
            print(indent);
    }

    public <T> void printOptionalBlock(List<? extends ScriptElement> elements, AstVisitor<T> visitor){
        if(elements == null || elements.size() == 0)
            return;
        if (elements.size() == 1 && !(elements.get(0) instanceof ComplexStatement)) {
            visitor.visit(elements.get(0));
        }
        else{
            printBlock(elements, visitor);
        }
    }
    public <T> void printBlock(List<? extends ScriptElement> elements, AstVisitor<T> visitor){
        printBlockStart();
        for(int i=0; i<elements.size();i++){
            ScriptElement el = elements.get(i);
            visitor.visit(el);
            if(i < elements.size()-1)
                println();
        }
        printBlockEnd();
    }

    public void printBlockEnd() {
        unIndent();
        println();
        print("} ");
    }

    public void printBlockStart() {
        print("{");
        indent();
        println();
    }

    public void printStringLiteral(String str) {
        print("\"");
        print(str);
        print("\"");
    }

    public void printSpace() {
        print(" ");
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
