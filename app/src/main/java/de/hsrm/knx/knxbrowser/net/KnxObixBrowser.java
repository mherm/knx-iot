package de.hsrm.knx.knxbrowser.net;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import org.jdeferred.Deferred;
import org.jdeferred.DeferredManager;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.Promise;
import org.jdeferred.android.AndroidDeferredManager;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;
import org.jdeferred.android.AndroidExecutionScopeable;
import org.jdeferred.impl.DeferredObject;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import de.hsrm.knx.knxbrowser.Environment;
import de.hsrm.knx.knxbrowser.EnvironmentManager;
import de.hsrm.knx.knxbrowser.Utils;
import de.hsrm.knx.knxbrowser.util.URIUtil;

import obix.Obj;
import obix.Uri;
import obix.io.KnxObixDecoder;
import obix.io.KnxObixEncoder;
import obix.net.KnxObixSession;

/**
 * Created by Manuel Hermenau on 16.09.2016.
 */
public class KnxObixBrowser implements Parcelable {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = KnxObixBrowser.class.getName();
    private static final int MAX_TRIES = 10;

    // ===========================================================
    // Fields
    // ===========================================================
    private DeferredManager dm = new AndroidDeferredManager();
    private KnxObixSession mSession;
    private Obj mLobbyObj;
    private Uri mLobbyUri;
    private String mHost;
    private Obj mState = null;
    private Set<KnxBrowserView> mViews = new HashSet<>();
    private final AtomicBoolean isInitialised = new AtomicBoolean(false);

    private Stack<Obj> mHistory = new Stack<>();

    // ===========================================================
    // Constructors
    // ===========================================================
    private KnxObixBrowser(URL host)  {
        try {
            mLobbyUri = toObixUri(host);
            mSession = new KnxObixSession(mLobbyUri);
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    @NonNull
    private Uri toObixUri(URL host) throws MalformedURLException {
        mHost = host.toString();
        if(!mHost.endsWith("/"))
            mHost = host.toString()+"/";

        if (!mHost.endsWith("installations/") )
            if(mHost.endsWith("obix/"))
                mHost = new URL(host, "installations/").toString();
           else
                mHost = new URL(host, "obix/installations/").toString();
        return new Uri(mHost);
    }

    protected KnxObixBrowser(Parcel in) {
        mHost = in.readString();
        mState = KnxObixDecoder.fromString(in.readString());
        mLobbyObj = KnxObixDecoder.fromString(in.readString());
        List<String> history = new LinkedList<>();
        in.readStringList(history);
        mHistory = new Stack<>();
        for(String obj : history){
            mHistory.push(KnxObixDecoder.fromString(obj));
        }
        initSession(mState.getRoot());
    }

    public boolean isAtHome(){
        return mState == null || mLobbyUri.equals(mState.getHref());
    }

    public static final Creator<KnxObixBrowser> CREATOR = new Creator<KnxObixBrowser>() {
        @Override
        public KnxObixBrowser createFromParcel(Parcel in) {
            return new KnxObixBrowser(in);
        }

        @Override
        public KnxObixBrowser[] newArray(int size) {
            return new KnxObixBrowser[size];
        }
    };

    public static KnxObixBrowser fromContext(Context context){
        URL gateway;
        final Environment activeEnvironment = EnvironmentManager.getInstance(context).getActiveEnvironment();
        gateway = activeEnvironment.getHost();
        return from(gateway);
    }

    public static KnxObixBrowser from(URL host){
        Log.d(TAG, "Creating new KnxObixBrowser instance...");
        KnxObixBrowser instance = new KnxObixBrowser(host);
        instance.initSession()
                .then(instance.UPDATE_STATE)
                .then(instance.NOTIFY_VIEWS_CALLBACK);
        return instance;
    }

    public static KnxObixBrowser from(URL host, Stack<Obj> history){
        if(history == null || history.isEmpty())
            return from(host);
        Log.d(TAG, "Restoring KnxObixBrowser instance...");
        KnxObixBrowser instance = new KnxObixBrowser(host);
        instance.mState = history.pop();
        instance.initSession(instance.mState.getRoot());
        instance.mHistory = history;
        return instance;
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public KnxObixBrowser addView(KnxBrowserView view){
        this.mViews.add(view);
        //Instantly notify view if there is a cached state
        if(mState != null)
            view.onNewObj(mState);
        return this;
    }

    public String getHost() {
        return mHost;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public Promise<Boolean, Throwable, Void> isAvailable(){
        Deferred<Boolean, Throwable,Void> dfd = new DeferredObject<>();
        refresh().<Obj>then((o)->dfd.resolve(true)).fail((e)->dfd.resolve(false));
        return dfd.promise();
    }

    private Promise<Obj, Throwable, Void> initSession(final Obj lobby) {
        if(lobby == null)
            return initSession();
        mLobbyObj = lobby;
        mSession.open(lobby);
        return dm.<Obj>when(()->lobby);
    }

    private Promise<Obj, Throwable, Void> initSession() {
       Log.d(TAG, "Initiating KnxObixBrowser...");
       return dm.<Obj>when(()-> {
            triggerNavigationStart();
           int tries = 0;
           while (tries < MAX_TRIES) {
               try {
                   mSession.open();
                   Obj lobby = mSession.getLobby();
                   Log.d(TAG, "Initiation successful. Root Uri: " + lobby);
                   mLobbyObj = lobby;
                   return lobby;
               } catch (Exception e) {
                   e.printStackTrace();
                   tries++;
               }
           }
           throw new ConnectException(mSession.getLobbyUri().toString());
       }).always((s,d,r)->{synchronized (isInitialised){isInitialised.set(true); isInitialised.notifyAll();}})
       .fail(new NavigationFailedCallback(mLobbyUri));
   }

    public Obj[] list(){
        return mState.list();
    }

    public Promise<Obj, Throwable, Void> navigate(Obj item) {
        return navigate(item, true);
    }
    public Promise<Obj, Throwable, Void> navigate(Obj item, boolean async) {

        final Callable<Obj> asyncTask = () -> {
            Log.d(TAG, "Navigating to " + item.getHref());
            triggerNavigationStart();
            try {
                return mSession.follow(item);
            } catch (Exception e) {
                throw new KnxNavigationFailedException(item, e);
            }
        };
        Promise result;
        if(async)
            result= dm.<Obj>when(asyncTask);
        else{
            try {
                final Obj resultObj = asyncTask.call();
                result = new DeferredObject().resolve(resultObj);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }
        result.then(UPDATE_STATE)
        .then(NOTIFY_VIEWS_CALLBACK)
        .fail(new NavigationFailedCallback(item));
        return result;
    }

    private void triggerNavigationStart() {
        for(KnxBrowserView view : mViews)
            view.onNavigationStart();
    }

    public Set<KnxBrowserView> getViews() {
        return mViews;
    }

    public Promise<Obj, Throwable, Void> submit(Obj obj) {
        return submit(obj, true);
    }
    public Promise<Obj, Throwable, Void> submit(Obj obj, boolean async) {
        final Callable<Obj> asyncTask = () -> {
            triggerNavigationStart();
            try {
                return mSession.write(obj);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
        final Promise<Obj, Throwable, Void> result;
        if(async)
            result = dm.<Obj>when(asyncTask);
        else{
            try {
                Obj resultObj = asyncTask.call();
                result = new DeferredObject().resolve(resultObj);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }
        return result;
    }

    public  Promise<Obj, Throwable, Void> refresh() {
        return navigate(mState);
    }

    public Promise<Obj, Throwable, Void> navigateBack(boolean reload){
        if(mHistory.isEmpty()) {
            return dm.<Obj>when(()->{throw new RuntimeException("History is empty");});
        }
        mState = mHistory.pop();
        if(reload){
            return refresh();
        }
        return dm.<Obj>when(()->mState).then(NOTIFY_VIEWS_CALLBACK);
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    private final DoneCallback<Obj> NOTIFY_VIEWS_CALLBACK = new DoneCallback<Obj>() {
        @Override
        public void onDone(Obj result) {
            Log.d(TAG, "Navigation successful. Result: "+result.toString());
            result.dump();

            Log.v(TAG, "Notifying "+getViews().size()+" Views...");
            for(KnxBrowserView view : KnxObixBrowser.this.getViews()){
                view.onNewObj(result);
            }
            Log.v(TAG, "Notified "+getViews().size()+" Views successfully");
        }
    };
    private final DoneCallback<Obj> UPDATE_STATE = new DoneCallback<Obj>() {
        @Override
        public void onDone(Obj result) {
            Obj currentState = KnxObixBrowser.this.mState;
            if(currentState != null && !uriEquals(currentState,result))
                KnxObixBrowser.this.mHistory.push(currentState);
            KnxObixBrowser.this.mState = result;
        }
        private boolean uriEquals(Obj currentState, Obj result) {
            String oldUri = currentState.getHref().encodeVal();
            String newUri = result.getHref().encodeVal();
            if(oldUri.endsWith("/"))
                oldUri = oldUri.substring(0, oldUri.length()-1);
            if(newUri.endsWith("/"))
                newUri = newUri.substring(0, newUri.length()-1);
            return oldUri.equals(newUri);
        }
    };

    public void removeView(KnxBrowserView view) {
        this.mViews.remove(view);
    }

    public URL resolveUri(Obj obj) {
        final Uri uri = mSession.resolveUri(obj);
        return URIUtil.makeURL(uri.toDisplayString());
    }

    public void waitForInit() {
        waitForInit(0);
    }
    public void waitForInit(long maxMs) {
        synchronized (isInitialised){
            if(!this.isInitialised.get()){
                try {
                    isInitialised.wait(maxMs);
                    if(!isInitialised.get())
                        throw new RuntimeException(this.getClass().getSimpleName()+" could not be instanciated within "+maxMs+"ms. Maybe there is no connection?");
                }catch (Exception e){
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private final class NavigationFailedCallback implements FailCallback<Throwable> {
        private final Obj target;

        NavigationFailedCallback(Obj target){
            this.target = target;
        }

        @Override
        public void onFail(Throwable t) {
            t.printStackTrace();
            KnxNavigationFailedException ex;
            if(t instanceof KnxNavigationFailedException)
                ex = (KnxNavigationFailedException) t;
            else
                ex = new KnxNavigationFailedException(target, t);
            for(KnxBrowserView view : getViews())
                view.onNavigationFailed(ex);
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mHost);
        dest.writeString(mState == null ? null : KnxObixEncoder.toString(mState));
        dest.writeString(mLobbyObj == null ? null : KnxObixEncoder.toString(mLobbyObj));
        dest.writeStringList(toStrings(mHistory));
    }

    private List<String> toStrings(Stack<Obj> history) {
        List<String> result=new ArrayList<>(history.size());
        for(int i=0; i<history.size(); i++)
            result.add(KnxObixEncoder.toString(history.get(i)));
        return result;
    }
}
