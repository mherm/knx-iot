package de.hsrm.knx.knxbrowser.script.ast;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Map;

import de.hsrm.knx.knxbrowser.script.SourceCodePrintStream;

/**
 * Created by Manuel Hermenau on 10.10.2016.
 */
public class KnxScriptWriter extends AstVisitor<Void> {
    private final SourceCodePrintStream os;

    public KnxScriptWriter(OutputStream out){
        os = new SourceCodePrintStream(out);
    }

    public static String writeOut(ScriptElement element){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        new KnxScriptWriter(bos).visit(element);
        return new String(bos.toByteArray());
    }

    @Override
    public Void visit(AskStatement stmt) {
        os.print("ask ");
        os.printStringLiteral(stmt.message);
        os.print(" ");
        visit(stmt.switchNode);
        return null;
    }

    @Override
    public Void visit(Block stmt) {
        os.printOptionalBlock(stmt.mStatements, this);
        return null;
    }

    @Override
    public Void visit(CurrentRoomStatement stmt) {
        os.print("current room");
        return null;
    }

    @Override
    public Void visit(Expression stmt) {
        visit(stmt.left);
        os.printSpace();
        visit(stmt.op);
        os.printSpace();
        visit(stmt.right);
        return null;
    }

    @Override
    public Void visit(HttpStatement stmt) {
        os.print("http ");
        os.print(stmt.mMethod.toString().toLowerCase());
        os.printSpace();
        os.print(stmt.mUrl);
        os.printSpace();
        if(stmt.mContent != null) {
            os.printStringLiteral(stmt.mContent);
            os.printSpace();
        }
        if(stmt.mHeaders != null) {
            os.print("headers=");
            os.printStringLiteral(stmt.mHeaders);
            os.printSpace();
        }
        return null;
    }

    @Override
    public Void visit(IfStatement stmt) {
        os.print("if ");
        visit(stmt.mExpression);
        os.printSpace();
        printOptionalIfBlock(stmt.mIfBlock);
        if(stmt.mElseBlock != null){
            os.print("else ");
            printOptionalIfBlock(stmt.mElseBlock);
        }else{
            os.println();
        }
        return null;
    }

    private void printOptionalIfBlock(Statement statement) {
        if(statement instanceof IfStatement || statement instanceof SimpleStatement || (statement instanceof Block && !(statement instanceof ParallelBlock))) {
            visit(statement);
        }else {
            os.printBlockStart();
            visit(statement);
            os.printBlockEnd();
            os.println();
        }
    }

    @Override
    public Void visit(KnxReadStatement stmt) {
        os.print("read ");
        writeOutDtpAndTarget(stmt);
        return null;
    }

    void writeOutDtpAndTarget(KnxStatement stmt) {
        os.print(stmt.mDatapoint.getUri());
        os.printSpace();
        os.print(stmt.target.getHref().toString());
    }

    @Override
    public Void visit(KnxScript stmt) {
        os.print("script ");
        os.printStringLiteral(stmt.mTitle);
        os.println();
        if(stmt.mDescription != null && !stmt.mDescription.isEmpty()){
            os.print("description ");
            os.printStringLiteral(stmt.mDescription);
            os.println();
        }
        os.println();
        if(stmt.mActivators.size() > 0) {
            os.print("active ");
            os.printOptionalBlock(stmt.mActivators, this);
            os.println();
            os.println();
        }

//        for(Rule r : stmt.mRules) {
//            visit(r);
//            os.println();
//        }
        visit(stmt.mRule);

        return null;
    }

    @Override
    public Void visit(KnxWriteStatement stmt) {
        os.print("write ");
        writeOutDtpAndTarget(stmt);
        os.printSpace();
        for(Literal l : stmt.literals) {
            visit(l);
            os.printSpace();
        }
        return null;
    }

    @Override
    public Void visit(Literal stmt) {
        if(stmt.val instanceof String)
            os.printStringLiteral((String)stmt.val);
        else
            os.print(stmt.val.toString());
        return null;
    }


    @Override
    public Void visit(EnvironmentFilter stmt) {
        os.print("environment ");
        os.print(stmt.mType.toString().toLowerCase());
        os.printSpace();
        os.printStringLiteral(stmt.mValue);
        return null;
    }

    @Override
    public Void visit(NotifyStatement stmt) {
        os.print("notify ");
        if(stmt.title != null){
            os.printStringLiteral(stmt.title);
            os.printSpace();
        }
        os.printStringLiteral(stmt.message);
        return null;
    }

    @Override
    public Void visit(Op stmt) {
        os.print(stmt.mSymbol);
        return null;
    }

    @Override
    public Void visit(ParallelBlock stmt) {
        os.print("parallel ");
        os.printBlock(stmt.mStatements,this);
        os.println();
        return null;
    }

    @Override
    public Void visit(RoomTrigger stmt) {
        os.print(stmt.mOn.toString().toLowerCase());
        os.printSpace();
        os.print("room ");
        os.print(stmt.mBeaconId);
        return null;
    }

    @Override
    public Void visit(Rule stmt) {
        os.print("on ");
        os.printOptionalBlock(stmt.mTriggers, this);
        os.println();
        os.print("do ");
        visit(stmt.mRunBlock);
        os.println();
        os.println();
        return null;
    }

    @Override
    public Void visit(SwitchNode el) {
        os.printBlockStart();
        for (Map.Entry<String, Statement> e : el.switchNodes.entrySet()){
            os.printStringLiteral(e.getKey());
            os.print(" ");
            Statement stmt = e.getValue();
            if(stmt instanceof SimpleStatement){
                os.print(": ");
                visit(stmt);
                os.println();
            }else{
                visit(stmt);
                os.println();
            }

        }
        os.printBlockEnd();
        os.println();
        return null;
    }

    @Override
    public Void visit(TimerangeFilter stmt) {
        os.print("time ");
        os.print(AstBuilder.dtf.print(stmt.mStartTime));
        os.printSpace();
        os.print(AstBuilder.dtf.print(stmt.mEndTime));
        return null;
    }

    @Override
    public Void visit(TimeTrigger stmt) {
        os.print("time ");
        os.print(AstBuilder.dtf.print(stmt.mTime));
        return null;
    }

    @Override
    public Void visit(WaitStatement stmt) {
        os.print("wait ");
        os.print(stmt.milliseconds);
        return null;
    }
}
