package de.hsrm.knx.knxbrowser.ui.controls;

import obix.Val;

/**
 * Created by Manuel Hermenau on 17.10.2016.
 */
public interface UnitConverter {
    public static final UnitConverter Percent = new UnitConverter() {
        @Override
        public double toObjVal(int progress) {
            return progress;
        }

        @Override
        public int toProgressVal(double objVal) {
            return (int)(objVal);
        }

        @Override
        public Val setVal(Val val, int percentage) {
            val.setInt(percentage);
            return val;
        }

        @Override
        public String unit() {
            return "%";
        }
    };
    public double toObjVal(int progress);
    public int toProgressVal(double objVal);
    public Val setVal(Val val, int progress);
    public String unit();
}
