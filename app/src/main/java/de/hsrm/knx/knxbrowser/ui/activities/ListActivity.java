package de.hsrm.knx.knxbrowser.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.db.DbHelper;
import de.hsrm.knx.knxbrowser.db.Entity;
import de.hsrm.knx.knxbrowser.ui.EntityAdapter;

import static butterknife.ButterKnife.findById;

/**
 * Created by Manuel Hermenau on 15.10.2016.
 */
public abstract class ListActivity<T extends Entity> extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    protected static final String TAG = ListActivity.class.getName();

    // ===========================================================
    // Fields
    // ===========================================================
    protected @BindView(R.id.listView ) ListView mListView;
    protected ArrayAdapter<T> mListAdapter;
    protected final ArrayList<T> mList = new ArrayList<>();
    protected final int mListLayout;

    protected DbHelper mDb;
    // ===========================================================
    // Constructors
    // ===========================================================
    protected ListActivity(int listLayout){
        mListLayout = listLayout;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDb = DbHelper.getInstance(this);
        initUI();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.listView) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_context_script, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.menu_script_delete:
                final T listItem = mListAdapter.getItem(info.position);
                onListItemDelete(listItem);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    protected abstract void onListItemDelete(T listItem);

    // ===========================================================
    // Methods
    // ===========================================================
    private void initUI() {
        //Apply Layout
        setContentView(R.layout.activity_manage_scripts);
        ButterKnife.bind(this);

        registerForContextMenu(mListView);

        //Create Toolbar
        //Toolbar toolbar = findById(this, R.id.toolbar);
        //setSupportActionBar(toolbar);

        mListAdapter = createListAdapter(mList);
        mListView.setAdapter(mListAdapter);

    }

    protected ArrayAdapter<T> createListAdapter(List<T> list){
        return new EntityAdapter<T>(this, mListLayout){
            @Override
            protected void bindView(View view, T item) {
                ListActivity.this.bindListItem(view, item);
            }
        };
    }

    protected abstract void bindListItem(View view, T item);

    @OnItemClick(R.id.listView)
    public void onItemClicked(AdapterView<?> parent, View view, int position, long id) {
        T clicked = mListAdapter.getItem(position);
        if(clicked != null) {
            Log.d(TAG, clicked.toString() + " clicked");
            onItemClicked(clicked);
        }
    }

    protected abstract void onItemClicked(T item);


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
