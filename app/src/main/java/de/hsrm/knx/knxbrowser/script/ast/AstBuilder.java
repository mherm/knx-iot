package de.hsrm.knx.knxbrowser.script.ast;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import de.hsrm.knx.knxbrowser.model.Datapoint;
import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.script.grammar.KnxScriptBaseVisitor;
import de.hsrm.knx.knxbrowser.script.grammar.KnxScriptParser;
import de.hsrm.knx.knxbrowser.script.ast.RoomTrigger.EnterRoomTrigger;
import de.hsrm.knx.knxbrowser.script.ast.RoomTrigger.LeaveRoomTrigger;
import obix.Obj;
import obix.Uri;


/**
 * Created by Manuel Hermenau on 07.10.2016.
 */
public class AstBuilder extends KnxScriptBaseVisitor<ScriptElement> {
    static final DateTimeFormatter dtf = DateTimeFormat.forPattern("HH:mm");

    @Override public ScriptElement visitStart(KnxScriptParser.StartContext ctx) {
        KnxScript script = new KnxScript();
        final KnxScriptParser.TitleContext title = ctx.header().title();
        script.mTitle = title.STRING().getText();

        final KnxScriptParser.DescriptionContext description = ctx.header().description();
        if (description != null)
            script.mDescription = description.STRING().getText();
        final KnxScriptParser.ActivationContext activation = ctx.activation();
        if (activation != null){
            for (KnxScriptParser.FilterContext filterCtx : activation.active_block().filter()) {
                final ActivationFilter activationFilter = (ActivationFilter) visit(filterCtx);
                if (activationFilter != null)
                    script.mActivators.add(activationFilter);
            }
        }
//        for(KnxScriptParser.RuleContext ruleCtx : ctx.rule()) {
//            script.mRules.add((Rule) visit(ruleCtx));
//        }
       KnxScriptParser.RuleContext ruleCtx = ctx.rule();
       script.mRule = (Rule) visit(ruleCtx);
       return script;
    }


    protected ScriptElement aggregateResult(ScriptElement aggregate, ScriptElement nextResult) {
        return nextResult == null? aggregate : nextResult;
    }

    @Override
    public ScriptElement visitCurrent_room_stmt(KnxScriptParser.Current_room_stmtContext ctx) {
        return super.visitCurrent_room_stmt(ctx);
    }

    @Override public ScriptElement visitEnvironment_filter(KnxScriptParser.Environment_filterContext ctx) {
        EnvironmentFilter.Type type = EnvironmentFilter.Type.lookup(ctx.ENV_ID_TYPE().getText());
        EnvironmentFilter environmentFilter = new EnvironmentFilter(type, ctx.STRING().getText());
        return environmentFilter;
    }


    @Override public ScriptElement visitTimerange_filter(KnxScriptParser.Timerange_filterContext ctx) {
        final DateTime startTime = dtf.parseDateTime(ctx.TIME(0).getText());
        final DateTime endTime = dtf.parseDateTime(ctx.TIME(1).getText());
        TimerangeFilter timerangeFilter = new TimerangeFilter(startTime, endTime);
        return timerangeFilter;
    }

    @Override public Rule visitRule(KnxScriptParser.RuleContext ctx) {
        Rule rule = new Rule();
        final List<KnxScriptParser.TriggerContext> triggerList = ctx.trigger_block().trigger();
        for(KnxScriptParser.TriggerContext triggerCtx : triggerList){
            Trigger trigger = (Trigger) visit(triggerCtx);
            if(trigger != null)
                rule.mTriggers.add(trigger);
        }

        rule.mRunBlock = wrapBlock((Statement) visit(ctx.statement_block()));
        return rule;
    }

    private Block wrapBlock(Statement element) {
        if(!(element instanceof Block)){
            Block block = new Block();
            block.mStatements.add(element);
            return block;
        }
        return (Block)element;
    }

    @Override public RoomTrigger visitEnter_room_trigger(KnxScriptParser.Enter_room_triggerContext ctx) {
        final BeaconId beaconId = BeaconId.parse(ctx.room_id().getText());
        RoomTrigger trigger = new EnterRoomTrigger(beaconId);
        return trigger;
    }
    @Override public RoomTrigger visitLeave_room_trigger(KnxScriptParser.Leave_room_triggerContext ctx) {
        final BeaconId beaconId = BeaconId.parse(ctx.room_id().getText());
        RoomTrigger trigger = new LeaveRoomTrigger(beaconId);
        return trigger;
    }
    @Override public TimeTrigger visitAlert_trigger(KnxScriptParser.Alert_triggerContext ctx) {
        TimeTrigger trigger = new TimeTrigger();
        trigger.setTime(dtf.parseDateTime(ctx.TIME().getText()));
        return trigger;
    }
    @Override public Statement visitStatement_block(KnxScriptParser.Statement_blockContext ctx) {
        final List<KnxScriptParser.StmtContext> stmts = ctx.stmt();
        if(stmts.size() == 1)
            return (Statement)visit(stmts.get(0));
        Block block = new Block();
        for(KnxScriptParser.StmtContext stmtCtx : stmts){
            block.mStatements.add((Statement) visit(stmtCtx));
        }
        return block;
    }
    @Override public NotifyStatement visitNotify_stmt(KnxScriptParser.Notify_stmtContext ctx) {
        NotifyStatement stmt = new NotifyStatement();
        final List<TerminalNode> params = ctx.STRING();
        if(params.size() == 1){
            stmt.message = ctx.STRING().get(0).getText();
        }else{
            stmt.title = ctx.STRING().get(0).getText();
            stmt.message = ctx.STRING().get(1).getText();
        }
        return stmt;
    }
    @Override public HttpStatement visitHttp_stmt(KnxScriptParser.Http_stmtContext ctx) {
        HttpStatement stmt = new HttpStatement();
        stmt.mMethod = HttpStatement.Method.lookup(ctx.method().getText());
        stmt.mUrl = ctx.URL().getText();
        final KnxScriptParser.Http_contentContext http_contentContext = ctx.http_content();
        if(http_contentContext != null)
            stmt.mContent = http_contentContext.getText();
        final KnxScriptParser.Http_headerContext http_headers = ctx.http_header();
        if(http_headers != null)
            stmt.mHeaders = http_headers.getText();
        return stmt;
    }
    @Override public Literal visitLiteral(KnxScriptParser.LiteralContext ctx) {
        if(ctx.BOOL() != null)
            return new Literal(Boolean.parseBoolean(ctx.BOOL().getText()));
        if(ctx.INTEGER() != null)
            return new Literal(Integer.parseInt(ctx.INTEGER().getText()));
        if(ctx.REAL() != null)
            return new Literal(Double.parseDouble(ctx.REAL().getText()));
        if(ctx.STRING() != null)
            return new Literal(ctx.STRING().getText());
        throw new RuntimeException("Unknown Literal: "+ctx);
    }
    @Override public ParallelBlock visitParallel_block(KnxScriptParser.Parallel_blockContext ctx) {
        ParallelBlock block = new ParallelBlock();
        for(KnxScriptParser.Simple_stmtContext stmtCtx : ctx.simple_stmt()){
            block.mStatements.add((Statement) visit(stmtCtx));
        }
        return block;
    }
    @Override public IfStatement visitIf_stmt(KnxScriptParser.If_stmtContext ctx) {
        IfStatement stmt = new IfStatement();
        stmt.mExpression = (Expression)visit(ctx.expression());
        final List<KnxScriptParser.Statement_blockContext> stmt_blocks = ctx.statement_block();
        stmt.mIfBlock = (Statement) visit(stmt_blocks.get(0));
        if(stmt_blocks.size() > 1)
            stmt.mElseBlock = (Statement) visit(ctx.statement_block(1));
        return stmt;
    }
    @Override public Expression visitExpression(KnxScriptParser.ExpressionContext ctx) {
        Expression exp = new Expression();
        exp.left = (SimpleStatement)visit(ctx.simple_stmt(0));
        exp.right = (SimpleStatement)visit(ctx.simple_stmt(1));
        exp.op = (Op) visit(ctx.op());
        return exp;
    }
    @Override public AskStatement visitAsk_stmt(KnxScriptParser.Ask_stmtContext ctx) {
        AskStatement stmt = new AskStatement();
        stmt.message = ctx.STRING().getText();
        stmt.switchNode = (SwitchNode)visit(ctx.switch_block());
        return stmt;
    }
    @Override public SwitchNode visitSwitch_block(KnxScriptParser.Switch_blockContext ctx) {
        SwitchNode node = new SwitchNode();
        for(KnxScriptParser.Switch_optionContext o: ctx.switch_option()){
            node.switchNodes.put(o.STRING().getText(), (Statement)visit(o));
        }
        return node;
    }
    @Override public Statement visitSwitch_option(KnxScriptParser.Switch_optionContext ctx) {
        if(ctx.simple_stmt() != null)
            return (Statement)visit(ctx.simple_stmt());

        Block block = new Block();
        for(KnxScriptParser.StmtContext stmtCtx : ctx.stmt()){
            block.mStatements.add((Statement) visit(stmtCtx));
        }
        return block;
    }
    @Override public Op visitOp(KnxScriptParser.OpContext ctx) {
        if(ctx.EQ() != null)
            return Op.EQ;
        if(ctx.GE() != null)
            return Op.GE;
        if(ctx.LE() != null)
            return Op.LE;
        if(ctx.GT() != null)
            return Op.GT;
        if(ctx.LT() != null)
            return Op.LT;
        if(ctx.NEQ() != null)
            return Op.NEQ;
        throw new RuntimeException("Unknown Op: "+ctx.getText());
    }
    @Override public KnxReadStatement visitKnx_read_stmt(KnxScriptParser.Knx_read_stmtContext ctx) {
        KnxReadStatement statement = new KnxReadStatement();
        statement.mDatapoint = Datapoint.parse(ctx.DTP().getText());
        Obj target = new Obj();
        Uri href = new Uri(ctx.URL().getText());
        target.setHref(href);
        target.setIs(statement.mDatapoint.toContract());
        statement.target = target;
        return statement;
    }
    @Override public KnxWriteStatement visitKmx_write_stmt(KnxScriptParser.Kmx_write_stmtContext ctx) {
        KnxWriteStatement statement = new KnxWriteStatement();
        statement.mDatapoint = Datapoint.parse(ctx.DTP().getText());
        Obj target = new Obj();
        Uri href = new Uri(ctx.URL().getText());
        target.setHref(href);
        target.setIs(statement.mDatapoint.toContract());
        statement.target = target;
        for(KnxScriptParser.LiteralContext literalCtx : ctx.literal()){
            statement.literals.add((Literal)visit(literalCtx));
        }
        return statement;
    }

    @Override public WaitStatement visitWait_stmt(KnxScriptParser.Wait_stmtContext ctx) {
        return new WaitStatement(Integer.parseInt(ctx.INTEGER().getText()));
    }

}
