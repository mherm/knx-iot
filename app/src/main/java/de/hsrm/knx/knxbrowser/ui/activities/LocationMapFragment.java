package de.hsrm.knx.knxbrowser.ui.activities;

import android.graphics.Color;
import android.graphics.Path;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.maps.MapBasedOutlineProvider;
import de.hsrm.knx.knxbrowser.maps.OutlineProvider;

public class LocationMapFragment extends Fragment implements RoomChangedListener{

    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = LocationMapFragment.class.getName();


    // ===========================================================
    // Fields
    // ===========================================================
    protected LocationServiceConnector mLocationService;

    //UI Elements
    protected ImageView mMapImageView;
    protected LayerDrawable mMapLayerDrawable;

    //Map
    protected OutlineProvider mRoomOutlineProvider;


    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mLocationService.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.content_location_map,  container);
        mMapImageView = (ImageView) v.findViewById(R.id.map_image_view);
        mMapLayerDrawable = (LayerDrawable) mMapImageView.getDrawable();
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initProviders();
        mLocationService = LocationServiceConnector.getInstance(getContext().getApplicationContext(), savedInstanceState);
        drawCurrentRoom();
    }

    @Override
    public void onPause() {
        super.onPause();
        mLocationService.removeRoomChangedListener(this);
        mLocationService.stopListening();
    }

    @Override
    public void onResume() {
        super.onResume();
        mLocationService.addRoomChangedListener(this);
        mLocationService.startListening ();
        drawCurrentRoom();
    }

    @Override
    public void onRoomChanged(BeaconId newRoom, BeaconId oldRoom) {
        Log.d(TAG, "onRoomChanged("+newRoom+","+oldRoom+")");
        getActivity().runOnUiThread(()->drawRoom(newRoom));
    }

    private void drawRoom(BeaconId beaconId) {
        final Path outline = mRoomOutlineProvider.getOutline(beaconId);
        drawRoom(outline);
    }


    // ===========================================================
    // Methods
    // ===========================================================

    private void drawCurrentRoom(){
        mLocationService.requestCurrentRoom(room ->
                getActivity().runOnUiThread(()->drawRoom(room)));
    }

    private void drawRoom(Path outline) {
        PathShape ps = new PathShape(outline, 100, 100);
        ShapeDrawable clip = new ShapeDrawable(ps);
        clip.getPaint().setColor(Color.argb(140, 255,0,0));
        clip.setBounds(0,0,mMapLayerDrawable.getIntrinsicWidth(), mMapLayerDrawable.getIntrinsicHeight());
        mMapLayerDrawable.setDrawableByLayerId(R.id.current_room_layer, clip);
        mMapLayerDrawable.invalidateSelf();
    }


    //JUST FOR DEMO

    private void initProviders() {

        mRoomOutlineProvider = new DebugOutlineProvider();;
    }

    public static class DebugOutlineProvider extends MapBasedOutlineProvider{
        public DebugOutlineProvider() {
            super(createMap());
        }

        private static Map<BeaconId, Path> createMap() {
            //Für Demo in Office
            Map<BeaconId, Path> officeRooms = new HashMap<>(3);
            officeRooms.put(new BeaconId(null,1,1), createOfficeOffice());
            officeRooms.put(new BeaconId(null,2,2), createOfficeHallway());
            officeRooms.put(new BeaconId(null,3,3), createOfficeKitchen());
            return officeRooms;
        }

        private static Path createOfficeKitchen() {
        Path p = new Path();
        p.moveTo(73f,23.53f);
        p.rLineTo(24.75f,0);
        p.rLineTo(0,16.51f);
        p.rLineTo(-24.75f,0);
        p.close();
        return p;
    }

    private static Path createOfficeHallway() {
        Path p = new Path();
        p.moveTo(61f,23.72f);
        p.rLineTo(12.25f,0);
        p.rLineTo(0,37f);
        p.rLineTo(-12.25f,0);
        p.close();
        return p;
    }

    private static Path createOfficeOffice() {
        Path p = new Path();
        p.moveTo(41f,60.91f);
        p.rLineTo(58.75f,0);
        p.rLineTo(0,39.09f);
        p.rLineTo(-58.75f,0);
        p.close();
        return p;
    }

    }

}
