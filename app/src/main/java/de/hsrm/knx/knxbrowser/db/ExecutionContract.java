package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 04.08.2016.
 */
import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.Utils;
import de.hsrm.knx.knxbrowser.script.ExecutionResult;

public class ExecutionContract extends TableContract<ExecutionResult>{
    public static final String TABLE_NAME = "executions";

    private static final ExecutionContract instance = new ExecutionContract();

    private ExecutionContract(){};

    /*Below is a subclass that defines the column names*/
    public static abstract class ExecutionEntry implements BaseColumns {
        public static final Map<String, ColumnContract> COLUMNS = new HashMap<>();

        //Column Name Constants
        public static final String SCRIPT      = "script";
        public static final String START_TIME  = "start_time";
        public static final String END_TIME    = "end_time";
        public static final String RESULT      = "result";
        public static final String LOG         = "log";
        public static final String TRIGGER     = "tigger";

        //Map creation
        static {
            contract(COLUMNS, SCRIPT,   "INT",       "DEFAULT ''", foreignKey(SCRIPT, ScriptContract.TABLE_NAME));
            contract(COLUMNS, START_TIME, "DATETIME",  "NOT NULL");
            contract(COLUMNS, END_TIME,  "DATETIME",   "DEFAULT -1");
            contract(COLUMNS, RESULT,   "INT",       "DEFAULT 0");
            contract(COLUMNS, TRIGGER,  "TEXT",      "DEFAULT 'Manually'");
            contract(COLUMNS, LOG,      "TEXT",      "DEFAULT ''");
        }

        private static String foreignKey(String field, String table) {
            return "FOREIGN KEY ("+field+") REFERENCES "+table+"("+_ID+")";
        }
    }

    public static abstract class ExecutionOperations extends Operations {
        public static final String CREATE_TABLE =
                createTable(TABLE_NAME, ExecutionEntry.COLUMNS);
        public static final String DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public ExecutionResult deserialize(Cursor c){
        int id              = c.getInt(c.getColumnIndexOrThrow(ExecutionEntry._ID));
        int scriptId         = c.getInt(c.getColumnIndexOrThrow(ExecutionEntry.SCRIPT));
        String trigger      = c.getString(c.getColumnIndexOrThrow(ExecutionEntry.TRIGGER));
        boolean success     = c.getInt(c.getColumnIndexOrThrow(ExecutionEntry.RESULT))!= 0;
        long startTs        = c.getLong(c.getColumnIndexOrThrow(ExecutionEntry.START_TIME));
        long endTs          = c.getLong(c.getColumnIndexOrThrow(ExecutionEntry.END_TIME));

        final DateTime start = startTs <0 ? null: Utils.fromUnixTs(startTs);
        final DateTime end = endTs <0 ? null:Utils.fromUnixTs(endTs);

        ExecutionResult result = new ExecutionResult();
        result.setId(id);
        result.setScriptId(scriptId);
        result.setSuccess(success);
        result.setStartTime(start);
        result.setEndTime(end);
        result.setTrigger(trigger);
        return result;
    }

    @Override
    public String tableName() {
        return TABLE_NAME;
    }

    public static TableContract<ExecutionResult> getInstance() {
        return instance;
    }

    public ContentValues serialize(ExecutionResult r){
        ContentValues cv = new ContentValues();
        if(r.getId() >= 0)
            cv.put(BaseColumns._ID,r.getId());
        cv.put(ExecutionEntry.SCRIPT,    r.getScriptId());
        cv.put(ExecutionEntry.TRIGGER,   r.getTrigger());
        cv.put(ExecutionEntry.RESULT,   r.isSuccess());
        cv.put(ExecutionEntry.START_TIME,Utils.toUnixTs(r.getStartTime()));
        if(r.getEndTime() != null)
            cv.put(ExecutionEntry.END_TIME,  Utils.toUnixTs(r.getEndTime()));
        if(r.getLog() != null)
            cv.put(ExecutionEntry.LOG,       r.getLog().toString());
        return cv;
    }

}
