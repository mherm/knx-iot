package de.hsrm.knx.knxbrowser;

/**
 * Created by Manuel Hermenau on 22.08.2016.
 */
public interface RoomIdTranslator {

    public String getDomainId(BeaconId beaconId);
//    public String getDomainName(int beaconMajor);
    public BeaconId getBeaconId(String domainId);

}
