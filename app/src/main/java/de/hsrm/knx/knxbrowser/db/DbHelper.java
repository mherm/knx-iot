package de.hsrm.knx.knxbrowser.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Looper;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import org.joda.time.DateTime;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hsrm.knx.knxbrowser.Environment;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.Utils;
import de.hsrm.knx.knxbrowser.script.ExecutionResult;
import de.hsrm.knx.knxbrowser.script.ast.KnxScript;
import de.hsrm.knx.knxbrowser.util.URIUtil;

import static de.hsrm.knx.knxbrowser.db.EnvironmentContract.EnvOperations;
import static de.hsrm.knx.knxbrowser.db.ScriptContract.ScriptOperations;
import static de.hsrm.knx.knxbrowser.db.ScriptContract.ScriptEntry;
import static de.hsrm.knx.knxbrowser.db.ExecutionContract.ExecutionOperations;
import static de.hsrm.knx.knxbrowser.db.ExecutionContract.ExecutionEntry;
import static de.hsrm.knx.knxbrowser.db.TableContract.contract;

public class DbHelper extends SQLiteOpenHelper {

    //Constants
    public static final String INTENT_ENTITY_CHANGED = "de.hsrm.knx.knxbrowser.db.action.ENTITY_CHANGED";
    public static final String INTENT_ENTITY_CREATED = "de.hsrm.knx.knxbrowser.db.action.ENTITY_CREATED";
    public static final String INTENT_ENTITY_DELETED = "de.hsrm.knx.knxbrowser.db.action.ENTITY_DELETED";

    public static final String INTENT_SCRIPT_ENABLED = "de.hsrm.knx.knxbrowser.db.action.SCRIPT_ENABLED";
    public static final String INTENT_SCRIPT_DISABLED = "de.hsrm.knx.knxbrowser.db.action.SCRIPT_DISABLED";

    public static final String EXTRA_ENTITY_ID = "de.hsrm.knx.knxbrowser.db.extra.ENTITY_ID";
    public static final String EXTRA_ENTITY_CLASS = "de.hsrm.knx.knxbrowser.db.extra.ENTITY_CLASS";

    private static final String TAG = DbHelper.class.getName();

    // Database Info
    private static final String DATABASE_NAME = "knx.db";
    private static final int DATABASE_VERSION =2;

    private static DbHelper sInstance = null;

    private final Context mContext;

    private DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    public static synchronized DbHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            Log.d(TAG, "Creating DbHelper instance...");
            sInstance = new DbHelper(context.getApplicationContext());

        }
        return sInstance;
    }

    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "DbHelper.onCreate()");
        db.execSQL(EnvOperations.CREATE_TABLE);
        db.execSQL(ScriptOperations.CREATE_TABLE);
        db.execSQL(ExecutionOperations.CREATE_TABLE);
        createDefaultEnvironment(db);
        loadDemoScripts(db);
    }

    private void clearDatabase(SQLiteDatabase db){
        db.execSQL(EnvOperations.DROP_TABLE);
        db.execSQL(ScriptOperations.DROP_TABLE);
        db.execSQL(ExecutionOperations.DROP_TABLE);
        onCreate(db);
    }

    private void loadDemoScripts(SQLiteDatabase db) {
        loadScriptFromResources(db, R.raw.hello_world);
        loadScriptFromResources(db, R.raw.hello_knx_world);
        loadScriptFromResources(db, R.raw.bye_ude_world);
        loadScriptFromResources(db, R.raw.hello_ude_world);
        loadScriptFromResources(db, R.raw.hello_notification_world, true);
    }


    public ScriptSaveResult loadScriptFromUri(Uri uri) {
        Log.i(TAG, "Loading script from URI "+uri);
        assertIsNotMainThread();
        try {
            InputStream inputStream = mContext.getContentResolver().openInputStream(uri);
            final KnxScript script = KnxScript.read(inputStream);
            SQLiteDatabase db = getWritableDatabase();
            int id = createNewScript(db, false, script);
            return new ScriptSaveResult(true, id);
        }catch (Exception e){
            Log.e(TAG, "Could not load script from "+uri);
        }
        return ScriptSaveResult.ERROR;
    }


    private void loadScriptFromResources(SQLiteDatabase db, int id) {
        loadScriptFromResources(db, id, false);
    }
    private void loadScriptFromResources(SQLiteDatabase db, int id, boolean enabled) {
        KnxScript script = KnxScript.read(mContext.getResources().openRawResource(id));
        createNewScript(db, enabled, script);
    }

    private int createNewScript(SQLiteDatabase db, boolean enabled, KnxScript script) {
        final KnxScriptConfiguration conf = new KnxScriptConfiguration();
        conf.setKnxScript(script);
        conf.setEnabled(enabled);
        saveEntity(db, conf, ScriptContract.getInstance());
        return conf.getId();
    }

    private void createDefaultEnvironment(SQLiteDatabase db) {
        Environment e = new Environment();
        e.setId(0);
        e.setName("Default");
        e.setHost(URIUtil.makeURL("http://10.18.59.101:8080"));
        saveEntity(db, e, EnvironmentContract.getInstance());
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        clearDatabase(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean deleteEnvironment(Environment env) {
        assertIsNotMainThread();
        final boolean result = deleteEntity(env, EnvironmentContract.getInstance());
        return result;
    }

    public boolean deleteScript(KnxScriptConfiguration script) {
        assertIsNotMainThread();
        final boolean result = deleteEntity(script, ScriptContract.getInstance());
        return result;
    }

    private <T extends Entity> boolean deleteEntity(Entity e, TableContract<T> contract) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedRows = db.delete(contract.tableName(), BaseColumns._ID+"=?",new String[]{String.valueOf(e.getId())});
        boolean deleted = deletedRows == 1;
        if(deleted) {
            broadcastMessage(INTENT_ENTITY_DELETED, e);
        }
        return deleted;
    }

    public List<Environment> getEnvironmentProfiles() {
        Log.d(TAG, "Reading saved Environment profiles");
        assertIsNotMainThread();
        return getEntities(EnvironmentContract.getInstance());
    }

    public Environment getEnvironmentProfile(int id) {
        Log.d(TAG, "Reading saved Environment profile "+id);
        assertIsNotMainThread();
        return getEntity(EnvironmentContract.getInstance(), id);
    }

    public List<KnxScriptConfiguration> getScripts() {
        Log.d(TAG, "Reading saved scripts");
        assertIsNotMainThread();
        final List<KnxScriptConfiguration> scripts = getEntities(ScriptContract.getInstance());
        attachLatestExecutions(scripts);
        return scripts;
    }

    private void attachLatestExecutions(List<KnxScriptConfiguration> scripts) {
        String[] ids = new String[scripts.size()];
        for(int i=0; i<scripts.size();i++){
            ids[i] = String.valueOf(scripts.get(i).getId());
        }
        TableContract<ExecutionResult> contract = ExecutionContract.getInstance();
        SQLiteDatabase db = getReadableDatabase();
        try (Cursor c = db.query(
                contract.tableName(),
                null,
                ExecutionEntry.SCRIPT+" IN ("+makePlaceholders(ids.length)+")",
                ids,
                null,
                null,
                null
        )) {
            if (c.moveToFirst()) {
                do {
                    final ExecutionResult exRes = contract.deserialize(c);
                    for(KnxScriptConfiguration sc : scripts){
                        if(sc.getId() == exRes.getScriptId()){
                            sc.setLatestExecution(exRes);
                        }
                    }
                }while (c.moveToNext());
            }
        }
    }

    public KnxScriptConfiguration getScript(int id) {
        Log.d(TAG, "Reading saved script "+id);
        assertIsNotMainThread();
        final KnxScriptConfiguration script = getEntity(ScriptContract.getInstance(), id);
        ExecutionResult latestEx = getLatestExecutionResultForScript(id);
        script.setLatestExecution(latestEx);
        return script;
    }

    public String getScriptText(int id) {
        Log.d(TAG, "Reading script content of saved script "+id);
        assertIsNotMainThread();
        SQLiteDatabase db = getReadableDatabase();
        final TableContract<KnxScriptConfiguration> contract = ScriptContract.getInstance();
        try (Cursor c = db.query(
                true,
                contract.tableName(),
                Utils.arr(ScriptEntry.SCRIPT),
                BaseColumns._ID+"=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null,
                "1"
        )) {
            if (c.moveToFirst()) {
                return c.getString(c.getColumnIndexOrThrow(ScriptEntry.SCRIPT));
            }
        }catch(Exception e){
            Log.e(TAG, "Error while loading entity from database", e);
            throw new RuntimeException(e);
        }
        return null;
    }

    public ScriptSaveResult saveScriptText(int id, String text) {
        Log.d(TAG, "Reading script content of saved script "+id);
        assertIsNotMainThread();

        try{
            final KnxScript script = KnxScript.parse(text);
            //If parsing went okay, we don't need to stringify it again
            SQLiteDatabase db = getReadableDatabase();
            if(id == -1){
                int newId=createNewScript(db, false, script);
                return new ScriptSaveResult(true,newId);
            }
            ContentValues cv = new ContentValues();
            final TableContract<KnxScriptConfiguration> contract = ScriptContract.getInstance();
            cv.put(ScriptEntry.SCRIPT, text);
            boolean updated = db.update(contract.tableName(), cv, ScriptEntry._ID+"=?",Utils.strArr(id)) == 1;

            if(updated){
                broadcastMessage(INTENT_ENTITY_CHANGED, id, KnxScriptConfiguration.class);
                return ScriptSaveResult.SUCCESS;
            }
            return ScriptSaveResult.NO_SUCCESS;
        }catch (Exception e){
            Log.e(TAG, "Could not parse script",e);
        }

        return ScriptSaveResult.ERROR;
    }

    private <T> T getEntity(TableContract<T> contract, int id) {
        SQLiteDatabase db = getReadableDatabase();
            try (Cursor c = db.query(
                    contract.tableName(),
                    null,
                    BaseColumns._ID+"=?",
                    new String[]{String.valueOf(id)},
                    null,
                    null,
                    null
            )) {
                if (c.moveToFirst()) {
                    return contract.deserialize(c);
                }
        }catch(Exception e){
                Log.e(TAG, "Error while loading entity from database", e);
                throw new RuntimeException(e);
        }
        return null;
    }

    @NonNull
    private <T> List<T> getEntities(TableContract<T> contract) {
        SQLiteDatabase db = getReadableDatabase();
        try (Cursor c = db.query(
                contract.tableName(),
                null,
                null,
                null,
                null,
                null,
                null
        )) {
            List<T> result = new ArrayList<>(c.getColumnCount());
            if (c.moveToFirst()) {
                do {
                    result.add(contract.deserialize(c));
                } while (c.moveToNext());
            }
            return result;
        }catch(Exception e){
            Log.e(TAG, "Error while loading entity from database", e);
            throw new RuntimeException(e);
        }
    }

    public Environment saveEnvironmentProfile(Environment env) {
        Log.d(TAG, "Saving Environment profile "+env.getName()+ " ("+env.getId()+")");
        assertIsNotMainThread();
        saveEntity(env, EnvironmentContract.getInstance());
        return env;
    }

    /**
     * Serializes the whole script. Can be expensive.
     * Use {@link #setScriptEnabled(int, boolean)} if you only want to change the enabled value
     * @param script
     * @return
     */
    public KnxScriptConfiguration saveScript(KnxScriptConfiguration script) {
        Log.d(TAG, "Saving script "+script.getTitle()+ " ("+script.getId()+")");
        assertIsNotMainThread();
        saveEntity(script, ScriptContract.getInstance());
        return script;
    }

    /**
     *
     * @param id
     * @param enabled
     * @return the value if it was written
     */
    public boolean setScriptEnabled(int id, boolean enabled) {
        Log.d(TAG, "Enabling script "+id);
        assertIsNotMainThread();
        SQLiteDatabase db = getWritableDatabase();
        String where = BaseColumns._ID+"=?";
        String[] ids = new String[]{String.valueOf(id)};
        ContentValues cv = new ContentValues();
        cv.put(ScriptEntry.ENABLED, enabled);

        boolean updated = db.update(
                ScriptContract.TABLE_NAME,
                cv,
                where,
                ids
        ) > 0;

        if(!updated)
            throw new RuntimeException("No script with the id "+id);
        if(enabled)
            broadcastMessage(INTENT_SCRIPT_ENABLED, id, KnxScriptConfiguration.class);
        else
            broadcastMessage(INTENT_SCRIPT_DISABLED, id, KnxScriptConfiguration.class);
        return enabled;
    }

    private <T extends Entity> void saveEntity(SQLiteDatabase db, T t, TableContract<T> contract) {
        boolean isChange = t.getId() >= 0;
        db.beginTransaction();
        ContentValues dbValues = contract.serialize(t);
        long result = db.insertWithOnConflict(contract.tableName(), null, dbValues, SQLiteDatabase.CONFLICT_REPLACE);
        db.setTransactionSuccessful();
        db.endTransaction();
        t.setId((int)result);
        if(isChange)
            broadcastMessage(INTENT_ENTITY_CHANGED, t);
        else
            broadcastMessage(INTENT_ENTITY_CREATED, t);
    }

    private <T extends Entity> void saveEntity(T t, TableContract<T> contract) {
        SQLiteDatabase db = getWritableDatabase();
        saveEntity(db, t, contract);
    }

    private void broadcastMessage(String action, Entity e){
        broadcastMessage(action, e.getId(), e.getClass());
    }
    private void broadcastMessage(String action, int id, Class<? extends Entity> type){
        Intent intent = new Intent(action);
        intent.putExtra(EXTRA_ENTITY_ID, id);
        intent.putExtra(EXTRA_ENTITY_CLASS, type.getName());
        Log.d(TAG, "Broadcasting: "+intent.getAction() + ": "+ id);
        mContext.sendBroadcast(intent);
    }


    private static final String makePlaceholders(int len) {
        return TextUtils.join(",", Collections.nCopies(len, "?"));
    }

    private void assertIsNotMainThread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new RuntimeException("Do not do database operations on the Main thread! Use an AsyncTask instead!");
        }
    }

    public ExecutionResult saveExecutionResult(ExecutionResult exRes) {
        Log.d(TAG, "Saving ExecutionResult for "+exRes.getScriptId());
        assertIsNotMainThread();
        saveEntity(exRes, ExecutionContract.getInstance());
        return exRes;
    }

    public ExecutionResult getExecutionResult(int id) {
        Log.d(TAG, "Reading ExecutionResult "+id);
        assertIsNotMainThread();
        return getEntity(ExecutionContract.getInstance(), id);
    }

    public ExecutionResult getLatestExecutionResultForScript(int id) {
        Log.d(TAG, "Reading ExecutionResult for script "+id);
        assertIsNotMainThread();
        TableContract<ExecutionResult> contract = ExecutionContract.getInstance();
        SQLiteDatabase db = getReadableDatabase();
        try (Cursor c = db.query(
                contract.tableName(),
                null,
                ExecutionEntry.SCRIPT+"=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                ExecutionEntry.START_TIME+" DESC",
                "1"
        )) {
            if (c.moveToFirst()) {
                return contract.deserialize(c);
            }
        }
        return null;
    }

    public List<ExecutionResult> getExecutionResultsForScript(int id) {
        Log.d(TAG, "Reading ExecutionResult for script "+id);
        assertIsNotMainThread();
        TableContract<ExecutionResult> contract = ExecutionContract.getInstance();
        SQLiteDatabase db = getReadableDatabase();
        try (Cursor c = db.query(
                contract.tableName(),
                null,
                ExecutionEntry.SCRIPT+"=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                ExecutionEntry.START_TIME+" DESC",
                "1"
        )) {
            List<ExecutionResult> result = new ArrayList<>(c.getColumnCount());
            if (c.moveToFirst()) {
                do {
                    result.add(contract.deserialize(c));
                } while (c.moveToNext());
            }
            return result;
        }
    }

    /**
     * HELPER FUNCTION FOR
     * @param Query
     * @return
     */
    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }


    }

    public void closeStaleExecutions() {
        Log.d(TAG, "Fixing stale database entries");
        assertIsNotMainThread();
        SQLiteDatabase db = getReadableDatabase();
        final TableContract<ExecutionResult> contract = ExecutionContract.getInstance();
        try (Cursor c = db.query(
                contract.tableName(),
                null,
                ExecutionEntry.END_TIME+" = ?",
                new String[]{"-1"},
                null,
                null,
                null
        )) {
            String[] ids = new String[c.getColumnCount()];
            int i=0;
            if (c.moveToFirst()) {
                do {
                    ids[i++] = c.getString(c.getColumnIndexOrThrow(ExecutionEntry._ID));
                } while (c.moveToNext());
            }
            db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(ExecutionEntry.END_TIME, Utils.toUnixTs(new DateTime()));
            db.update(contract.tableName(), cv, ExecutionEntry._ID+" IN ("+makePlaceholders(ids.length)+")", ids);
        }

    }

    public static class ScriptSaveResult {
        public static final ScriptSaveResult ERROR = new ScriptSaveResult(false, null);
        public static final ScriptSaveResult SUCCESS = new ScriptSaveResult(true, null);
        public static final ScriptSaveResult NO_SUCCESS = new ScriptSaveResult(false, null);
        private final boolean success;
        private final Integer newId;

        public ScriptSaveResult(boolean success, Integer newId) {
            this.newId = newId;
            this.success = success;
        }

        public Integer getNewId() {
            return newId;
        }

        public boolean isSuccess() {
            return success;
        }

    }
}