package de.hsrm.knx.knxbrowser;

import android.os.Parcel;
import android.os.Parcelable;

import com.estimote.sdk.Beacon;

import java.util.UUID;

/**
 * Created by Manuel Hermenau on 31.08.2016.
 */
public class BeaconId implements Parcelable {
    // ===========================================================
    // Constants
    // ===========================================================

    public static final BeaconId NONE = new BeaconId(null, -1, -1){
        @Override
        public String represent() {
            return "None";
        }
    };


    //For Parcelable
    public static final Creator<BeaconId> CREATOR = new Creator<BeaconId>() {
        @Override
        public BeaconId createFromParcel(Parcel in) {
            if (in == null)
                return NONE;
            return new BeaconId(in);
        }

        @Override
        public BeaconId[] newArray(int size) {
            return new BeaconId[size];
        }
    };

    // ===========================================================
    // Fields
    // ===========================================================
    private final UUID mProximityUuid;
    private final int mMajor;
    private final int mMinor;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BeaconId(UUID proximity_uuid, int major, int minor) {
        this.mMajor = major;
        this.mProximityUuid = proximity_uuid;
        this.mMinor = minor;
    }

    public BeaconId(Beacon beacon) {
        this.mMajor = beacon.getMajor();
        this.mProximityUuid = beacon.getProximityUUID();
        this.mMinor = beacon.getMinor();
    }

    private BeaconId(Parcel parcel) {
        this.mProximityUuid = (UUID) parcel.readValue(UUID.class.getClassLoader());
        this.mMajor = parcel.readInt();
        this.mMinor = parcel.readInt();
    }


// ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getMajor() {
        return mMajor;
    }

    public int getMinor() {
        return mMinor;
    }

    public UUID getProximityUuid() {
        return mProximityUuid;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    /**
     * An UUID with value {@code null} is acting like a wildcard, which means two BeaconIds
     * are considered equal if their major and minors are equal and ONE of the UUIDs is null.
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BeaconId beaconId = (BeaconId) o;

        if (mMajor != beaconId.mMajor) return false;
        if (mMinor != beaconId.mMinor) return false;
        //return mProximityUuid == null || beaconId.mProximityUuid == null || mProximityUuid.equals(beaconId.mProximityUuid);
        return true;

    }

    @Override
    /**
     * The UUID is ignored for calculating the hashCode to achieve consitency with BeaconId#equals(Object)
     *
     * @see #equals(Object)
     */
    public int hashCode() {
        int result = /*mProximityUuid != null ? mProximityUuid.hashCode() : */ 0;
        result = 31 * result + mMajor;
        result = 31 * result + mMinor;
        return result;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mProximityUuid);
        dest.writeInt(this.mMajor);
        dest.writeInt(this.mMinor);
    }

    // ===========================================================
    // Methods
    // ===========================================================


    public String represent(){
        return getMajor()+"."+getMinor();
    }

    public String toString(){
        return represent();
    }

    public static BeaconId parse(String str){
        final String[] split = str.split("\\.");
        if(split.length != 2){
            throw new RuntimeException("Could not parse BeaconId "+str);
        }
        int major = Integer.parseInt(split[0]);
        int minor = Integer.parseInt(split[1]);
        return new BeaconId(null, major, minor);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
