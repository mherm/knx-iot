package de.hsrm.knx.knxbrowser.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.db.AndroidDatabaseManager;

public class LocationMapActivity extends AppCompatActivity {

    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = LocationMapActivity.class.getName();


    // ===========================================================
    // Fields
    // ===========================================================
    private MenuItem mLocateMeMenuItem;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        mLocateMeMenuItem = menu.findItem(R.id.action_locate_me);
        mLocateMeMenuItem.setIcon(R.drawable.ic_gps_fixed_white_24dp);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.action_show_scripts:
                startActivity(new Intent(this, ScriptsActivity.class));
                return true;
            case R.id.action_show_database_debug:
                startActivity(new Intent(this, AndroidDatabaseManager.class));
                return true;
            case R.id.action_locate_me:
                Intent i = new Intent(this, WebserviceActivity.class);
                i.setAction(WebserviceActivity.ACTION_LOCATE_ME);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // ===========================================================
    // Methods
    // ===========================================================

    private void initUI() {
        //Apply Layout
        setContentView(R.layout.activity_location_map);
        RelativeLayout content = (RelativeLayout)findViewById(R.id.content_map_view);


        //Create Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }


}
