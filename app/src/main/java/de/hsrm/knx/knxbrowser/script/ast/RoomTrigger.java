package de.hsrm.knx.knxbrowser.script.ast;

import de.hsrm.knx.knxbrowser.BeaconId;

/**
 * Created by Manuel Hermenau on 07.10.2016.
 */
public abstract class RoomTrigger implements Trigger {
    // ===========================================================
    // Constants
    // ===========================================================
    On mOn;
    BeaconId mBeaconId;
    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================
    protected RoomTrigger(On on, BeaconId beaconId){
        mOn = on;
        mBeaconId = beaconId;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
//    public boolean equals(Object t){
//        if(t == null)
//            return false;
//        if(t.getClass() != this.getClass())
//            return false;
//        RoomTrigger rt = (RoomTrigger)t;
//        return this.mBeaconId.equals(rt.mBeaconId);
//    }
//
//    @Override
//    public int hashCode() {
//        return mBeaconId.hashCode();
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomTrigger)) return false;

        RoomTrigger that = (RoomTrigger) o;

        if (mOn != that.mOn) return false;
        return mBeaconId != null ? mBeaconId.equals(that.mBeaconId) : that.mBeaconId == null;

    }

    @Override
    public int hashCode() {
        int result = mOn != null ? mOn.hashCode() : 0;
        result = 31 * result + (mBeaconId != null ? mBeaconId.hashCode() : 0);
        return result;
    }


    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    public static enum On {
        Enter, Leave
    }

    public static class EnterRoomTrigger extends RoomTrigger {
        public EnterRoomTrigger(BeaconId beaconId){super(On.Enter, beaconId);};
    }
    public static class LeaveRoomTrigger extends RoomTrigger {
        public LeaveRoomTrigger(BeaconId beaconId){super(On.Leave, beaconId);};
    }
}
