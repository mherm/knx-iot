package de.hsrm.knx.knxbrowser;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.joda.time.DateTime;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import obix.Obj;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class Utils {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public static URL getGatewayFromSharedPrefs(Context context){
        try {
            SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String url = sPrefs.getString(context.getString(R.string.preference_gatewayip_key), context.getString(R.string.preference_gatewayip_default));
            if(!url.contains("://"))
                url = "http://"+url;
            URL gateway = new URL(url);
            return gateway;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toUnixTs(DateTime dateTime) {
        return String.valueOf(dateTime.getMillis() / 1000);
    }

    public static long toUnixTsLong(DateTime dateTime) {
        return dateTime.getMillis() / 1000;
    }

    public static DateTime fromUnixTs(long ts) {
        return new DateTime(ts * 1000);
    }

    public static int eraseDate(DateTime dateTime){
        return (int) dateTime.getMillisOfDay();
    }

    public static DateTime nextDateTime(int millisOfDay){
        DateTime nextTime = new DateTime().withMillisOfDay(millisOfDay);
        if(nextTime.isAfterNow())
            return nextTime;
        return nextTime.plusDays(1);
    }

    /**
     *
     * @param first
     * @param rest
     * @return an UNMODIFIABLE List of the given Strings
     */
    public static final List<String> lst(String first, String... rest) {
        return Arrays.asList(arr(first, rest));
    }
    /**
     *
     * @param first
     * @param rest
     */
    public static final <T> List<T> lst(T first, T... rest) {
        List<T> result =  new ArrayList<T>(rest.length+1);
        result.add(first);
        result.addAll(Arrays.asList(rest));
        return result;
    }

    public static final String[] arr(String first, String... rest) {
        String[] result = new String[rest.length + 1];
        result[0] = first;
        for (int i = 1; i < rest.length; i++) {
            result[i] = rest[i - 1];
        }
        return result;
    }

    public static final boolean isReadable(Obj obj){
        final Obj readable = obj.get("readable");
        return readable != null && readable.getBool();
    }

    public static String[] strArr(int first, int... rest) {
        String[] result = new String[rest.length + 1];
        result[0] = String.valueOf(first);
        for (int i = 1; i < rest.length; i++) {
            result[i] = String.valueOf(rest[i - 1]);
        }
        return result;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
