package de.hsrm.knx.knxbrowser.ui.activities;

import de.hsrm.knx.knxbrowser.BeaconId;

/**
 * Created by Manuel Hermenau on 13.10.2016.
 */

public interface RoomInfoCallback {
    public void onRoomInfoReceived(BeaconId currentRoom);

}
