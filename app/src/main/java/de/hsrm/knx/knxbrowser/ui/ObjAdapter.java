package de.hsrm.knx.knxbrowser.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.ui.controls.IconFactory;
import de.hsrm.knx.knxbrowser.ui.controls.UiControlFactory;
import obix.Obj;

/**
 * Created by Manuel Hermenau on 19.09.2016.
 */
public class ObjAdapter extends ArrayAdapter<Obj> {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final Obj EMPTY_OBJ = new Obj("No Data");

    // ===========================================================
    // Fields
    // ===========================================================

    private final IconFactory mIconFactory = new IconFactory();
    private final UiControlFactory mUiControlFactory;

    // ===========================================================
    // Constructors
    // ===========================================================

    public ObjAdapter(Context context, List<Obj> items) {
        super(context, R.layout.view_obj_list_row, items);
        mUiControlFactory = new UiControlFactory(context);
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int getCount() {
        int cnt = super.getCount();
        if(cnt == 0)
            return 1;
        return cnt;
    }

    @Override
    public Obj getItem(int pos) {
        int cnt = super.getCount();
        if(cnt == 0)
            return null;
        return super.getItem(pos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        //Don't reuse Views because the programmatically added Buttons won't dissappear
        //FIXME: Implement ViewHolder? Remove generated views by Id?
//        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.view_obj_list_row, null);
//        }

        LinearLayout line1 = (LinearLayout) v.findViewById(R.id.ObjListTableRow01);
        LinearLayout line2 = (LinearLayout) v.findViewById(R.id.ObjListTableRow02);

        Obj obj = getItem(position);

        if (obj != null) {
            TextView tvName = (TextView) v.findViewById(R.id.obj_list_name);
            ImageView ivIcon = (ImageView) v.findViewById(R.id.obj_list_icon);
            TextView tvType = (TextView) v.findViewById(R.id.obj_list_type);
            TextView tvContent = (TextView) v.findViewById(R.id.obj_list_content);
            TextView tvValue = (TextView) v.findViewById(R.id.obj_list_value);

            if(ivIcon != null){
                ivIcon.setImageResource(mIconFactory.getIconResource(obj));
            }

            if (tvType != null) {
                String elementType = obj.getElement();
                tvType.setText(elementType);
            }

            String displayName = obj.getDisplayName();
            if(displayName == null || displayName.isEmpty())
                displayName = obj.getName().trim().replaceAll("\\s+", " ");


            if(obj.isVal()){
                String text = obj.toString();
                if(text.isEmpty()) {
                    text = "<empty>";
                    int color = getContext().getResources().getColor(R.color.dim_foreground_disabled_material_light);
                    tvName.setTextColor(color);
                }
                tvName.setText(text);
                tvContent.setText(displayName);
            }else {
                if (tvContent != null) {
                    String content;
                    content = obj.getIs().toString();
                    tvContent.setText(content);
                }
                if (tvName != null) {
                    tvName.setText(displayName);
                }

                View control = mUiControlFactory.make(obj, v);
                if (control != null) {
                    line1.addView(control);
                }
            }
        }

        return v;
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}