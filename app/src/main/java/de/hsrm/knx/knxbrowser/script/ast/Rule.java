package de.hsrm.knx.knxbrowser.script.ast;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Manuel Hermenau on 06.10.2016.
 */
public class Rule implements Statement{
    private static final String TAG = Rule.class.getName();
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    List<Trigger> mTriggers = new LinkedList<>();
    Block mRunBlock;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public Block getRunBlock() {
        return mRunBlock;
    }

    public List<Trigger> getTriggers() {
        return mTriggers;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
