package de.hsrm.knx.knxbrowser.script.ast;

import org.antlr.v4.runtime.misc.OrderedHashSet;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ReflectionUtil {

	private static Set<Field> getAllFieldsUntil(Set<Field> resultList,
			Class<?> forType, Class<?> untilSuperType) {
		resultList.addAll(Arrays.asList(forType.getDeclaredFields()));

		Class<?> superclass = forType.getSuperclass();
		if (superclass != untilSuperType && superclass != null) {
			getAllFieldsUntil(resultList, superclass, untilSuperType);
		}

		return resultList;
	}

	/**
	 * Traverses the object tree upwards until the given super-type
	 * is reached and collects all Fields of the Classes on the way, exclusively
	 * the ones of the <code>untilSuperType</code>.
	 *
	 * @param forType
	 * @param untilSuperType
	 * @return
	 */
	public static Set<Field> getAllFieldsUntil(Class<?> forType,
			Class<?> untilSuperType) {
		Set<Field> fields = new OrderedHashSet<>();
		return getAllFieldsUntil(fields, forType, untilSuperType);
	}

	public static Set<Field> getAllFields(Class<?> type) {
		return getAllFieldsUntil(type, Object.class);
	}
	
	public static Field getDeclaredField(Class<? extends Object> clazz,	String propertyName) throws NoSuchFieldException {
		return getDeclaredField(clazz, Object.class, propertyName);
	}
	
	public static Field getDeclaredField(Class<? extends Object> clazz, Class<?> untilSuperType, String propertyName) throws NoSuchFieldException {
		try{
			return clazz.getDeclaredField(propertyName);
		}catch(NoSuchFieldException e){
			Class<?> superclass = clazz.getSuperclass();
			if (superclass != untilSuperType && superclass != null) {
				return getDeclaredField(superclass, untilSuperType, propertyName);
			}
		}
		throw new NoSuchFieldException("No such field: "+propertyName);
	}

	public static Method[] getMethods(Class<?> clazz, String name){
		Method[] methods = clazz.getMethods();
		List<Method> result = new ArrayList<Method>(methods.length);
		for(Method m : methods){
			if(name.equals(m.getName())) result.add(m);
		}
		return result.toArray(new Method[result.size()]);
	}
	


}
