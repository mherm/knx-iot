package de.hsrm.knx.knxbrowser.net;

import obix.Obj;

/**
 * Created by Manuel Hermenau on 19.09.2016.
 */
public interface KnxBrowserView {
    public void onNewObj(Obj obj);
    public void onNavigationStart();
    public void onNavigationFailed(KnxNavigationFailedException exception);
}
