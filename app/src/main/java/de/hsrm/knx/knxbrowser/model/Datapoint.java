package de.hsrm.knx.knxbrowser.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import obix.Contract;

/**
 * Created by Manuel Hermenau on 21.09.2016.
 */
public enum Datapoint {

    // ===========================================================
    // Constants
    // ===========================================================

    DTP_1_001("knx:DPST-1-1", "switch",             "DPT_Switch",           new String[]{"Off", "On"}),
    DTP_1_002("knx:DPST-1-2", "bool",               "DPT_Bool",             new String[]{"False", "True"}),
    DTP_1_003("knx:DPST-1-3", "enable",             "DPT_Enable",           new String[]{"Disable", "Enable"}),
    DTP_1_004("knx:DPST-1-4", "ramp",               "DPT_Ramp",             new String[]{"No Ramp", "Ramp"}),
    DTP_1_005("knx:DPST-1-5", "alarm",              "DPT_Alarm",            new String[]{"No alarm","Alarm"}),
    DTP_1_006("knx:DPST-1-6", "binary_value",       "DPT_BinaryValue",      new String[]{"Low","High"}),
    DTP_1_007("knx:DPST-1-7", "step",               "DPT_Step",             new String[]{"Decrease","Increase"}),
    DTP_1_008("knx:DPST-1-8", "up_down",             "DPT_UpDown",          new String[]{"Up","Down"}),
    DTP_1_009("knx:DPST-1-9", "open_close",          "DPT_OpenClose",       new String[]{"Open","Close"}),
    DTP_1_010("knx:DPST-1-10","start",              "DPT_Start",            new String[]{"Stop","Start"}),
    DTP_1_011("knx:DPST-1-11","state",              "DPT_State",            new String[]{"Inactive","Active"}),
    DTP_1_012("knx:DPST-1-12","invert",             "DPT_Invert",           new String[]{"Not inverted","Inverted"}),
    DTP_1_013("knx:DPST-1-13","dim_send_style",       "DPT_DimSendStyle",   new String[]{"Start/stop","Cyclically"}),
    DTP_1_014("knx:DPST-1-14","inputSource",        "DPT_InputSource",      new String[]{"Fixed","Calculated"}),
    DTP_1_015("knx:DPST-1-15","reset",              "DPT_Reset",            new String[]{"no action","reset command"}),
    DTP_1_016("knx:DPST-1-16","ack",                "DPT_Ack",              new String[]{"no action","acknowledge command"}),
    DTP_1_017("knx:DPST-1-17","trigger",            "DPT_Trigger",          new String[]{"trigger","trigger "}),
    DTP_1_018("knx:DPST-1-18","occupancy",          "DPT_Occupancy",        new String[]{"not occupied","occupied"}),
    DTP_1_019("knx:DPST-1-19","window_door",        "DPT_Window_Door",      new String[]{"closed","open"}),
    //20 does not exists    
    DTP_1_021("knx:DPST-1-21","logical_function",    "DPT_LogicalFunction",  new String[]{"logical function OR","logical function AND"}),
    DTP_1_022("knx:DPST-1-22","scene_a_b",           "DPT_Scene_AB",         new String[]{"scene A","scene B"}),
    DTP_1_023("knx:DPST-1-23","shutter_blinds_mode", "DPT_ShutterBlinds_Mode",new String[]{"only move Up/Down mode (shutter)","move Up/Down + StepStop mode (blind)"}),


    DTP_2_001("knx:DPST-2-1", "switch_control", "DPT_Switch_Control",           new String[]{"No Control", "No Control", "Control. Function value 0", "Control. Function value 1"}),
    DTP_2_002("knx:DPST-2-2", "bool_control", "DPT_Bool_Control", 		        new String[]{"False", "True"}),
    DTP_2_003("knx:DPST-2-3", "enable_control", "DPT_Enable_Control", 		    new String[]{"Disable", "Enable"}),
    DTP_2_004("knx:DPST-2-4", "ramp_control", "DPT_Ramp_Control", 		        new String[]{"No Ramp", "Ramp"}),
    DTP_2_005("knx:DPST-2-5", "alarm_control", "DPT_Alarm_Control", 		    new String[]{"No alarm","Alarm"}),
    DTP_2_006("knx:DPST-2-6", "binaryValue_control", "DPT_BinaryValue_Control", new String[]{"Low","High"}),
    DTP_2_007("knx:DPST-2-7", "step_control", "DPT_Step_Control", 		        new String[]{"Decrease","Increase"}),
    DTP_2_008("knx:DPST-2-8", "upDown_control", "DPT_UpDown_Control", 		    new String[]{"Up","Down"}),
    DTP_2_009("knx:DPST-2-9", "openClose_control", "DPT_OpenClose_Control", 	new String[]{"Open","Close"}),
    DTP_2_010("knx:DPST-2-10","start_control", "DPT_Start_Control", 		    new String[]{"Stop","Start"}),
    DTP_2_011("knx:DPST-2-11","state_control", "DPT_State_Control", 		    new String[]{"Inactive","Active"}),
    DTP_2_012("knx:DPST-2-12","invert_control", "DPT_Invert_Control", 		    new String[]{"Not inverted","Inverted"}),

    DTP_3_007("knx:DPST-3-7","control_dimming", "DPT_Control_Dimming", 		    new String[]{}),
    DTP_3_008("knx:DPST-3-8","invert_control", "DPT_Control_Blinds", 		    new String[]{}),


    DTP_4_001("knx:DPST-4-1","char_ascii", "DPT_Char_ASCII", new String[]{}),
    DTP_4_002("knx:DPST-4-2",				"char_8859_1"	,"DPT_Char_8859_1",	 new String[]{}),
    DTP_5_001("knx:DPST-5-1",				"percentage (0..100%)","DPT_Scaling",	 new String[]{}),
    DTP_5_003("knx:DPST-5-3",				"angle","DPT_Angle",	 new String[]{}),
    DTP_5_004("knx:DPST-5-4",				"percent_u8"	,"DPT_Percent_U8",	 new String[]{}),
    DTP_5_005("knx:DPST-5-5",				"decimal_factor"		,"DPT_DecimalFactor",	 new String[]{}),
    DTP_5_006("knx:DPST-5-6",				"tariff","DPT_Tariff",	 new String[]{}),
    DTP_5_010("knx:DPST-5-10",				"1_ucount"		,"DPT_Value_1_Ucount",	 new String[]{}),
    DTP_6_001("knx:DPST-6-1",				"percent_v8"	,"DPT_Percent_V8",	 new String[]{}),
    DTP_6_010("knx:DPST-6-10",				"1_count"		,"DPT_Value_1_Count",	 new String[]{}),
    DTP_6_020("knx:DPST-6-20",				"status_mode3"	,"DPT_Status_Mode3",	 new String[]{}),
    DTP_7_001("knx:DPST-7-1",				"2_ucount"		,"DPT_Value_2_Ucount",	 new String[]{}),
    DTP_7_002("knx:DPST-7-2",				"time_period_msec"		,"DPT_TimePeriodMsec",	 new String[]{}),
    DTP_7_003("knx:DPST-7-3",				"time_period10_m_sec"		,"DPT_TimePeriod10MSec",	 new String[]{}),
    DTP_7_004("knx:DPST-7-4",				"time_period100_m_sec"			,"DPT_TimePeriod100MSec",	 new String[]{}),
    DTP_7_005("knx:DPST-7-5",				"time_period_sec"		,"DPT_TimePeriodSec",	 new String[]{}),
    DTP_7_006("knx:DPST-7-6",				"time_period_min"		,"DPT_TimePeriodMin",	 new String[]{}),
    DTP_7_007("knx:DPST-7-7",				"time_period_hrs"		,"DPT_TimePeriodHrs",	 new String[]{}),
    DTP_7_010("knx:DPST-7-10",				"prop_data_type"	,"DPT_PropDataType",	 new String[]{}),
    DTP_7_011("knx:DPST-7-11",				"length_mm"	,"DPT_Length_mm",	 new String[]{}),
    DTP_7_012("knx:DPST-7-12",				"u_el_currentm_a"	,"DPT_UElCurrentmA",	 new String[]{}),
    DTP_7_013("knx:DPST-7-13",				"brightness"	,"DPT_Brightness",	 new String[]{}),
    DTP_8_001("knx:DPST-8-1",				"2_count"		,"DPT_Value_2_Count",	 new String[]{}),
    DTP_8_002("knx:DPST-8-2",				"delta_time_msec"		,"DPT_DeltaTimeMsec",	 new String[]{}),
    DTP_8_003("knx:DPST-8-3",				"delta_time10_m_sec"		,"DPT_DeltaTime10MSec",	 new String[]{}),
    DTP_8_004("knx:DPST-8-4",				"delta_time100_m_sec"		,"DPT_DeltaTime100MSec",	 new String[]{}),
    DTP_8_005("knx:DPST-8-5",				"delta_time_sec"	,"DPT_DeltaTimeSec",	 new String[]{}),
    DTP_8_006("knx:DPST-8-6",				"delta_time_min"	,"DPT_DeltaTimeMin",	 new String[]{}),
    DTP_8_007("knx:DPST-8-7",				"delta_time_hrs"	,"DPT_DeltaTimeHrs",	 new String[]{}),
    DTP_8_010("knx:DPST-8-10",				"percent_v16"	,"DPT_Percent_V16",	 new String[]{}),
    DTP_8_011("knx:DPST-8-11",				"rotation_angle"		,"DPT_Rotation_Angle",	 new String[]{}),
    DTP_9_001("knx:DPST-9-1",				"temp"	,"DPT_Value_Temp",	 new String[]{}),
    DTP_9_002("knx:DPST-9-2",				"tempd"	,"DPT_Value_Tempd",	 new String[]{}),
    DTP_9_003("knx:DPST-9-3",				"tempa"	,"DPT_Value_Tempa",	 new String[]{}),
    DTP_9_004("knx:DPST-9-4",				"lux"	,"DPT_Value_Lux",	 new String[]{}),
    DTP_9_005("knx:DPST-9-5",				"wsp"	,"DPT_Value_Wsp",	 new String[]{}),
    DTP_9_006("knx:DPST-9-6",				"pres"	,"DPT_Value_Pres",	 new String[]{}),
    DTP_9_007("knx:DPST-9-7",				"humidity"		,"DPT_Value_Humidity",	 new String[]{}),
    DTP_9_008("knx:DPST-9-8",				"air_quality"		,"DPT_Value_AirQuality",	 new String[]{}),
    DTP_9_010("knx:DPST-9-10",				"time1"	,"DPT_Value_Time1",	 new String[]{}),
    DTP_9_011("knx:DPST-9-11",				"time2"	,"DPT_Value_Time2",	 new String[]{}),
    DTP_9_020("knx:DPST-9-20",				"volt"	,"DPT_Value_Volt",	 new String[]{}),
    DTP_9_021("knx:DPST-9-21",				"curr"	,"DPT_Value_Curr",	 new String[]{}),
    DTP_9_022("knx:DPST-9-22",				"power_density"	,"DPT_PowerDensity",	 new String[]{}),
    DTP_9_023("knx:DPST-9-23",				"kelvin_per_percent"		,"DPT_KelvinPerPercent",	 new String[]{}),
    DTP_9_024("knx:DPST-9-24",				"power","DPT_Power",	 new String[]{}),
    DTP_9_025("knx:DPST-9-25",				"volume_flow"			,"DPT_Value_Volume_Flow",	 new String[]{}),
    DTP_9_026("knx:DPST-9-26",				"rain_amount"	,"DPT_Rain_Amount",	 new String[]{}),
    DTP_9_027("knx:DPST-9-27",				"temp_f"	,"DPT_Value_Temp_F",	 new String[]{}),
    DTP_9_028("knx:DPST-9-28",				"wsp_kmh"		,"DPT_Value_Wsp_kmh",	 new String[]{}),
    DTP_10_001("knx:DPST-10-1",			"time_of_day"		,"DPT_TimeOfDay",	 new String[]{}),
    DTP_11_001("knx:DPST-11-1",			"date","DPT_Date",	 new String[]{}),
    DTP_12_001("knx:DPST-12-1",			"4_ucount"			,"DPT_Value_4_Ucount",	 new String[]{}),
    DTP_13_001("knx:DPST-13-1",			"4_count"			,"DPT_Value_4_Count",	 new String[]{}),
    DTP_13_002("knx:DPST-13-2",			"flow_rate_m3/h"			,"DPT_FlowRate_m3/h",	 new String[]{}),
    DTP_13_010("knx:DPST-13-10",			"active_energy"		,"DPT_ActiveEnergy",	 new String[]{}),
    DTP_13_011("knx:DPST-13-11",			"apparant_energy"			,"DPT_ApparantEnergy",	 new String[]{}),
    DTP_13_012("knx:DPST-13-12",			"reactive_energy"			,"DPT_ReactiveEnergy",	 new String[]{}),
    DTP_13_013("knx:DPST-13-13",			"active_energy_k_wh"			,"DPT_ActiveEnergy_kWh",	 new String[]{}),
    DTP_13_014("knx:DPST-13-14",			"apparant_energy_k_v_ah"				,"DPT_ApparantEnergy_kVAh",	 new String[]{}),
    DTP_13_015("knx:DPST-13-15",			"reactive_energy_k_va_rh"				,"DPT_ReactiveEnergy_kVARh",	 new String[]{}),
    DTP_13_100("knx:DPST-13-100",			"long_delta_time_sec"			,"DPT_LongDeltaTimeSec",	 new String[]{}),
    DTP_14_000("knx:DPST-14-0",			"acceleration"				,"DPT_Value_Acceleration",	 new String[]{}),
    DTP_14_001("knx:DPST-14-1",			"acceleration_angular"						,"DPT_Value_Acceleration_Angular",	 new String[]{}),
    DTP_14_002("knx:DPST-14-2",			"activation_energy"					,"DPT_Value_Activation_Energy",	 new String[]{}),
    DTP_14_003("knx:DPST-14-3",			"activity"			,"DPT_Value_Activity",	 new String[]{}),
    DTP_14_004("knx:DPST-14-4",			"mol"		,"DPT_Value_Mol",	 new String[]{}),
    DTP_14_005("knx:DPST-14-5",			"amplitude"			,"DPT_Value_Amplitude",	 new String[]{}),
    DTP_14_006("knx:DPST-14-6",			"angle_rad"			,"DPT_Value_AngleRad",	 new String[]{}),
    DTP_14_007("knx:DPST-14-7",			"angle_deg"			,"DPT_Value_AngleDeg",	 new String[]{}),
    DTP_14_008("knx:DPST-14-8",			"angular_momentum"					,"DPT_Value_Angular_Momentum",	 new String[]{}),
    DTP_14_009("knx:DPST-14-9",			"angular_velocity"					,"DPT_Value_Angular_Velocity",	 new String[]{}),
    DTP_14_010("knx:DPST-14-10",			"area"		,"DPT_Value_Area",	 new String[]{}),
    DTP_14_011("knx:DPST-14-11",			"capacitance"				,"DPT_Value_Capacitance",	 new String[]{}),
    DTP_14_012("knx:DPST-14-12",			"charge_density_surface"						,"DPT_Value_Charge_DensitySurface",	 new String[]{}),
    DTP_14_013("knx:DPST-14-13",			"charge_density_volume"						,"DPT_Value_Charge_DensityVolume",	 new String[]{}),
    DTP_14_014("knx:DPST-14-14",			"compressibility"					,"DPT_Value_Compressibility",	 new String[]{}),
    DTP_14_015("knx:DPST-14-15",			"conductance"				,"DPT_Value_Conductance",	 new String[]{}),
    DTP_14_016("knx:DPST-14-16",			"electrical_conductivity"							,"DPT_Value_Electrical_Conductivity",	 new String[]{}),
    DTP_14_017("knx:DPST-14-17",			"density"			,"DPT_Value_Density",	 new String[]{}),
    DTP_14_018("knx:DPST-14-18",			"electric_charge"					,"DPT_Value_Electric_Charge",	 new String[]{}),
    DTP_14_019("knx:DPST-14-19",			"electric_current"					,"DPT_Value_Electric_Current",	 new String[]{}),
    DTP_14_020("knx:DPST-14-20",			"electric_current_density"							,"DPT_Value_Electric_CurrentDensity",	 new String[]{}),
    DTP_14_021("knx:DPST-14-21",			"electric_dipole_moment"						,"DPT_Value_Electric_DipoleMoment",	 new String[]{}),
    DTP_14_022("knx:DPST-14-22",			"electric_displacement"						,"DPT_Value_Electric_Displacement",	 new String[]{}),
    DTP_14_023("knx:DPST-14-23",			"electric_field_strength"						,"DPT_Value_Electric_FieldStrength",	 new String[]{}),
    DTP_14_024("knx:DPST-14-24",			"electric_flux"				,"DPT_Value_Electric_Flux",	 new String[]{}),
    DTP_14_025("knx:DPST-14-25",			"electric_flux_density"						,"DPT_Value_Electric_FluxDensity",	 new String[]{}),
    DTP_14_026("knx:DPST-14-26",			"electric_polarization"						,"DPT_Value_Electric_Polarization",	 new String[]{}),
    DTP_14_027("knx:DPST-14-27",			"electric_potential"					,"DPT_Value_Electric_Potential",	 new String[]{}),
    DTP_14_028("knx:DPST-14-28",			"electric_potential_difference"									,"DPT_Value_Electric_PotentialDifference",	 new String[]{}),
    DTP_14_029("knx:DPST-14-29",			"electromagnetic_moment"						,"DPT_Value_ElectromagneticMoment",	 new String[]{}),
    DTP_14_030("knx:DPST-14-30",			"electromotive_force"						,"DPT_Value_Electromotive_Force",	 new String[]{}),
    DTP_14_031("knx:DPST-14-31",			"energy"		,"DPT_Value_Energy",	 new String[]{}),
    DTP_14_032("knx:DPST-14-32",			"force"		,"DPT_Value_Force",	 new String[]{}),
    DTP_14_033("knx:DPST-14-33",			"frequency"			,"DPT_Value_Frequency",	 new String[]{}),
    DTP_14_034("knx:DPST-14-34",			"angular_frequency"					,"DPT_Value_Angular_Frequency",	 new String[]{}),
    DTP_14_035("knx:DPST-14-35",			"heat_capacity"				,"DPT_Value_Heat_Capacity",	 new String[]{}),
    DTP_14_036("knx:DPST-14-36",			"heat_flow_rate"				,"DPT_Value_Heat_FlowRate",	 new String[]{}),
    DTP_14_037("knx:DPST-14-37",			"heat_quantity"				,"DPT_Value_Heat_Quantity",	 new String[]{}),
    DTP_14_038("knx:DPST-14-38",			"impedance"			,"DPT_Value_Impedance",	 new String[]{}),
    DTP_14_039("knx:DPST-14-39",			"length"		,"DPT_Value_Length",	 new String[]{}),
    DTP_14_040("knx:DPST-14-40",			"light_quantity"				,"DPT_Value_Light_Quantity",	 new String[]{}),
    DTP_14_041("knx:DPST-14-41",			"luminance"			,"DPT_Value_Luminance",	 new String[]{}),
    DTP_14_042("knx:DPST-14-42",			"luminous_flux"				,"DPT_Value_Luminous_Flux",	 new String[]{}),
    DTP_14_043("knx:DPST-14-43",			"luminous_intensity"					,"DPT_Value_Luminous_Intensity",	 new String[]{}),
    DTP_14_044("knx:DPST-14-44",			"magnetic_field_strength"						,"DPT_Value_Magnetic_FieldStrength",	 new String[]{}),
    DTP_14_045("knx:DPST-14-45",			"magnetic_flux"				,"DPT_Value_Magnetic_Flux",	 new String[]{}),
    DTP_14_046("knx:DPST-14-46",			"magnetic_flux_density"						,"DPT_Value_Magnetic_FluxDensity",	 new String[]{}),
    DTP_14_047("knx:DPST-14-47",			"magnetic_moment"					,"DPT_Value_Magnetic_Moment",	 new String[]{}),
    DTP_14_048("knx:DPST-14-48",			"magnetic_polarization"						,"DPT_Value_Magnetic_Polarization",	 new String[]{}),
    DTP_14_049("knx:DPST-14-49",			"magnetization"				,"DPT_Value_Magnetization",	 new String[]{}),
    DTP_14_050("knx:DPST-14-50",			"magnetomotive_force"					,"DPT_Value_MagnetomotiveForce",	 new String[]{}),
    DTP_14_051("knx:DPST-14-51",			"mass"		,"DPT_Value_Mass",	 new String[]{}),
    DTP_14_052("knx:DPST-14-52",			"mass_flux"			,"DPT_Value_MassFlux",	 new String[]{}),
    DTP_14_053("knx:DPST-14-53",			"momentum"			,"DPT_Value_Momentum",	 new String[]{}),
    DTP_14_054("knx:DPST-14-54",			"phase_angle_rad"				,"DPT_Value_Phase_AngleRad",	 new String[]{}),
    DTP_14_055("knx:DPST-14-55",			"phase_angle_deg"				,"DPT_Value_Phase_AngleDeg",	 new String[]{}),
    DTP_14_056("knx:DPST-14-56",			"power"		,"DPT_Value_Power",	 new String[]{}),
    DTP_14_057("knx:DPST-14-57",			"power_factor"				,"DPT_Value_Power_Factor",	 new String[]{}),
    DTP_14_058("knx:DPST-14-58",			"pressure"			,"DPT_Value_Pressure",	 new String[]{}),
    DTP_14_059("knx:DPST-14-59",			"reactance"			,"DPT_Value_Reactance",	 new String[]{}),
    DTP_14_060("knx:DPST-14-60",			"resistance"			,"DPT_Value_Resistance",	 new String[]{}),
    DTP_14_061("knx:DPST-14-61",			"resistivity"				,"DPT_Value_Resistivity",	 new String[]{}),
    DTP_14_062("knx:DPST-14-62",			"self_inductance"				,"DPT_Value_SelfInductance",	 new String[]{}),
    DTP_14_063("knx:DPST-14-63",			"solid_angle"			,"DPT_Value_SolidAngle",	 new String[]{}),
    DTP_14_064("knx:DPST-14-64",			"sound_intensity"					,"DPT_Value_Sound_Intensity",	 new String[]{}),
    DTP_14_065("knx:DPST-14-65",			"speed"		,"DPT_Value_Speed",	 new String[]{}),
    DTP_14_066("knx:DPST-14-66",			"stress"		,"DPT_Value_Stress",	 new String[]{}),
    DTP_14_067("knx:DPST-14-67",			"surface_tension"					,"DPT_Value_Surface_Tension",	 new String[]{}),
    DTP_14_068("knx:DPST-14-68",			"common_temperature"					,"DPT_Value_Common_Temperature",	 new String[]{}),
    DTP_14_069("knx:DPST-14-69",			"absolute_temperature"						,"DPT_Value_Absolute_Temperature",	 new String[]{}),
    DTP_14_070("knx:DPST-14-70",			"temperature_difference"						,"DPT_Value_TemperatureDifference",	 new String[]{}),
    DTP_14_071("knx:DPST-14-71",			"thermal_capacity"					,"DPT_Value_Thermal_Capacity",	 new String[]{}),
    DTP_14_072("knx:DPST-14-72",			"thermal_conductivity"						,"DPT_Value_Thermal_Conductivity",	 new String[]{}),
    DTP_14_073("knx:DPST-14-73",			"thermoelectric_power"						,"DPT_Value_ThermoelectricPower",	 new String[]{}),
    DTP_14_074("knx:DPST-14-74",			"time"		,"DPT_Value_Time",	 new String[]{}),
    DTP_14_075("knx:DPST-14-75",			"torque"		,"DPT_Value_Torque",	 new String[]{}),
    DTP_14_076("knx:DPST-14-76",			"volume"		,"DPT_Value_Volume",	 new String[]{}),
    DTP_14_077("knx:DPST-14-77",			"volume_flux"				,"DPT_Value_Volume_Flux",	 new String[]{}),
    DTP_14_078("knx:DPST-14-78",			"weight"		,"DPT_Value_Weight",	 new String[]{}),
    DTP_14_079("knx:DPST-14-79",			"work"		,"DPT_Value_Work",	 new String[]{}),
    DTP_15_000("knx:DPST-15-0",			"access_data"		,"DPT_Access_Data",	 new String[]{}),
    DTP_16_000("knx:DPST-16-0",			"string_ascii"		,"DPT_String_ASCII",	 new String[]{}),
    DTP_16_001("knx:DPST-16-1",			"string_8859_1"			,"DPT_String_8859_1",	 new String[]{}),
    DTP_17_001("knx:DPST-17-1",			"scene_number"		,"DPT_SceneNumber",	 new String[]{}),
    DTP_18_001("knx:DPST-18-1",			"scene_control"		,"DPT_SceneControl",	 new String[]{}),
    DTP_19_001("knx:DPST-19-1",			"date_time"	,"DPT_DateTime",	 new String[]{}),
    DTP_20_001("knx:DPST-20-1",			"sclo_mode"	,"DPT_SCLOMode",	 new String[]{}),
    DTP_20_002("knx:DPST-20-2",			"building_mode"		,"DPT_BuildingMode",	 new String[]{}),
    DTP_20_003("knx:DPST-20-3",			"occ_mode"	,"DPT_OccMode",	 new String[]{}),
    DTP_20_004("knx:DPST-20-4",			"priority"	,"DPT_Priority",	 new String[]{}),
    DTP_20_005("knx:DPST-20-5",			"light_application_mode"				,"DPT_LightApplicationMode",	 new String[]{}),
    DTP_20_006("knx:DPST-20-6",			"application_area"			,"DPT_ApplicationArea",	 new String[]{}),
    DTP_20_007("knx:DPST-20-7",			"alarm_class_type"			,"DPT_AlarmClassType",	 new String[]{}),
    DTP_20_008("knx:DPST-20-8",			"psu_mode"	,"DPT_PSUMode",	 new String[]{}),
    DTP_20_011("knx:DPST-20-11",			"error_class_system"				,"DPT_ErrorClass_System",	 new String[]{}),
    DTP_20_012("knx:DPST-20-12",			"error_class_hvac"			,"DPT_ErrorClass_HVAC",	 new String[]{}),
    DTP_20_013("knx:DPST-20-13",			"time_delay"		,"DPT_Time_Delay",	 new String[]{}),
    DTP_20_014("knx:DPST-20-14",			"beaufort_wind_force_scale"						,"DPT_Beaufort_Wind_Force_Scale",	 new String[]{}),
    DTP_20_017("knx:DPST-20-17",			"sensor_select"		,"DPT_SensorSelect",	 new String[]{}),
    DTP_20_020("knx:DPST-20-20",			"actuator_connect_type"				,"DPT_ActuatorConnectType",	 new String[]{}),
    DTP_20_100("knx:DPST-20-100",			"fuel_type"	,"DPT_FuelType",	 new String[]{}),
    DTP_20_101("knx:DPST-20-101",			"burner_type"		,"DPT_BurnerType",	 new String[]{}),
    DTP_20_102("knx:DPST-20-102",			"hvac_mode"	,"DPT_HVACMode",	 new String[]{}),
    DTP_20_103("knx:DPST-20-103",			"dhw_mode"	,"DPT_DHWMode",	 new String[]{}),
    DTP_20_104("knx:DPST-20-104",			"load_priority"		,"DPT_LoadPriority",	 new String[]{}),
    DTP_20_105("knx:DPST-20-105",			"hvac_contr_mode"			,"DPT_HVACContrMode",	 new String[]{}),
    DTP_20_106("knx:DPST-20-106",			"hvac_emerg_mode"			,"DPT_HVACEmergMode",	 new String[]{}),
    DTP_20_107("knx:DPST-20-107",			"changeover_mode"			,"DPT_ChangeoverMode",	 new String[]{}),
    DTP_20_108("knx:DPST-20-108",			"valve_mode"		,"DPT_ValveMode",	 new String[]{}),
    DTP_20_109("knx:DPST-20-109",			"damper_mode"		,"DPT_DamperMode",	 new String[]{}),
    DTP_20_110("knx:DPST-20-110",			"heater_mode"		,"DPT_HeaterMode",	 new String[]{}),
    DTP_20_111("knx:DPST-20-111",			"fan_mode"	,"DPT_FanMode",	 new String[]{}),
    DTP_20_112("knx:DPST-20-112",			"master_slave_mode"			,"DPT_MasterSlaveMode",	 new String[]{}),
    DTP_20_113("knx:DPST-20-113",			"status_room_setp"			,"DPT_StatusRoomSetp",	 new String[]{}),
    DTP_20_120("knx:DPST-20-120",			"ada_type"	,"DPT_ADAType",	 new String[]{}),
    DTP_20_121("knx:DPST-20-121",			"backup_mode"		,"DPT_BackupMode",	 new String[]{}),
    DTP_20_122("knx:DPST-20-122",			"start_synchronization"				,"DPT_StartSynchronization",	 new String[]{}),
    DTP_20_600("knx:DPST-20-600",			"behaviour_lock_unlock"					,"DPT_Behaviour_Lock_Unlock",	 new String[]{}),
    DTP_20_601("knx:DPST-20-601",			"behaviour_bus_power_up_down"						,"DPT_Behaviour_Bus_Power_Up_Down",	 new String[]{}),
    DTP_20_602("knx:DPST-20-602",			"dali_fade_time"			,"DPT_DALI_Fade_Time",	 new String[]{}),
    DTP_20_603("knx:DPST-20-603",			"blinking_mode"		,"DPT_BlinkingMode",	 new String[]{}),
    DTP_20_604("knx:DPST-20-604",			"light_control_mode"			,"DPT_LightControlMode",	 new String[]{}),
    DTP_20_605("knx:DPST-20-605",			"switch_pb_model"			,"DPT_SwitchPBModel",	 new String[]{}),
    DTP_20_606("knx:DPST-20-606",			"pb_action"	,"DPT_PBAction",	 new String[]{}),
    DTP_20_607("knx:DPST-20-607",			"dimm_pb_model"		,"DPT_DimmPBModel",	 new String[]{}),
    DTP_20_608("knx:DPST-20-608",			"switch_on_mode"		,"DPT_SwitchOnMode",	 new String[]{}),
    DTP_20_609("knx:DPST-20-609",			"load_type_set"		,"DPT_LoadTypeSet",	 new String[]{}),
    DTP_20_610("knx:DPST-20-610",			"load_type_detected"			,"DPT_LoadTypeDetected",	 new String[]{}),
    DTP_20_801("knx:DPST-20-801",			"sab_except_behaviour"				,"DPT_SABExceptBehaviour",	 new String[]{}),
    DTP_20_802("knx:DPST-20-802",			"sab_behaviour_lock_unlock"					,"DPT_SABBehaviour_Lock_Unlock",	 new String[]{}),
    DTP_20_803("knx:DPST-20-803",			"sssb_mode"	,"DPT_SSSBMode",	 new String[]{}),
    DTP_20_804("knx:DPST-20-804",			"blinds_control_mode"				,"DPT_BlindsControlMode",	 new String[]{}),
    DTP_20_1000("knx:DPST-20-1000",			"comm_mode"	,"DPT_CommMode",	 new String[]{}),
    DTP_20_1001("knx:DPST-20-1001",			"add_info_types"		,"DPT_AddInfoTypes",	 new String[]{}),
    DTP_20_1002("knx:DPST-20-1002",			"rf_mode_select"			,"DPT_RF_ModeSelect",	 new String[]{}),
    DTP_20_1003("knx:DPST-20-1003",			"rf_filter_select"			,"DPT_RF_FilterSelect",	 new String[]{}),
    DTP_21_001("knx:DPST-21-1",			"status_gen"		,"DPT_StatusGen",	 new String[]{}),
    DTP_21_002("knx:DPST-21-2",			"device_control"			,"DPT_Device_Control",	 new String[]{}),
    DTP_21_100("knx:DPST-21-100",			"force_sign"		,"DPT_ForceSign",	 new String[]{}),
    DTP_21_101("knx:DPST-21-101",			"force_sign_cool"			,"DPT_ForceSignCool",	 new String[]{}),
    DTP_21_102("knx:DPST-21-102",			"status_rhc"		,"DPT_StatusRHC",	 new String[]{}),
    DTP_21_103("knx:DPST-21-103",			"status_sdhwc"		,"DPT_StatusSDHWC",	 new String[]{}),
    DTP_21_104("knx:DPST-21-104",			"fuel_type_set"		,"DPT_FuelTypeSet",	 new String[]{}),
    DTP_21_105("knx:DPST-21-105",			"status_rcc"		,"DPT_StatusRCC",	 new String[]{}),
    DTP_21_106("knx:DPST-21-106",			"status_ahu"		,"DPT_StatusAHU",	 new String[]{}),
    DTP_21_601("knx:DPST-21-601",			"light_actuator_error_info"					,"DPT_LightActuatorErrorInfo",	 new String[]{}),
    DTP_21_1000("knx:DPST-21-1000",			"rf_mode_info"		,"DPT_RF_ModeInfo",	 new String[]{}),
    DTP_21_1001("knx:DPST-21-1001",			"rf_filter_info"			,"DPT_RF_FilterInfo",	 new String[]{}),
    DTP_21_1010("knx:DPST-21-1010",			"channel_activation_8"				,"DPT_Channel_Activation_8",	 new String[]{}),
    DTP_22_100("knx:DPST-22-100",			"status_dhwc"		,"DPT_StatusDHWC",	 new String[]{}),
    DTP_22_101("knx:DPST-22-101",			"status_rhcc"		,"DPT_StatusRHCC",	 new String[]{}),
    DTP_22_1000("knx:DPST-22-1000",			"media"	,"DPT_Media",	 new String[]{}),
    DTP_22_1010("knx:DPST-22-1010",			"channel_activation_16"					,"DPT_Channel_Activation_16",	 new String[]{}),
    DTP_23_001("knx:DPST-23-1",			"on_off_action"		,"DPT_OnOff_Action",	 new String[]{}),
    DTP_23_002("knx:DPST-23-2",			"alarm_reaction"			,"DPT_Alarm_Reaction",	 new String[]{}),
    DTP_23_003("knx:DPST-23-3",			"up_down_action"			,"DPT_UpDown_Action",	 new String[]{}),
    DTP_23_102("knx:DPST-23-102",			"hvac_pb_action"			,"DPT_HVAC_PB_Action",	 new String[]{}),
    DTP_24_001("knx:DPST-24-1",			"var_string_8859_1"			,"DPT_VarString_8859_1",	 new String[]{}),
    DTP_25_1000("knx:DPST-25-1000",			"double_nibble"		,"DPT_DoubleNibble",	 new String[]{}),
    DTP_26_001("knx:DPST-26-1",			"scene_info"		,"DPT_SceneInfo",	 new String[]{}),
    DTP_27_001("knx:DPST-27-1",			"combined_info_on_off"				,"DPT_CombinedInfoOnOff",	 new String[]{}),
    DTP_28_001("knx:DPST-28-1",			"utf-8"	,"DPT_UTF-8",	 new String[]{}),
    DTP_29_010("knx:DPST-29-10",			"active_energy_v64"			,"DPT_ActiveEnergy_V64",	 new String[]{}),
    DTP_29_011("knx:DPST-29-11",			"apparant_energy_v64"				,"DPT_ApparantEnergy_V64",	 new String[]{}),
    DTP_29_012("knx:DPST-29-12",			"reactive_energy_v64"				,"DPT_ReactiveEnergy_V64",	 new String[]{}),
    DTP_30_1010("knx:DPST-30-1010",			"channel_activation_24"					,"DPT_Channel_Activation_24",	 new String[]{}),
    DTP_31_101("knx:DPST-31-101",			"pb_action_hvac_extended"					,"DPT_PB_Action_HVAC_Extended",	 new String[]{}),
    DTP_200_100("knx:DPST-200-100",			"heat/_cool_z"		,"DPT_Heat/Cool_Z",	 new String[]{}),
    DTP_200_101("knx:DPST-200-101",			"binary_value_z"			,"DPT_BinaryValue_Z",	 new String[]{}),
    DTP_201_100("knx:DPST-201-100",			"hvac_mode_z"		,"DPT_HVACMode_Z",	 new String[]{}),
    DTP_201_102("knx:DPST-201-102",			"dhw_mode_z"		,"DPT_DHWMode_Z",	 new String[]{}),
    DTP_201_104("knx:DPST-201-104",			"hvac_contr_mode_z"			,"DPT_HVACContrMode_Z",	 new String[]{}),
    DTP_201_105("knx:DPST-201-105",			"enabl_h/_cstage_z dpt_enabl_h/c_stage"								,"DPT_EnablH/Cstage_Z DPT_EnablH/CStage",	 new String[]{}),
    DTP_201_107("knx:DPST-201-107",			"building_mode_z"			,"DPT_BuildingMode_Z",	 new String[]{}),
    DTP_201_108("knx:DPST-201-108",			"occ_mode_z"		,"DPT_OccMode_Z",	 new String[]{}),
    DTP_201_109("knx:DPST-201-109",			"hvac_emerg_mode_z"			,"DPT_HVACEmergMode_Z",	 new String[]{}),
    DTP_202_001("knx:DPST-202-1",			"rel_value_z"		,"DPT_RelValue_Z",	 new String[]{}),
    DTP_202_002("knx:DPST-202-2",			"u_count_value8_z"			,"DPT_UCountValue8_Z",	 new String[]{}),
    DTP_203_002("knx:DPST-203-2",			"time_period_msec_z"			,"DPT_TimePeriodMsec_Z",	 new String[]{}),
    DTP_203_003("knx:DPST-203-3",			"time_period10_msec_z"				,"DPT_TimePeriod10Msec_Z",	 new String[]{}),
    DTP_203_004("knx:DPST-203-4",			"time_period100_msec_z"				,"DPT_TimePeriod100Msec_Z",	 new String[]{}),
    DTP_203_005("knx:DPST-203-5",			"time_period_sec_z"			,"DPT_TimePeriodSec_Z",	 new String[]{}),
    DTP_203_006("knx:DPST-203-6",			"time_period_min_z"			,"DPT_TimePeriodMin_Z",	 new String[]{}),
    DTP_203_007("knx:DPST-203-7",			"time_period_hrs_z"			,"DPT_TimePeriodHrs_Z",	 new String[]{}),
    DTP_203_011("knx:DPST-203-11",			"u_flow_rate_liter/h_z"				,"DPT_UFlowRateLiter/h_Z",	 new String[]{}),
    DTP_203_012("knx:DPST-203-12",			"u_count_value16_z"			,"DPT_UCountValue16_Z",	 new String[]{}),
    DTP_203_013("knx:DPST-203-13",			"u_el_currentμa_z"			,"DPT_UElCurrentμA_Z",	 new String[]{}),
    DTP_203_014("knx:DPST-203-14",			"power_kw_z"		,"DPT_PowerKW_Z",	 new String[]{}),
    DTP_203_015("knx:DPST-203-15",			"atm_pressure_abs_z"			,"DPT_AtmPressureAbs_Z",	 new String[]{}),
    DTP_203_017("knx:DPST-203-17",			"percent_u16_z"		,"DPT_PercentU16_Z",	 new String[]{}),
    DTP_203_100("knx:DPST-203-100",			"hvac_air_qual_z"			,"DPT_HVACAirQual_Z",	 new String[]{}),
    DTP_203_101("knx:DPST-203-101",			"wind_speed_z dpt_wind_speed"						,"DPT_WindSpeed_Z DPT_WindSpeed",	 new String[]{}),
    DTP_203_102("knx:DPST-203-102",			"sun_intensity_z"			,"DPT_SunIntensity_Z",	 new String[]{}),
    DTP_203_104("knx:DPST-203-104",			"hvac_air_flow_abs_z"			,"DPT_HVACAirFlowAbs_Z",	 new String[]{}),
    DTP_204_001("knx:DPST-204-1",			"rel_signed_value_z"			,"DPT_RelSignedValue_Z",	 new String[]{}),
    DTP_205_002("knx:DPST-205-2",			"delta_time_msec_z"			,"DPT_DeltaTimeMsec_Z",	 new String[]{}),
    DTP_205_003("knx:DPST-205-3",			"delta_time10_msec_z"				,"DPT_DeltaTime10Msec_Z",	 new String[]{}),
    DTP_205_004("knx:DPST-205-4",			"delta_time100_msec_z"				,"DPT_DeltaTime100Msec_Z",	 new String[]{}),
    DTP_205_005("knx:DPST-205-5",			"delta_time_sec_z"			,"DPT_DeltaTimeSec_Z",	 new String[]{}),
    DTP_205_006("knx:DPST-205-6",			"delta_time_min_z"			,"DPT_DeltaTimeMin_Z",	 new String[]{}),
    DTP_205_007("knx:DPST-205-7",			"delta_time_hrs_z"			,"DPT_DeltaTimeHrs_Z",	 new String[]{}),
    DTP_205_017("knx:DPST-205-17",			"percent_v16_z"			,"DPT_Percent_V16_Z",	 new String[]{}),
    DTP_205_100("knx:DPST-205-100",			"temp_hvac_abs_z"			,"DPT_TempHVACAbs_Z",	 new String[]{}),
    DTP_205_101("knx:DPST-205-101",			"temp_hvac_rel_z"			,"DPT_TempHVACRel_Z",	 new String[]{}),
    DTP_205_102("knx:DPST-205-102",			"hvac_air_flow_rel_z"			,"DPT_HVACAirFlowRel_Z",	 new String[]{}),
    DTP_206_100("knx:DPST-206-100",			"hvac_mode_next"		,"DPT_HVACModeNext",	 new String[]{}),
    DTP_206_102("knx:DPST-206-102",			"dhw_mode_next"		,"DPT_DHWModeNext",	 new String[]{}),
    DTP_206_104("knx:DPST-206-104",			"occ_mode_next"		,"DPT_OccModeNext",	 new String[]{}),
    DTP_206_105("knx:DPST-206-105",			"building_mode_next"			,"DPT_BuildingModeNext",	 new String[]{}),
    DTP_207_100("knx:DPST-207-100",			"status_buc"		,"DPT_StatusBUC",	 new String[]{}),
    DTP_207_101("knx:DPST-207-101",			"lock_sign"	,"DPT_LockSign",	 new String[]{}),
    DTP_207_102("knx:DPST-207-102",			"dem_boc"		,"DPT_ValueDemBOC",	 new String[]{}),
    DTP_207_104("knx:DPST-207-104",			"act_pos_dem_abs"		,"DPT_ActPosDemAbs",	 new String[]{}),
    DTP_207_105("knx:DPST-207-105",			"status_act"		,"DPT_StatusAct",	 new String[]{}),
    DTP_207_600("knx:DPST-207-600",			"status_lighting_actuator"					,"DPT_StatusLightingActuator",	 new String[]{}),
    DTP_209_100("knx:DPST-209-100",			"status_hpm"		,"DPT_StatusHPM",	 new String[]{}),
    DTP_209_101("knx:DPST-209-101",			"temp_room_dem_abs"			,"DPT_TempRoomDemAbs",	 new String[]{}),
    DTP_209_102("knx:DPST-209-102",			"status_cpm"		,"DPT_StatusCPM",	 new String[]{}),
    DTP_209_103("knx:DPST-209-103",			"status_wtc"		,"DPT_StatusWTC",	 new String[]{}),
    DTP_210_100("knx:DPST-210-100",			"temp_flow_water_dem_abs"				,"DPT_TempFlowWaterDemAbs",	 new String[]{}),
    DTP_211_100("knx:DPST-211-100",			"energy_dem_water"			,"DPT_EnergyDemWater",	 new String[]{}),
    DTP_212_100("knx:DPST-212-100",			"temp_room_setp_set_shift[3]"					,"DPT_TempRoomSetpSetShift[3]",	 new String[]{}),
    DTP_212_101("knx:DPST-212-101",			"temp_room_setp_set[3]"				,"DPT_TempRoomSetpSet[3]",	 new String[]{}),
    DTP_213_100("knx:DPST-213-100",			"temp_room_setp_set[4]"				,"DPT_TempRoomSetpSet[4]",	 new String[]{}),
    DTP_213_101("knx:DPST-213-101",			"temp_dhw_setp_set[4]"				,"DPT_TempDHWSetpSet[4]",	 new String[]{}),
    DTP_213_102("knx:DPST-213-102",			"temp_room_setp_set_shift[4]"					,"DPT_TempRoomSetpSetShift[4]",	 new String[]{}),
    DTP_214_100("knx:DPST-214-100",			"power_flow_water_dem_hpm"				,"DPT_PowerFlowWaterDemHPM",	 new String[]{}),
    DTP_214_101("knx:DPST-214-101",			"power_flow_water_dem_cpm"				,"DPT_PowerFlowWaterDemCPM",	 new String[]{}),
    DTP_215_100("knx:DPST-215-100",			"status_boc"		,"DPT_StatusBOC",	 new String[]{}),
    DTP_215_101("knx:DPST-215-101",			"status_cc"	,"DPT_StatusCC",	 new String[]{}),
    DTP_216_100("knx:DPST-216-100",			"spec_heat_prod"		,"DPT_SpecHeatProd",	 new String[]{}),
    DTP_217_001("knx:DPST-217-1",			"version"	,"DPT_Version",	 new String[]{}),
    DTP_218_001("knx:DPST-218-1",			"volume_liter_z"			,"DPT_VolumeLiter_Z",	 new String[]{}),
    DTP_218_002("knx:DPST-218-2",			"flow_rate_m3/h_z"			,"DPT_FlowRate_m3/h_Z",	 new String[]{}),
    DTP_219_001("knx:DPST-219-1",			"alarm_info"		,"DPT_AlarmInfo",	 new String[]{}),
    DTP_220_100("knx:DPST-220-100",			"temp_hvac_abs_next"			,"DPT_TempHVACAbsNext",	 new String[]{}),
    DTP_221_001("knx:DPST-221-1",			"ser_num"	,"DPT_SerNum",	 new String[]{}),
    DTP_222_100("knx:DPST-222-100",			"temp_room_setp_set_f16[3]"					,"DPT_TempRoomSetpSetF16[3]",	 new String[]{}),
    DTP_222_101("knx:DPST-222-101",			"temp_room_setp_set_shift_f16[3]"						,"DPT_TempRoomSetpSetShiftF16[3]",	 new String[]{}),
    DTP_223_100("knx:DPST-223-100",			"energy_dem_air"		,"DPT_EnergyDemAir",	 new String[]{}),
    DTP_224_100("knx:DPST-224-100",			"temp_supply _air_setp_set"					,"DPT_TempSupply AirSetpSet",	 new String[]{}),
    DTP_225_001("knx:DPST-225-1",			"scaling_speed"		,"DPT_ScalingSpeed",	 new String[]{}),
    DTP_225_002("knx:DPST-225-2",			"scaling_step_time"				,"DPT_Scaling_Step_Time",	 new String[]{}),
    DTP_225_003("knx:DPST-225-3",			"tariff_next"		,"DPT_TariffNext",	 new String[]{}),
    DTP_229_001("knx:DPST-229-1",			"metering_value"			,"DPT_MeteringValue",	 new String[]{}),
    DTP_230_1000("knx:DPST-230-1000",		"m_bus_address"			,"DPT_MBus_Address",	 new String[]{}),
    DTP_231_001("knx:DPST-231-1",			"locale_ascii"		,"DPT_Locale_ASCII",	 new String[]{}),
    DTP_232_600("knx:DPST-232-600",			"colour_rgb"		,"DPT_Colour_RGB",	 new String[]{}),
    DTP_234_001("knx:DPST-234-1",			"language_code_alpha2_ascii"					,"DPT_LanguageCodeAlpha2_ASCII",	 new String[]{}),
    DTP_234_002("knx:DPST-234-2",			"region_code_alpha2_ascii"					,"DPT_RegionCodeAlpha2_ASCII",	 new String[]{}),
    DTP_235_001("knx:DPST-235-1",			"tariff_active_energy"				,"DPT_Tariff_ActiveEnergy",	 new String[]{}),
    DTP_236_001("knx:DPST-236-1",			"prioritised_mode_control"					,"DPT_Prioritised_Mode_Control",	 new String[]{}),
    DTP_237_600("knx:DPST-237-600",			"dali_control_gear_diagnostic"						,"DPT_DALI_Control_Gear_Diagnostic",	 new String[]{}),
    DTP_238_001("knx:DPST-238-1",			"scene_config"		,"DPT_SceneConfig",	 new String[]{}),
    DTP_238_600("knx:DPST-238-600",			"dali_diagnostics"			,"DPT_DALI_Diagnostics",	 new String[]{}),
    DTP_239_001("knx:DPST-239-1",			"flagged_scaling"			,"DPT_FlaggedScaling",	 new String[]{}),
    DTP_240_800("knx:DPST-240-800",			"combined_position"			,"DPT_CombinedPosition",	 new String[]{}),
    DTP_241_800("knx:DPST-241-800",			"status_a_b"				,"DPT_StatusSAB",		new String[]{});



    public static final String ContractUri = "/knx/Datapoint";

    public static final int GROUP_B1 = 1;
    public static final int GROUP_B2 = 2;
    public static final int GROUP_B1U3 = 3 ;
    public static final int GROUP_A8   = 4 ;
    public static final int GROUP_U8   = 5 ;
    public static final int GROUP_V8 = 6 ;
    public static final int GROUP_U16 =7;
    public static final int GROUP_V16 =8;
    public static final int GROUP_F16 =9;
    public static final int GROUP_N3N5r2N6r2N6 = 10;
    public static final int GROUP_r3N5R4N4r1U7 = 11;
    public static final int GROUP_U32 = 12;
    public static final int GROUP_V32 = 13;
    public static final int GROUP_F32 = 14;
    public static final int GROUP_U4U4U4U4U4U4B4N4 = 15;
    public static final int GROUP_A112  = 16;
    public static final int GROUP_r2U6 = 17;
    public static final int GROUP_B1r1U6 = 18;
    public static final int GROUP_U8r4U4r3U5U3U5r2U6r2U6B16 = 19;
    public static final int GROUP_N8 = 20;
    public static final int GROUP_B8 = 21;
    public static final int GROUP_B16 = 22;
    public static final int GROUP_N2 = 23;
    public static final int GROUP_An  = 24;
    public static final int GROUP_U4U4 = 25;
    public static final int GROUP_r1b1U6 = 26;
    public static final int GROUP_B32 = 27;
    public static final int GROUP_An_2  = 28;
    public static final int GROUP_V64 = 29;
    public static final int GROUP_B24 =  30;
    public static final int GROUP_N3 =   31;
    public static final int GROUP_B1Z8 = 200;
    public static final int GROUP_N8Z8 = 201;
    public static final int GROUP_U8Z8 = 202;
    public static final int GROUP_U16Z8 = 203;
    public static final int GROUP_V8Z8 = 204;
    public static final int GROUP_V16Z8 = 205;
    public static final int GROUP_U16N8 = 206;
    public static final int GROUP_U8B8 = 207;
    //208 does not exists
    public static final int GROUP_V16B8 = 209;
    public static final int GROUP_V16B16 = 210;
    public static final int GROUP_U8N8 = 211;
    public static final int GROUP_V16V16V16 = 212;
    public static final int GROUP_V16V16V16V16 = 213;
    public static final int GROUP_V16U8B8 = 214;
    public static final int GROUP_V16U8B16 = 215;
    public static final int GROUP_U16U8N8B8 = 216;
    public static final int GROUP_U5U5U6 = 217;
    public static final int GROUP_V32Z8 = 218;
    public static final int GROUP_U8N8N8N8B8B8 = 219;
    public static final int GROUP_U16V16 = 220;
    public static final int GROUP_N16U32 = 221;
    public static final int GROUP_F16F16F16 = 222;
    public static final int GROUP_V8N8N8 = 223;
    public static final int GROUP_V16V16N8N8 = 224;
    public static final int GROUP_U16U8 = 225;
    //226-28 do not exists
    public static final int GROUP_V32N8Z8 = 229;
    public static final int GROUP_U16U32U8N8 = 230;
    public static final int GROUP_A8A8A8A8 = 231;
    public static final int GROUP_U8U8U8 = 232;
    //233 does not exists
    public static final int GROUP_A8A8 = 234;
    public static final int GROUP_V32U8B8 = 235;
    public static final int GROUP_B1N3N4 = 236;
    public static final int GROUP_B10U6 = 237;
    public static final int GROUP_B2U6 = 238;
    public static final int GROUP_U8r7B1 = 239;
    public static final int GROUP_U8U8B8 = 240;
    public static final int GROUP_U8U8B16 = 241;


    //Some Aliases
    public static final int GROUP_STRING = GROUP_A112;
    public static final int GROUP_DATETIME = GROUP_U8r4U4r3U5U3U5r2U6r2U6B16;
    public static final int GROUP_VARSTRING = GROUP_An;
    public static final int GROUP_DOUBLE_NIPPLE = GROUP_U4U4;
    public static final int GROUP_UTF8 = GROUP_An_2;


    // ===========================================================
    // Fields
    // ===========================================================

    private final String uri;
    private final String mAttrName;
    private final String mDptName;
    private final String id;

    private final int group;
    private final int dtp;

    private final String[] encodingEnum;


    // ===========================================================
    // Constructors
    // ===========================================================
    Datapoint(String uri, String attrName, String dptName, String[] encodingEnum){
        this.uri = uri;
        this.mAttrName = attrName;
        String[] parts = this.uri.replace("knx:DPST-", "").split("-");
        this.group = Integer.parseInt(parts[0]);
        this.dtp = Integer.parseInt(parts[1]);
        this.id = String.format("%d.%03d", group, dtp);
        this.encodingEnum = encodingEnum;
        this.mDptName = dptName;
    }

    public static Datapoint fromUri(String uri){
        for (Datapoint dp : Datapoint.values()){
            if(dp.uri.equals(uri))
                return dp;
        }
        return null;
    }


    private static Datapoint fromId(String str) {
        for (Datapoint dp : Datapoint.values()){
            if(dp.id.equals(str))
                return dp;
        }
        return null;
    }
    private static Datapoint fromName(String str) {
        for (Datapoint dp : Datapoint.values()){
            if(dp.mDptName.equals(str))
                return dp;
        }
        return null;
    }
    private static Datapoint fromNumbers(int major, int minor) {
        for (Datapoint dp : Datapoint.values()){
            if(dp.group == major && dp.dtp ==minor)
                return dp;
        }
        return null;
    }

    public static Datapoint parse(String str){
        Datapoint result = fromUri(str);
        if(result != null)
            return result;
        result = fromId(str);
        if(result != null)
            return result;
        result = fromName(str);
        if(result != null)
            return result;
        int[] numbers = guessNumbers(str);
        if(numbers != null)
            result = fromNumbers(numbers[0], numbers[1]);
        if(result != null)
            return result;
        throw new RuntimeException("Could not parse Datapoint from '"+str+"'");
    }

    private static int[] guessNumbers(String str) {
        Pattern p = Pattern.compile("^.*?(\\d|\\d{2}|\\d{3})(\\.|-|_)(\\d|\\d{2}|\\d{3}|\\d{4})$");
        final Matcher matcher = p.matcher(str);
        if(matcher.matches()){
            return new int[]{
                    Integer.parseInt(matcher.group(1)),
                    Integer.parseInt(matcher.group(3))
            };
        }
        return null;
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================
    public String objName() {
        return mAttrName;
    }

    public String uri() {
        return uri;
    }

    public int getDtp() { return dtp; }

    public String[] getEncodingEnum() {return encodingEnum; }

    public int getGroup() {return group; }

    public String getId() { return id; }

    public String getDptName() {return mDptName; }

    public String getUri() { return uri; }

    public Contract toContract() {
        return new Contract(new String[]{Datapoint.ContractUri, this.uri});
    }


    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
