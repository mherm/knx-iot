package de.hsrm.knx.knxbrowser.maps;

import android.graphics.Path;

import de.hsrm.knx.knxbrowser.BeaconId;

/**
 * Created by Manuel Hermenau on 25.08.2016.
 */
public interface OutlineProvider {
    public Path getOutline(BeaconId id);
}
