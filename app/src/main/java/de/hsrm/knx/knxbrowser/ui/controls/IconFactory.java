package de.hsrm.knx.knxbrowser.ui.controls;

import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.R;
import obix.Obj;

/**
 * Created by Manuel Hermenau on 20.09.2016.
 */
public class IconFactory {
    private static final int DEFAULT_DRAWABLE = R.drawable.ic_contract_default;
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private final Map<String, Integer> mappings = new HashMap<>();
    {
//        mappings.put(new Uri("knx:DPST-1-1"),            R.drawable.ic_contract_dtp_1_001);
//        mappings.put(new Uri("knx:DPST-1-2"),            R.drawable.ic_contract_dtp_1_002);
//        mappings.put(new Uri("knx:DPST-1-3"),            R.drawable.ic_contract_dtp_1_003);
//        mappings.put(new Uri("knx:DPST-1-4"),            R.drawable.ic_contract_dtp_1_004);
//        mappings.put(new Uri("knx:DPST-1-5"),            R.drawable.ic_contract_dtp_1_005);
//        mappings.put(new Uri("knx:DPST-1-6"),            R.drawable.ic_contract_dtp_1_006);
//        mappings.put(new Uri("knx:DPST-1-7"),            R.drawable.ic_contract_dtp_1_007);
//        mappings.put(new Uri("knx:DPST-1-8"),            R.drawable.ic_contract_dtp_1_008);
//        mappings.put(new Uri("knx:DPST-1-9"),            R.drawable.ic_contract_dtp_1_009);
//        mappings.put(new Uri("knx:DPST-1-10"),           R.drawable.ic_contract_dtp_1_010);
//        mappings.put(new Uri("knx:DPST-1-11"),           R.drawable.ic_contract_dtp_1_011);
//        mappings.put(new Uri("knx:DPST-1-12"),           R.drawable.ic_contract_dtp_1_012);
//        mappings.put(new Uri("knx:DPST-1-13"),           R.drawable.ic_contract_dtp_1_013);
//        mappings.put(new Uri("knx:DPST-1-14"),           R.drawable.ic_contract_dtp_1_014);
//        mappings.put(new Uri("knx:DPST-1-15"),           R.drawable.ic_contract_dtp_1_015);
//        mappings.put(new Uri("knx:DPST-1-16"),           R.drawable.ic_contract_dtp_1_016);
//        mappings.put(new Uri("knx:DPST-1-17"),           R.drawable.ic_contract_dtp_1_017);
//        mappings.put(new Uri("knx:DPST-1-18"),           R.drawable.ic_contract_dtp_1_018);
//        mappings.put(new Uri("knx:DPST-1-19"),           R.drawable.ic_contract_dtp_1_019);
//        mappings.put(new Uri("knx:DPST-1-20"),           R.drawable.ic_contract_dtp_1_020);
//        mappings.put(new Uri("knx:DPST-1-21"),           R.drawable.ic_contract_dtp_1_021);
//        mappings.put(new Uri("knx:DPST-1-22"),           R.drawable.ic_contract_dtp_1_022);
//        mappings.put(new Uri("knx:DPST-1-23"),           R.drawable.ic_contract_dtp_1_023);
        mappings.put("/knx/Installation",       R.drawable.ic_contract_installation);
        mappings.put("knx/View",                R.drawable.ic_contract_view);
        mappings.put("/knx/Device",              R.drawable.ic_contract_device);
        mappings.put("/knx/Datapoint",           R.drawable.ic_contract_datapoint);
        mappings.put("/knx/Functionality",       R.drawable.ic_contract_functionality);
        mappings.put("/knx/BuildingPartRoom",    R.drawable.ic_contract_buildingpart_room);
        mappings.put("/knx/BuildingPartFloor",   R.drawable.ic_contract_buildingpart_floor);
        mappings.put("/knx/BuildingPart",        R.drawable.ic_contract_buildingpart);
        mappings.put("/knx/Manufacturer",        R.drawable.ic_contract_manufacturer);



        mappings.put("obix:ref",                R.drawable.ic_contract_ref);
    }

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    public int getIconResource(Obj obj){
        if(obj == null || !"ref".equals(obj.getElement())|| obj.getIs() == null ||obj.getIs().size() == 0)
            return R.drawable.ic_contract_none;
        String isPrimary = obj.getIs().primary().get();
        Integer drawable = mappings.get(isPrimary);
        if( drawable != null)
            return drawable;
        return DEFAULT_DRAWABLE;
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
