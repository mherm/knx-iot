package de.hsrm.knx.knxbrowser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manuel Hermenau on 22.08.2016.
 */
public class MapRoomIdTranslator implements RoomIdTranslator {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final Map<BeaconId, String> mIdToStringMap;
    private final Map<String, BeaconId> mStringToIdMap;

    // ===========================================================
    // Constructors
    // ===========================================================

    public MapRoomIdTranslator(Map<BeaconId, String> mapping){
        mIdToStringMap = mapping;
        mStringToIdMap = new HashMap<>(mapping.size());
        for(Map.Entry<BeaconId, String> e : mapping.entrySet()){
            if(mStringToIdMap.put(e.getValue(), e.getKey()) != null)
                throw new IllegalArgumentException("Mapping values must be bidirectional. Values is not unique: "+e.getValue());
        }
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public String getDomainId(BeaconId beaconMajor) {
        return mIdToStringMap.get(beaconMajor);
    }

    @Override
    public BeaconId getBeaconId(String domainId) {
        return mStringToIdMap.get(domainId);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
