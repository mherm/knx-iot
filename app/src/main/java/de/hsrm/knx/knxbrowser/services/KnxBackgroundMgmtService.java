package de.hsrm.knx.knxbrowser.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.internal.Flags;

import org.antlr.v4.runtime.misc.MultiMap;
import org.jdeferred.DeferredManager;
import org.jdeferred.Promise;
import org.jdeferred.android.AndroidDeferredManager;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.hsrm.knx.knxbrowser.BeaconGroup;
import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.EnvironmentManager;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.Utils;
import de.hsrm.knx.knxbrowser.script.ast.TimeTrigger;
import de.hsrm.knx.knxbrowser.ui.activities.RoomChangedListener;
import de.hsrm.knx.knxbrowser.ui.activities.WebserviceActivity;
import de.hsrm.knx.knxbrowser.db.DbHelper;
import de.hsrm.knx.knxbrowser.db.Entity;
import de.hsrm.knx.knxbrowser.db.EntityChangeReceiver;
import de.hsrm.knx.knxbrowser.db.EntityListener;
import de.hsrm.knx.knxbrowser.db.KnxScriptConfiguration;
import de.hsrm.knx.knxbrowser.script.ExecutionResult;
import de.hsrm.knx.knxbrowser.script.ast.ActivationFilter;
import de.hsrm.knx.knxbrowser.script.ast.KnxScript;
import de.hsrm.knx.knxbrowser.script.ast.KnxScriptWriter;
import de.hsrm.knx.knxbrowser.script.ast.RoomTrigger;
import de.hsrm.knx.knxbrowser.script.ast.Trigger;

import static java.lang.String.copyValueOf;
import static java.lang.String.format;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class KnxBackgroundMgmtService extends IntentService implements RoomChangedListener, EntityListener {

    // ===========================================================
    // Constants
    // ===========================================================

    public static final String INTENT_START_SERVICE= "de.hsrm.knx.knxbrowser.action.START_SCRIPT_SERVICE";
    public static final String INTENT_STOP_SERVICE= "de.hsrm.knx.knxbrowser.action.STOP_SCRIPT_SERVICE";

    public static final String INTENT_START_LOCATION_SERVICE = "de.hsrm.knx.knxbrowser.action.START_LOCATION_SERVICE";
    public static final String INTENT_STOP_LOCATION_SERVICE = "de.hsrm.knx.knxbrowser.action.STOP_LOCATION_SERVICE";

    public static final String INTENT_ROOM_CHANGED = "de.hsrm.knx.knxbrowser.action.ROOM_CHANGED";
    public static final String INTENT_CURRENT_ROOM = "de.hsrm.knx.knxbrowser.action.CURRENT_ROOM";
    public static final String INTENT_REQUEST_CURRENT_ROOM = "de.hsrm.knx.knxbrowser.action.REQUEST_CURRENT_ROOM";
    public static final String EXTRA_ROOM_BEACON_ID = "de.hsrm.knx.knxbrowser.extra.ROOM_BEACON_ID";

    private static final String INTENT_TIME_TRIGGER =  "de.hsrm.knx.knxbrowser.action.TIME_TRIGGER";
    private static final String EXTRA_TRIGGER_TIME =  "de.hsrm.knx.knxbrowser.extra.TRIGGER_TIME";

    private static final String TAG = KnxBackgroundMgmtService.class.getName();
    private static final int ONGOING_NOTIFICATION_ID = 0xDECAFBAD;

    // ===========================================================
    // Fields
    // ===========================================================

    private Boolean mIsRunning = null;

    private MultiMap<Trigger, KnxScriptConfiguration> mScriptsByTriggers = new MultiMap<>();

    private BeaconManager mBeaconManager;
    private Region mRegion;

    private BeaconId mCurrentRoom;

    private HashMap<BeaconId, BeaconGroup> mBeaconGroupSamples = new HashMap<>();
    private Integer mSampleSize =15;
    private Integer mCollectedSamples=0;

    private ExecutorService mExecutors = Executors.newCachedThreadPool();
    private boolean mIsRanging = false;
    private DbHelper mDb;
    private DeferredManager mDm = new AndroidDeferredManager();
    private EnvironmentManager mEnvironmentManager;

    private EntityChangeReceiver mEntityChangeReceiver;

    private final Handler handler = new Handler();

    // ===========================================================
    // Constructors
    // ===========================================================

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public KnxBackgroundMgmtService() {
        super("KnxBackgroundService");
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onHandleIntent(Intent intent) {
        handleIntent(intent);

        if(mIsRunning.booleanValue() == true)
            //Make sure this Thread does not terminate
            Looper.loop();
    }

    @Override
    public void onCreate(){
        super.onCreate();
        this.mEnvironmentManager = EnvironmentManager.getInstance(this);
        mDb = DbHelper.getInstance(this);
        fixStaleExecutions().<Void>then((v)->loadScripts()).then((v)->{
            mEntityChangeReceiver = new EntityChangeReceiver(Utils.<Class<? extends Entity>>lst(KnxScriptConfiguration.class, ExecutionResult.class), this);
            mEntityChangeReceiver.register(this);
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()");

        if(mBeaconManager != null && mIsRanging) {
            stopRanging();
        }
    }

    private synchronized void stopRanging() {
        mBeaconManager.stopRanging(mRegion);
        mBeaconManager.disconnect();
        mIsRanging = false;
    }


    // ===========================================================
    // Methods
    // ===========================================================
    private void handleIntent(Intent intent){
        final String action = intent.getAction();
        Log.d(TAG, "Intent received: "+action);
        initService();
        if(INTENT_STOP_SERVICE.equals(action))
            mIsRunning=false;
        else if(INTENT_START_LOCATION_SERVICE.equals(action))
            startLocationService();
        else if(INTENT_STOP_LOCATION_SERVICE.equals(action))
            stopRanging();
        else if(INTENT_REQUEST_CURRENT_ROOM.equals(action))
            broadcastCurrentRoomInfo();
        else if(INTENT_TIME_TRIGGER.equals(action)) {
            final int triggerTime = intent.getIntExtra(EXTRA_TRIGGER_TIME, -1);
            if(triggerTime>=0) {
                TimeTrigger timeTrigger = new TimeTrigger();
                timeTrigger.setTimeWithoutDate(triggerTime);
                callScripts(timeTrigger);
            }
        }
        else if(DbHelper.INTENT_SCRIPT_ENABLED.equals(action))
            handleScriptEnabled(intent);
        else if(DbHelper.INTENT_SCRIPT_DISABLED.equals(action))
            handleScriptDisabled(intent);

        if(!mEnvironmentManager.getActiveEnvironment().testConnection(this))
            mIsRunning=false;
        //TODO WakefulReceiver
    }

    private void handleScriptEnabled(Intent intent) {
        final int id = intent.getIntExtra(DbHelper.EXTRA_ENTITY_ID, -1);
        enableScript(id);
        reloadScript(id);
    }

    private void enableScript(int id) {
        findScript(id).<KnxScriptConfiguration>then(s->s.setEnabled(true));    }

    private void handleScriptDisabled(Intent intent) {
        final int id = intent.getIntExtra(DbHelper.EXTRA_ENTITY_ID, -1);
        disableScript(id);
    }

    private void disableScript(int id) {
        findScript(id).<KnxScriptConfiguration>then(s->s.setEnabled(false));
    }

    private Promise<KnxScriptConfiguration, Throwable, Void> findScript(int id) {
        //Search the map. TODO: keep a reference by id somewhere
        Log.d(TAG, "findScript("+id+")");
        for(List<KnxScriptConfiguration> cl : mScriptsByTriggers.values()){
            for(KnxScriptConfiguration c : cl)
                if(c.getId() == id) {
                    return mDm.when(()->c);
                }
        }
        Log.d(TAG, "Could not findScript "+id+". Loading it from database...");
        //Fallback: load from db
        return mDm.when(()->mDb.getScript(id)).<KnxScriptConfiguration>then((s)->registerScript(s));
    }


    private synchronized void initService(){
        if(mIsRunning != null){
            Log.d(TAG, "Service instance already exists");
            return;
        }else{
            Log.i(TAG, "Creating KnxBackgroundMgmtService");
        }
        initBeaconManager();
        Notification notification = buildNotification();
        startForeground(ONGOING_NOTIFICATION_ID, notification);
        mIsRunning = true;
    }

    private synchronized void startLocationService(){
        if(mIsRanging){
            Log.d(TAG, "Service is already ranging for beacons");
            broadcastCurrentRoomInfo();
            return;
        }else{
            Log.i(TAG, "Start ranging for beacons...");
        }
        startRanging();
    }

    private void initBeaconManager() {
        Flags.FORCE_OLD_SCANNING_API.set(true);
        mBeaconManager = new BeaconManager(this);
        mRegion = new Region("ranged region", UUID.fromString("CCDD8BDA-F856-D761-D9A3-AA6D3B5CA7B2"), null, null);
        //mRegion = new Region("ranged region", null, null, null);
        Log.d(TAG, "Creating BeaconManager for Region "+ mRegion.toString());
        mBeaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> list) {
                handleBeaconList(list);
            }
        });
    }

    private synchronized void handleBeaconList(List<Beacon> list) {
        //Log.d(TAG, "Beacons found: "+list);
        List<BeaconGroup> beaconGroups = groupBeacons(list); //modifies mBeaconGroups

        //Collect up to mSampleSize samples to avoid missing some Beacons because of timing problems
        if(++mCollectedSamples == mSampleSize) {
            if(!beaconGroups.isEmpty()) {
                //Sort ascending by average signal strength
                Collections.sort(beaconGroups);
                //Log.d(TAG, "Majors grouped by strength: " + beaconGroups);

                //Last item in list is the strongest
                BeaconId currentRoom = beaconGroups.get(beaconGroups.size() - 1).getBeaconId();
                //Log.d(TAG, "Current room: " + currentRoom);
                handleCurrentRoom(currentRoom);
            }else{
                handleCurrentRoom(null);
            }
            mCollectedSamples = 0;
            mBeaconGroupSamples.clear();
        }


        final long t = System.currentTimeMillis();
        for(BeaconGroup beacon : beaconGroups){
            Data d = new Data();
            d.strength = beacon.getAverageStrength();
            d.id = beacon.getBeaconId().represent();
            d.t=t;
            //readings.add( d);
        }
        //Log.i("MY READINGS", readings.toString());

    }
    List<Data> readings = new LinkedList<Data>();
    class Data {long t; int rssi; double distance; double strength; String id;
        public String toString(){return String.format("%d\t%f\t%s\n",t,strength,id);}}
    /**
     * Groups the beacons by their major id.
     * @param list
     * @return an unordered ArrayList containing all BeaconGroups
     */
    private List<BeaconGroup> groupBeacons(List<Beacon> list) {
        for(Beacon b : list){
            BeaconId bid = new BeaconId(b);
            BeaconGroup grp = mBeaconGroupSamples.get(bid);
            if(grp == null){
                grp = BeaconGroup.create(b);
                mBeaconGroupSamples.put(bid, grp);
            }else{
                grp.add(b);
            }
        }
        return new ArrayList<>(mBeaconGroupSamples.values());
    }

    /**
     * Called when an update on the current room arrived.
     * Schedules a broadcast if the room changed since the last update.
     * @param detectedRoom
     */
    private synchronized void handleCurrentRoom(BeaconId detectedRoom) {
        boolean doUpdate = false;
        if (detectedRoom == null){
            if (mCurrentRoom != null)
                doUpdate = true;
        }else{
            if(!detectedRoom.equals(mCurrentRoom)){
                doUpdate = true;
            }
        }
        if(doUpdate) {
            Log.i(TAG, String.format("Current room changed! Old Room: %s - New room: %s", mCurrentRoom, detectedRoom));
            broadcastRoomChanged(detectedRoom);
        }
        mCurrentRoom = detectedRoom;
    }

    private void broadcastCurrentRoomInfo() {
        broadcastRoom(INTENT_CURRENT_ROOM, mCurrentRoom);
    }


    private void broadcastRoomChanged(BeaconId newRoom) {
        broadcastRoom(INTENT_ROOM_CHANGED, newRoom);
        onRoomChanged(newRoom, mCurrentRoom);
    }

    private void broadcastRoom(String intentStr, BeaconId room) {
        if(room == null)
            room = BeaconId.NONE;
        Intent intent = new Intent(intentStr);
        intent.putExtra(EXTRA_ROOM_BEACON_ID, room);
        Log.d(TAG, "Broadcasting: "+intent.getAction() + ": "+ room);
        getApplicationContext().sendBroadcast(intent);
    }

    private void startRanging() {
        //TODO: Move SystemRequirementsCheck to Activity
        //SystemRequirementsChecker.checkWithDefaultDialogs(this);
        mBeaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                mBeaconManager.setForegroundScanPeriod(200,0);
                mBeaconManager.startRanging(mRegion);
                mIsRanging = true;
            }
        });
    }

    private Notification buildNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Intent notificationIntent = new Intent(this, WebserviceActivity.class);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, ONGOING_NOTIFICATION_ID, notificationIntent, 0);
        builder.setContentIntent(pendingNotificationIntent);
        builder.setSmallIcon(R.drawable.ic_gps_fixed_white_24dp);
        builder.setContentTitle("KNX Background Service");
        builder.setContentText("Scripting Engine is active.");
        builder.setAutoCancel(false);

        Intent killServiceIntent = new Intent(this, KnxBackgroundMgmtService.class);
        killServiceIntent.setAction(INTENT_STOP_SERVICE);
        PendingIntent pendingKillServiceIntent = PendingIntent.getService(this, 0, killServiceIntent, 0);

        builder.addAction(R.drawable.ic_gps_off_black_24dp, "Disable", pendingKillServiceIntent);
        return builder.build();
    }

    @Override
    public void onRoomChanged(BeaconId newRoom, BeaconId oldRoom) {
        Trigger onLeaveTrigger = new RoomTrigger.LeaveRoomTrigger(oldRoom);
        Trigger onEnterTrigger = new RoomTrigger.EnterRoomTrigger(newRoom);

        callScripts(onLeaveTrigger);
        callScripts(onEnterTrigger);
    }

    private void callScripts(Trigger trigger) {
        Log.d(TAG, "Calling Scripts for "+trigger.toString());
        final List<KnxScriptConfiguration> knxScriptConfigurations = mScriptsByTriggers.get(trigger);
        if(knxScriptConfigurations == null || knxScriptConfigurations.isEmpty()) {
            Log.d(TAG, "No Scripts for "+trigger.toString());
            Log.d(TAG, "Available scripts: "+mScriptsByTriggers);
            return;
        }
        for(KnxScriptConfiguration ksc : knxScriptConfigurations){
            if(!ksc.isEnabled()) {
                Log.d(TAG, ksc.getTitle()+" is disabled");
                continue;
            }
            Log.d(TAG, "Executing "+ksc.getTitle()+"...");
            mExecutors.execute(()-> {
                if(isAlreadyRunning(ksc))
                    return;
                if(!checkActive(ksc.getKnxScript()))
                    return;
                final KnxScript knxScript = ksc.getKnxScript();
                Log.i(TAG, "Running script '"+knxScript.getTitle()+"'...");
                notifyScriptRun(ksc);
                ExecutionResult dummyResult = new ExecutionResult();
                dummyResult.setScriptId(ksc.getId());
                dummyResult.setStartTime(new DateTime());
                dummyResult.setTrigger(KnxScriptWriter.writeOut(trigger));
                DbHelper.getInstance(KnxBackgroundMgmtService.this).saveExecutionResult(dummyResult);
                ExecutionResult exRes = knxScript.run(KnxBackgroundMgmtService.this);
                dummyResult.setEndTime(exRes.getEndTime());
                dummyResult.setSuccess(exRes.isSuccess());
                dummyResult.setLog(exRes.getLog());
                Log.i(TAG, format("Finished script '%s' %s", knxScript.getTitle(), exRes.isSuccess()?"sucessfully":"with errors"));
                DbHelper.getInstance(KnxBackgroundMgmtService.this).saveExecutionResult(dummyResult);
                notifyScriptFinish(ksc, exRes);
            });
        }

    }

    private boolean isAlreadyRunning(KnxScriptConfiguration ksc) {
        final ExecutionResult latestExecutionResult = mDb.getLatestExecutionResultForScript(ksc.getId());
        if(latestExecutionResult == null)
            return false;
        boolean result = latestExecutionResult.getEndTime() == null;
        if(result) {
//            final int secondsSinceStart =
//                    Seconds.secondsBetween(latestExecutionResult.getStartTime(), new DateTime()).getSeconds();
//            if(secondsSinceStart < 90) //TODO Global Setting MAX_EXECUTION_TIME
//                result = false;
//            else
                Log.w(TAG, "Script is already running. Won't start again: " + ksc.getTitle());
        }
        return result;
    }

    private void notifyScriptFinish(KnxScriptConfiguration ksc, ExecutionResult res) {
        //TODO Script-Spezifische Notifications?
        String text = format("Finished script '%s' %s", ksc.getTitle(), res.isSuccess()?"sucessfully":"with errors");
        handler.post(()->Toast.makeText(this, text, Toast.LENGTH_LONG).show());
    }

    private void notifyScriptRun(KnxScriptConfiguration ksc) {
        //TODO Script-Spezifische Notifications?
        handler.post(()->Toast.makeText(this, "Started script '"+ksc.getTitle()+"'", Toast.LENGTH_LONG).show());
    }

    private boolean checkActive(KnxScript script) {
        for(ActivationFilter activator : script.getActivators()){
            if(!activator.matches(this)) {
                Log.v(TAG,
                        String.format("Activator %s does not match for script '%s'"
                                , activator.getClass().getSimpleName(), script.getTitle()));
                return false;
            }
        }
        Log.v(TAG,
                String.format("All activators match for script '%s'", script.getTitle()));
        return true;
    }

    @Override
    public void onEntityDeleted(Class<? extends Entity> type, int id) {
        if(type == KnxScriptConfiguration.class) {
            deregisterScript(id);
        }
    }

    @Override
    public void onEntityChanged(Class<? extends Entity> type, int id) {
        if(type == KnxScriptConfiguration.class) {
            reloadScript(id);
        }
    }

    @Override
    public void onEntityCreated(Class<? extends Entity> type, int id) {
        if(type == KnxScriptConfiguration.class) {
            registerScript(id);
        }
    }

    private void reloadScript(int id) {
        deregisterScript(id);
        registerScript(id);
    }

    private void reloadScriptForExecutionResult(int id) {
        mDm.when(()->mDb.getExecutionResult(id)).then((er)->{
            reloadScript(er.getScriptId());
        });
    }

    private void deregisterScript(int id) {
        mDm
                .when(()->mDb.getScript(id))
                .then((script)->{
                    deregisterScript(script);
                });
    }

    private void deregisterScript(KnxScriptConfiguration script) {
        for(List<KnxScriptConfiguration> lst : mScriptsByTriggers.values()){
            final Iterator<KnxScriptConfiguration> it = lst.iterator();
            while(it.hasNext()){
                KnxScriptConfiguration conf = it.next();
                if(conf.getId() == script.getId()) {
                    it.remove();
                }
            }
        }
    }

    private void registerScript(int id) {
        mDm
                .when(()->mDb.getScript(id))
                .then((script)->{
                    registerScript(script);
                }).fail((ex)->{throw new RuntimeException(ex);});
    }

    private void registerScript(KnxScriptConfiguration script) {
        for(Trigger t : script.getKnxScript().getRule().getTriggers()) {
            mScriptsByTriggers.map(t, script);
            if(t instanceof TimeTrigger){
                scheduleAlarmTrigger((TimeTrigger)t);
            }
        }
    }

    private void scheduleAlarmTrigger(TimeTrigger t) {
        DateTime dt = Utils.nextDateTime(((TimeTrigger) t).getTimeWithoutDate());
        int time = t.getTimeWithoutDate();
        PendingIntent pendingIntent = createAlarmPendingIntent(time);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, dt.getMillis(), 1000*60*60*24, pendingIntent);
    }

    private PendingIntent createAlarmPendingIntent(int time) {
        PendingIntent pendingIntent;
        Intent intent = new Intent(this, this.getClass());
        intent.putExtra(EXTRA_TRIGGER_TIME, time);
        intent.setAction(INTENT_TIME_TRIGGER);
        pendingIntent = PendingIntent.getService(
                this.getApplicationContext(), 234324243, intent, 0);
        return pendingIntent;
    }


    private Promise<Void, Throwable, Void> fixStaleExecutions() {
       return mDm.when(()-> mDb.closeStaleExecutions());
    }
    private Promise<List<KnxScriptConfiguration>, Throwable, Void> loadScripts() {
        Log.d(TAG, "loadScripts()");
        return mDm
                .when(()->mDb.getScripts())
                .then((scripts)-> {
                    mScriptsByTriggers.clear();
                    for (KnxScriptConfiguration scr : scripts) {
                        registerScript(scr);
                    }
                    Log.d(TAG, "Scripts loaded. "+mScriptsByTriggers);
                })
                .fail((ex)->{throw new RuntimeException(ex);});
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
