package de.hsrm.knx.knxbrowser.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.services.KnxBackgroundMgmtService;

/**
 * Created by Manuel Hermenau on 25.09.2016.
 */
public class LocationServiceConnector {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = LocationServiceConnector.class.getName();

    // ===========================================================
    // Fields
    // ===========================================================

    private static final Map<Context, LocationServiceConnector> instances = new HashMap<>();

    //Broadcast Receiver
    protected Context mContext;
    protected BeaconId mCurrentRoom;
    protected BroadcastReceiver mBroadcastReceiver;
    protected List<RoomChangedListener> mListeners = new LinkedList<>();

    // ===========================================================
    // Constructors
    // ===========================================================

    public LocationServiceConnector(Context context){
        this(context, null);
    }

    public LocationServiceConnector(Context context, Bundle savedInstaceState){
        mContext = context;
        if(savedInstaceState == null)
            mCurrentRoom = BeaconId.NONE;
        else{
            mCurrentRoom = savedInstaceState.getParcelable("mCurrentRoom");
        }
        startLocationService();
    }

    public static LocationServiceConnector getInstance(Context context, Bundle initWith){
        LocationServiceConnector result = instances.get(context);
        if(result == null){
            result= new LocationServiceConnector(context, initWith);
            instances.put(context, result);
        }
        return result;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    // ===========================================================
    // Methods
    // ===========================================================

    public void saveState(Bundle outState){
        outState.putParcelable("mCurrentRoom", mCurrentRoom);
    }

    public void stopListening (){
        //TODO STATE
        //mContext.unregisterReceiver(mBroadcastReceiver);
    }
    public void startListening (){
        //TODO STATE
        if(mBroadcastReceiver == null) {
            mBroadcastReceiver = new ActionReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(KnxBackgroundMgmtService.INTENT_ROOM_CHANGED);
            mContext.registerReceiver(mBroadcastReceiver, filter);
        }
    }

    public void disconnect(){
        if(mContext != null)
            mContext.unregisterReceiver(mBroadcastReceiver);
    }

    protected void startLocationService() {
        Intent intent = new Intent(mContext, KnxBackgroundMgmtService.class);
        intent.setAction(KnxBackgroundMgmtService.INTENT_START_LOCATION_SERVICE);
        mContext.startService(intent);
    }

    public void setCurrentRoom(BeaconId newRoom) {
        mCurrentRoom = newRoom;
    }

    public void requestCurrentRoom(RoomInfoCallback callback) {
        if(mCurrentRoom != null && mCurrentRoom != BeaconId.NONE){
            callback.onRoomInfoReceived(mCurrentRoom);
        }else{
            //Register receiver
            CurrentRoomReceiver tmpBroadcastReceiver = new CurrentRoomReceiver(callback);
            IntentFilter filter = new IntentFilter();
            filter.addAction(KnxBackgroundMgmtService.INTENT_CURRENT_ROOM);
            mContext.registerReceiver(tmpBroadcastReceiver, filter);

            //Send request to service
            Intent intent = new Intent(mContext, KnxBackgroundMgmtService.class);
            intent.setAction(KnxBackgroundMgmtService.INTENT_REQUEST_CURRENT_ROOM);
            mContext.startService(intent);
        }
    }

    private void notifyListeners(BeaconId newLocation){
        for(RoomChangedListener l : mListeners) {
            l.onRoomChanged(newLocation, mCurrentRoom);
        }
    }

    public void addRoomChangedListener(RoomChangedListener listener) {
        mListeners.add(listener);
    }

    public boolean removeRoomChangedListener(RoomChangedListener listener) {
       return mListeners.remove(listener);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    protected class ActionReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent == null)
                return;
            if(intent.getAction().equals(KnxBackgroundMgmtService.INTENT_ROOM_CHANGED)){
                BeaconId unparceled = intent.getParcelableExtra(KnxBackgroundMgmtService.EXTRA_ROOM_BEACON_ID);
                final BeaconId roomNo = unparceled != null ? unparceled : BeaconId.NONE;
                Log.d(TAG, "Broadcast Received! "+intent.getAction()+ " : "+unparceled);
                notifyListeners(roomNo);
                setCurrentRoom(roomNo);
            }
        }
    }
    protected class CurrentRoomReceiver extends BroadcastReceiver{
        private final RoomInfoCallback callback;
        public  CurrentRoomReceiver(RoomInfoCallback callback){
            this.callback = callback;
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent == null)
                return;
            if(intent.getAction().equals(KnxBackgroundMgmtService.INTENT_CURRENT_ROOM)){
                mContext.unregisterReceiver(this);
                BeaconId unparceled = intent.getParcelableExtra(KnxBackgroundMgmtService.EXTRA_ROOM_BEACON_ID);
                final BeaconId roomNo = unparceled != null ? unparceled : BeaconId.NONE;
                Log.d(TAG, "Broadcast Received! "+intent.getAction()+ " : "+unparceled);
                callback.onRoomInfoReceived(roomNo);
                setCurrentRoom(roomNo);
            }
        }
    }
}
