package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 04.08.2016.
 */
import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.Environment;
import de.hsrm.knx.knxbrowser.util.URIUtil;

public class EnvironmentContract extends TableContract<Environment>{
    public static final String TABLE_NAME = "environments";

    private static final EnvironmentContract instance = new EnvironmentContract();

    private EnvironmentContract(){};

    /*Below is a subclass that defines the column names*/
    public static abstract class EnvEntry implements BaseColumns {
        public static final Map<String, ColumnContract> COLUMNS = new HashMap<>();

        //Column Name Constants
        public static final String NAME                 = "name";
        public static final String SSID                 = "ssid";
        public static final String HOST                 = "host";
        public static final String ANDROID_NETWORK_ID   = "android_network_id";


        //Map creation
        static {
            contract(COLUMNS, NAME,                 "TEXT",      "UNIQUE");
            contract(COLUMNS, HOST,                 "TEXT",      "NOT NULL DEFAULT 'knxserver'");
            contract(COLUMNS, SSID,                 "TEXT",      "DEFAULT NULL");
            contract(COLUMNS, ANDROID_NETWORK_ID,   "INT",      "DEFAULT NULL");
        }
    }

    public static abstract class EnvOperations extends Operations {
        public static final String CREATE_TABLE =
                createTable(TABLE_NAME, EnvEntry.COLUMNS);
        public static final String DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public Environment deserialize(Cursor c){
        int id          = c.getInt(c.getColumnIndexOrThrow(EnvEntry._ID));
        String name     = c.getString(c.getColumnIndexOrThrow(EnvEntry.NAME));
        String ssid     = c.getString(c.getColumnIndexOrThrow(EnvEntry.SSID));
        String host     = c.getString(c.getColumnIndexOrThrow(EnvEntry.HOST));
        final int nIdIndex = c.getColumnIndexOrThrow(EnvEntry.ANDROID_NETWORK_ID);
        Integer nId      = c.isNull(nIdIndex) ? null : c.getInt(nIdIndex);
        URL url = URIUtil.makeURL(host);

        Environment result = new Environment();
        result.setId(id);
        result.setName(name);
        result.setHost(url);
        result.setWifiSsid(ssid);
        result.setNetworkId(nId);
        return result;
    }

    @Override
    public String tableName() {
        return TABLE_NAME;
    }

    public static TableContract<Environment> getInstance() {
        return instance;
    }

    public ContentValues serialize(Environment e){
        ContentValues cv = new ContentValues();
        if(e.getId() >= 0)
            cv.put(EnvEntry._ID, e.getId());
        cv.put(EnvEntry.NAME, e.getName());
        cv.put(EnvEntry.HOST, e.getHost().toString());
        cv.put(EnvEntry.SSID, e.getWifiSsid());
        cv.put(EnvEntry.ANDROID_NETWORK_ID, e.getNetworkId());
        return cv;
    }

}
