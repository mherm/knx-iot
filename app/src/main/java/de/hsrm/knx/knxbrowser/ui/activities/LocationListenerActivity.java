package de.hsrm.knx.knxbrowser.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.MapRoomIdTranslator;
import de.hsrm.knx.knxbrowser.RoomIdTranslator;

public abstract class LocationListenerActivity extends AppCompatActivity implements RoomChangedListener {

    // ===========================================================
    // Constants
    // ===========================================================
    protected static String TAG = LocationListenerActivity.class.getName();


    // ===========================================================
    // Fields
    // ===========================================================

    //Broadcaast Receiver
    protected BeaconId mCurrentRoom = BeaconId.NONE;
    protected LocationServiceConnector mLocationService;
    protected final RoomIdTranslator mRoomIdTranslator = new MapRoomIdTranslator(createRoomMappings());


    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationService = new LocationServiceConnector(this, savedInstanceState);
        mLocationService.addRoomChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocationService.startListening();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationService.stopListening();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("mCurrentRoom", mCurrentRoom);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mLocationService.disconnect();
        super.onDestroy();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        BeaconId currentRoom = savedInstanceState.getParcelable("mCurrentRoom");
        setCurrentRoom(currentRoom);
    }


    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * Only for use with my testing devices
     * @return
     */
    private Map<BeaconId,String> createRoomMappings() {
        Map<BeaconId, String> mapping = new HashMap<>();
        mapping.put(BeaconId.NONE, "not on the premises");
        mapping.put(new BeaconId(null,1,1), "room 035");
        mapping.put(new BeaconId(null,2,2), "the corridor");
        mapping.put(new BeaconId(null,3,3), "room 037");

//        mapping.put(3031, "the corridor");
//        mapping.put(12544, "the kitchen");
//        mapping.put(17948, "your office");
        return mapping;
    }

    protected String translateRoomId(BeaconId id){
       return mRoomIdTranslator.getDomainId(id);
    }


    private void setCurrentRoom(BeaconId majorId) {
        mCurrentRoom = majorId;
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
