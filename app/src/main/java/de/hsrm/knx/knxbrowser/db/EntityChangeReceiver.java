package de.hsrm.knx.knxbrowser.db;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.ArrayList;
import java.util.List;

import de.hsrm.knx.knxbrowser.Utils;

/**
 * Created by Manuel Hermenau on 12.10.2016.
 */
public class EntityChangeReceiver extends BroadcastReceiver{

    private final List<Class<? extends Entity>> mTypes;
    private final List<String> mClassNames;
    private final EntityListener mListener;

    public EntityChangeReceiver(Class<? extends Entity> type, EntityListener entityListener){
       this(Utils.lst(type), entityListener);
    }

    public EntityChangeReceiver(List<Class<? extends Entity>> types, EntityListener entityListener){
        mTypes = types;
        mClassNames = new ArrayList<>(types.size());
        for(Class c : types)
            mClassNames.add(c.getName());
        mListener = entityListener;
    }

    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        String entityClass = intent.getStringExtra(DbHelper.EXTRA_ENTITY_CLASS);
        int entityId = intent.getIntExtra(DbHelper.EXTRA_ENTITY_ID, -1);
        if(entityClass == null)
            return;
        int idx = mClassNames.indexOf(entityClass);
        if(idx == -1)
            return;
        if(entityId == -1)
            return;

        if(action.equals(DbHelper.INTENT_ENTITY_CHANGED))
            mListener.onEntityChanged(mTypes.get(idx), entityId);
        else if(action.equals(DbHelper.INTENT_ENTITY_CREATED))
            mListener.onEntityCreated(mTypes.get(idx), entityId);
        else if(action.equals(DbHelper.INTENT_ENTITY_DELETED))
            mListener.onEntityDeleted(mTypes.get(idx), entityId);
    }

    /**
     * Registers this receiver to the given context
     * @param context
     */
    public void register(Context context){
        IntentFilter filter = new IntentFilter();
        filter.addAction(DbHelper.INTENT_ENTITY_CHANGED);
        filter.addAction(DbHelper.INTENT_ENTITY_CREATED);
        filter.addAction(DbHelper.INTENT_ENTITY_DELETED);
        context.registerReceiver(this, filter);
    }

    public void unregister(Context ctx){
        ctx.unregisterReceiver(this);
    }
}
