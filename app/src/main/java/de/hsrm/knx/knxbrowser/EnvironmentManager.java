package de.hsrm.knx.knxbrowser;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import org.jdeferred.DeferredManager;
import org.jdeferred.Promise;
import org.jdeferred.android.AndroidDeferredManager;
import org.jdeferred.android.AndroidDoneCallback;
import org.jdeferred.android.AndroidExecutionScope;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import de.hsrm.knx.knxbrowser.db.DbHelper;
import de.hsrm.knx.knxbrowser.db.Entity;
import de.hsrm.knx.knxbrowser.db.EntityChangeReceiver;
import de.hsrm.knx.knxbrowser.db.EntityListener;
import de.hsrm.knx.knxbrowser.services.WifiChangeListener;
import de.hsrm.knx.knxbrowser.services.WifiChangeReceiver;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class EnvironmentManager implements SharedPreferences.OnSharedPreferenceChangeListener, EntityListener,WifiChangeListener {
    private static final String TAG = EnvironmentManager.class.getName();
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private static EnvironmentManager mInstance;
    private Map<Integer, Environment> mProfiles = new HashMap<>();

    private Environment mActiveEnvironment;
    private Context mContext;
    private DbHelper mDb;

    private DeferredManager mDm = new AndroidDeferredManager();
    private CountDownLatch mInitialized = new CountDownLatch(1);

    // ===========================================================
    // Constructors
    // ===========================================================

    private EnvironmentManager(Context context) {
        mContext = context;
        mDb = DbHelper.getInstance(mContext);
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        sPrefs.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(sPrefs, context.getString(R.string.preference_activecontext_key));
        registerReceivers();
        loadEnvironments().then(new AndroidDoneCallback<Void>() {
            @Override
            public void onDone(Void v) {
                mActiveEnvironment = mProfiles.get(loadActiveEnvironmentId());
                findActiveEnvironment();
                mInitialized.countDown();
            }
            @Override
            public AndroidExecutionScope getExecutionScope() {
                return AndroidExecutionScope.BACKGROUND;
            }
        });
    }

    private void registerReceivers() {
        new EntityChangeReceiver(Environment.class, this).register(mContext);
        new WifiChangeReceiver(this).register(mContext);
    }

    private Promise<Void, Throwable, Void> loadEnvironments() {
        return mDm
                .when(()->{
                    List<Environment> envs = mDb.getEnvironmentProfiles();
                    mProfiles.clear();
                    for (Environment e : envs) {
                        mProfiles.put(e.getId(), e);
                    }
                });
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public synchronized void onSharedPreferenceChanged(SharedPreferences sPrefs, String key) {
        if(key.equals(mContext.getString(R.string.preference_activecontext_key))) {
            int activeId = sPrefs.getInt(mContext.getString(R.string.preference_activecontext_key), 0);
            mActiveEnvironment = mProfiles.get(activeId);
        }
    }

    @Override
    public synchronized void onEntityDeleted(Class<? extends Entity> type, int id) {
        if(type == Environment.class) {
            mProfiles.remove(id);
            if (mActiveEnvironment.getId() == id)
                setActiveEnvironment(0);
        }
    }

    public void setActiveEnvironment(Environment e) {
        setActiveEnvironment(e.getId());
    }

    private int loadActiveEnvironmentId(){
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        int currentId = sPrefs.getInt(mContext.getString(R.string.preference_activecontext_key), 0);
        return currentId;
    }

    public void setActiveEnvironment(int i) {
        if(mProfiles.get(i) == null)
            return;
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        int currentId = sPrefs.getInt(mContext.getString(R.string.preference_activecontext_key), 0);
        if(currentId == i){
            Log.d(TAG, i+" is already the active environment");
            return;
        }

        final SharedPreferences.Editor edit = sPrefs.edit();
        edit.putInt(mContext.getString(R.string.preference_activecontext_key), i);
        edit.commit();
    }

    @Override
    public synchronized void onEntityChanged(Class<? extends Entity> type, int id) {
        if(type == Environment.class) {
            updateEnvironment(id).then((e) -> {
                if(getActiveEnvironment() == null)
                    setActiveEnvironment(id); //Only for the first start ever.
                else if (id == getActiveEnvironment().getId()) {
                    findActiveEnvironment();
                }
            });
        }
    }

    @Override
    public void onEntityCreated(Class<? extends Entity> type, int id) {
        if(type == Environment.class) {
            updateEnvironment(id);
        }
    }

    private Promise<Environment, Throwable, Void> updateEnvironment(final int id) {
       return mDm
                .when(()->mDb.getEnvironmentProfile(id))
                .<Environment>then((e)->mProfiles.put(e.getId(), e));
    }

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * @param context
     * @return
     */
    public static EnvironmentManager getInstance(Context context){
        context = context.getApplicationContext();
        if(mInstance == null) {
            mInstance = new EnvironmentManager(context);
        }
        return mInstance;
    }

    public Environment getActiveEnvironment() {
        try {
            mInitialized.await();
        } catch (InterruptedException e) {
            throw new IllegalStateException("EnvironmentManager is not initialized");
        }
        return mActiveEnvironment;
    }

    @Override
    public void onWifiConnected(NetworkInfo networkInfo, WifiInfo wifiInfo) {
        findActiveEnvironment(wifiInfo);
    }

    private void findActiveEnvironment() {
        findActiveEnvironment(getCurrentWifiInfo());
    }

    private WifiInfo getCurrentWifiInfo() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        return wifiManager.getConnectionInfo();
    }

    private void findActiveEnvironment(WifiInfo wifiInfo) {
        Runnable find = ()-> {
                    String ssid = wifiInfo.getSSID();
                    for (Environment e : mProfiles.values()) {
                        final String envSsid = e.getWifiSsid();
                        if (envSsid != null && envSsid.equals(ssid)) {
                            if (e.testConnection(mContext)) {
                                setActiveEnvironment(e.getId());
                            }else {
                                setActiveEnvironment(0);
                            }
                            return;
                        }
                    }
                     final int networkId = wifiInfo.getNetworkId();
                    Environment env = new Environment();
                    env.setWifiSsid(ssid);
                    env.setName(networkId < 0 ? "Mobile" : ssid);
                    env.setNetworkId(networkId);
                    env.setHost(mProfiles.get(0).getHost()); //init with default IP
                    mDb.getInstance(mContext).saveEnvironmentProfile(env);
                    setActiveEnvironment(env);
                };
        if(Looper.myLooper() == Looper.getMainLooper())
            mDm.when(find);
        else
            find.run();
        //setActiveEnvironment(0);
    }

    @Override
    public void onWifiDisconnected(NetworkInfo networkInfo) {
        setActiveEnvironment(0);
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
