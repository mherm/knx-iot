package de.hsrm.knx.knxbrowser.ui.controls;

import android.util.Log;
import android.widget.CompoundButton;

import de.hsrm.knx.knxbrowser.model.Datapoint;
import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import obix.Obj;
import obix.io.KnxObixDecoder;
import obix.io.KnxObixEncoder;

/**
 * Created by Manuel Hermenau on 21.09.2016.
 */
public class ToggleSwitchListener implements CompoundButton.OnCheckedChangeListener {
    private static final String TAG = ToggleSwitchListener.class.getName();
    private final Obj obj;
    private final KnxObixBrowser browser;
    private Datapoint dtp;
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================
    public ToggleSwitchListener(Obj obj, KnxObixBrowser browser, Datapoint dtp){
        this.obj = KnxObixDecoder.fromString(KnxObixEncoder.toString(obj)); //clone mObj
        this.browser = browser;
        this.dtp = dtp;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        obj.get(dtp.objName()).setBool(isChecked);
        buttonView.setEnabled(false);
        Log.d(TAG, "Changing value of "+obj.toString()+" to \""+isChecked+"\"");
        browser.submit(obj).<Obj>then((result) -> {
            boolean resultBool = result.get(dtp.objName()).getBool();
            if(resultBool == isChecked)
                Log.d(TAG, "Sucessfully changed value of "+obj.toString()+" to \""+isChecked+"\"");
            else
                Log.e(TAG, "Changing value of "+obj.toString()+" to \""+isChecked+"\" was unsuccessfull!");
            buttonView.setEnabled(true);
            if(buttonView.isChecked() != resultBool){
                buttonView.setOnCheckedChangeListener(null);
                buttonView.setChecked(resultBool);
                buttonView.setOnCheckedChangeListener(this);
            }
        }
        );
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
