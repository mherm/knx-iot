package de.hsrm.knx.knxbrowser.ui.controls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import de.hsrm.knx.knxbrowser.model.Datapoint;
import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import de.hsrm.knx.knxbrowser.R;
import obix.Obj;
import obix.Val;

/**
 * Created by Manuel Hermenau on 17.10.2016.
 */
class SliderCallback implements UiControlFactory.InitControlCallback {
    private final UnitConverter unit;
    private final Context mContext;
    private final KnxObixBrowser mBrowser;

    SliderCallback(UnitConverter unit, Context context, KnxObixBrowser browser) {
        this.unit = unit;
        mContext = context;
        mBrowser = browser;
    }

    @Override
    public View initControl(Val value, Obj obj, Datapoint dtp, ViewGroup parent) {
        LayoutInflater Li = LayoutInflater.from(mContext.getApplicationContext());
        ViewGroup layout = (ViewGroup) Li.inflate(R.layout.control_popup_slider, null);
        final TextView textView = (TextView) layout.findViewById(R.id.slider_value);
        double val = getVal(value);
        int progressVal = unit.toProgressVal(val);
        textView.setText(progressVal + unit.unit());
        SeekBar slider = (SeekBar) layout.findViewById(R.id.seek_bar);
        slider.setProgress(progressVal);
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            long lastAction= 0;
            int lastValue = -1;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    lastValue = progress;
                    if(System.currentTimeMillis() - lastAction > 200) {
                        sendValue(progress);
                    }
                    lastAction = System.currentTimeMillis();
                }
            }

            private void sendValue(int progress) {
                unit.setVal(value, progress);
                mBrowser.submit(obj).<Obj>then((o) -> {
                    final Val newValObj = (Val) o.get(dtp.objName());
                    double newVal = getVal(newValObj);
                    int progressVal = unit.toProgressVal(newVal);
                    if(slider.isPressed())
                        textView.setText(progressVal + unit.unit());
                });
                lastValue = -1;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setVisibility(View.GONE);
                textView.setVisibility(View.VISIBLE);
                if(lastValue != -1){
                    sendValue(lastValue);
                }
            }
        });

        final View.OnTouchListener onTouchListener = (v, e) -> {
            final int action = e.getAction();
            if(!(action == MotionEvent.ACTION_UP && action == MotionEvent.ACTION_HOVER_EXIT))
                slider.dispatchTouchEvent(e);
            if (action == MotionEvent.ACTION_DOWN
                    || action == MotionEvent.ACTION_HOVER_ENTER) {
                textView.setVisibility(View.GONE);
                slider.setVisibility(View.VISIBLE);
                return true;
            }
            return false;
        };
        parent.setOnTouchListener(onTouchListener);
        textView.setOnTouchListener(onTouchListener);
        final View line = (View) parent.getParent();
        line.setOnHoverListener((v, e)->onTouchListener.onTouch(v,e));
        line.setOnTouchListener(onTouchListener);
        return layout;
    }

    private double getVal(Val value) {
        try{
            return  value.getReal();
        }catch (ClassCastException e){
            //Not a double
        }
        return Double.valueOf(value.getInt());
    }

}
