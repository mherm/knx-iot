package de.hsrm.knx.knxbrowser.script;

import org.joda.time.DateTime;

import de.hsrm.knx.knxbrowser.db.SimpleEntity;

/**
 * Created by Manuel Hermenau on 14.10.2016.
 */
public class ExecutionResult extends SimpleEntity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private int                     mScriptId;
    private DateTime                mStartTime;
    private DateTime                mEndTime;
    private StringBuffer            mLog = new StringBuffer();
    private String                  mTrigger;
    private boolean                 mSuccess = false;
    // ===========================================================
    // Constructors
    // ===========================================================


    public ExecutionResult() {
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public DateTime getEndTime() {
        return mEndTime;
    }

    public void setEndTime(DateTime endTime) {
        mEndTime = endTime;
    }

    public StringBuffer getLog() {
        return mLog;
    }

    public void setLog(StringBuffer log) {
        mLog = log;
    }

    public int getScriptId() {
        return mScriptId;
    }

    public void setScriptId(int scriptId) {
        mScriptId = scriptId;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean success) {
        mSuccess = success;
    }

    public String getTrigger() {
        return mTrigger;
    }

    public void setTrigger(String trigger) {
        mTrigger = trigger;
    }

    public DateTime getStartTime() {
        return mStartTime;
    }

    public void setStartTime(DateTime startTime) {
        mStartTime = startTime;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
