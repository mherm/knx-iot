package de.hsrm.knx.knxbrowser.db;

import org.joda.time.DateTime;

import de.hsrm.knx.knxbrowser.db.SimpleEntity;
import de.hsrm.knx.knxbrowser.script.ExecutionResult;
import de.hsrm.knx.knxbrowser.script.ast.KnxScript;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class KnxScriptConfiguration extends SimpleEntity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private KnxScript mKnxScript;
    private KnxScript mPendingChanges;
    private DateTime mCreated;
    private boolean isEnabled;

    private ExecutionResult latestExecution;

    // ===========================================================
    // Constructors
    // ===========================================================

    public KnxScriptConfiguration() {}

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public DateTime getCreated() {
        return mCreated;
    }

    public void setCreated(DateTime created) {
        mCreated = created;
    }

    public String getDescription() {
        return mKnxScript.getDescription();
    }

    public KnxScript getKnxScript() {
        return mKnxScript;
    }

    public void setKnxScript(KnxScript knxScript) {
        mKnxScript = knxScript;
    }

    public KnxScript getPendingChanges() {
        return mPendingChanges;
    }

    public void setPendingChanges(KnxScript pendingChanges) {
        mPendingChanges = pendingChanges;
    }

    public String getTitle() {
        return mKnxScript.getTitle();
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public ExecutionResult getLatestExecution() {
        return latestExecution;
    }

    public void setLatestExecution(ExecutionResult latestExecution) {
        this.latestExecution = latestExecution;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
