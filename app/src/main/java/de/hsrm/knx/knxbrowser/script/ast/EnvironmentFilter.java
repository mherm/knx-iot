package de.hsrm.knx.knxbrowser.script.ast;

import android.content.Context;
import android.util.Log;

import de.hsrm.knx.knxbrowser.Environment;
import de.hsrm.knx.knxbrowser.EnvironmentManager;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class EnvironmentFilter implements ActivationFilter{
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = EnvironmentFilter.class.getName();
    // ===========================================================
    // Fields
    // ===========================================================
    String mValue;
    Type mType;
    // ===========================================================
    // Constructors
    // ===========================================================

    public EnvironmentFilter(Type type, String value) {
        mValue= value;
        mType = type;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public boolean matches(Context context) {
        //FIXME: Was is mit mehreren profiles?
        Environment activeEnvironment =  EnvironmentManager.getInstance(context).getActiveEnvironment();
        if(mType == Type.Name && !activeEnvironment.getName().equals(mValue))
            return false;
        if(mType == Type.SSID && !activeEnvironment.getWifiSsid().equals(mValue))
            return false;
        final boolean result = activeEnvironment.testConnection(context);
        if(!result)
            Log.v(TAG, "Active profile connection is not available.");
        return result;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    public enum Type implements ScriptElement{
        Name, SSID, Any;
        public static Type lookup(String str){
            for (Type m : Type.values()) {
                if (m.name().equalsIgnoreCase(str)) {
                    return m;
                }
            }
            return Any;
        }
    }

}
