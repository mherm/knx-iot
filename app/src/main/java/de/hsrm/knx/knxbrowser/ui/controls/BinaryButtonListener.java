package de.hsrm.knx.knxbrowser.ui.controls;

import android.view.View;
import android.widget.ToggleButton;

import de.hsrm.knx.knxbrowser.model.Datapoint;
import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import de.hsrm.knx.knxbrowser.Utils;
import obix.Obj;

/**
 * Created by Manuel Hermenau on 14.10.2016.
 */
public class BinaryButtonListener implements View.OnClickListener{
    final boolean mVal;
    final Obj mObj;
    final View mParentView;
    final ToggleButton mOtherButton;
    final KnxObixBrowser mKnxObixBrowser;
    final Datapoint mDtp;

    public BinaryButtonListener(boolean val, Obj obj, View parentView, ToggleButton other, KnxObixBrowser knxObixBrowser, Datapoint dtp) {
        mOtherButton = other;
        mVal = val;
        mObj = obj;
        mParentView = parentView;
        mKnxObixBrowser = knxObixBrowser;
        mDtp = dtp;
    }

    @Override
    public void onClick(View view) {
        mObj.get(mDtp.objName()).setBool(mVal);
        mParentView.setEnabled(false);
        mKnxObixBrowser.submit(mObj).<Obj>then((o)->{
            mParentView.setEnabled(true);
            ((ToggleButton)view).setChecked(false);

            //Highlight the current value only if the DTP is readable
            //Or is it okay  because we just wrote it successfully?
            if(Utils.isReadable(o.get(mDtp.objName()))) {
                final boolean value = o.get(mDtp.objName()).getBool();
                if (value == mVal) {
                    ((ToggleButton)view).setChecked(true);
                    mOtherButton.setChecked(false);
                } else {
                    ((ToggleButton)view).setChecked(false);
                    mOtherButton.setChecked(true);
                }
            }
        });
    }
}
