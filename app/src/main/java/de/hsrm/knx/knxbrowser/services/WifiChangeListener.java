package de.hsrm.knx.knxbrowser.services;

import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;

/**
 * Created by Manuel Hermenau on 12.10.2016.
 */
public interface WifiChangeListener {
    public void onWifiConnected(NetworkInfo networkInfo, WifiInfo wifiInfo);

    public void onWifiDisconnected(NetworkInfo networkInfo) ;
}
