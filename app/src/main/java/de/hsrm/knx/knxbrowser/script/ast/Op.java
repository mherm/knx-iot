package de.hsrm.knx.knxbrowser.script.ast;

/**
 * Created by Manuel Hermenau on 06.10.2016.
 */
public abstract class Op implements ScriptElement{
    // ===========================================================
    // Constants
    // ===========================================================
    public static final Op EQ = new Op("=") {
        @Override
        public boolean evaluate(Literal<Comparable> left, Literal<Comparable> right) {
            return left.get().compareTo(right.get()) == 0;
        }
    };

    public static final Op GT = new Op(">") {
        @Override
        public boolean evaluate(Literal<Comparable> left, Literal<Comparable> right) {
            return left.get().compareTo(right.get()) >0 ;
        }
    };

    public static final Op LT = new Op("<") {
        @Override
        public boolean evaluate(Literal<Comparable> left, Literal<Comparable> right) {
            return left.get().compareTo(right.get()) < 0;
        }
    };
    public static final Op GE = new Op(">=") {
        @Override
        public boolean evaluate(Literal<Comparable> left, Literal<Comparable> right) {
            return left.get().compareTo(right.get()) >= 0 ;
        }
    };

    public static final Op LE = new Op("<=") {
        @Override
        public boolean evaluate(Literal<Comparable> left, Literal<Comparable> right) {
            return left.get().compareTo(right.get()) <= 0;
        }
    };
    public static final Op NEQ = new Op("!=") {
        @Override
        public boolean evaluate(Literal<Comparable> left, Literal<Comparable> right) {
            return !EQ.evaluate(left,right);
        }
    };
    // ===========================================================
    // Fields
    // ===========================================================
    String mSymbol;

    // ===========================================================
    // Constructors
    // ===========================================================
    Op(String symbol){
        mSymbol = symbol;
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public abstract boolean evaluate(Literal<Comparable> left, Literal<Comparable> right);

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
