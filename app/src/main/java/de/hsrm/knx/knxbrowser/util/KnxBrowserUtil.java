package de.hsrm.knx.knxbrowser.util;

/**
 * Created by Manuel Hermenau on 28.09.2016.
 */
public class KnxBrowserUtil {
    public static String trimContent(String str){
        return str.trim().replaceAll("\\s+", " ");
    }

}
