package de.hsrm.knx.knxbrowser.ui.activities;

import de.hsrm.knx.knxbrowser.BeaconId;

/**
 * Created by Manuel Hermenau on 25.09.2016.
 */

public interface RoomChangedListener {
    public void onRoomChanged(BeaconId newRoom, BeaconId oldRoom);
}
