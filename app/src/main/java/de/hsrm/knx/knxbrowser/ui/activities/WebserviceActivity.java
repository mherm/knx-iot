package de.hsrm.knx.knxbrowser.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.jdeferred.android.AndroidDeferredManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import de.hsrm.knx.knxbrowser.net.KnxBrowserView;
import de.hsrm.knx.knxbrowser.net.KnxNavigationFailedException;
import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.EnvironmentManager;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.db.AndroidDatabaseManager;
import de.hsrm.knx.knxbrowser.ui.ObjAdapter;
import de.hsrm.knx.knxbrowser.util.URIUtil;
import obix.Obj;
import obix.Uri;

import static butterknife.ButterKnife.findById;

/**
 * Created by Manuel Hermenau on 16.09.2016.
 */
public class WebserviceActivity extends LocationListenerActivity implements KnxBrowserView, SharedPreferences.OnSharedPreferenceChangeListener {


    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = WebserviceActivity.class.getName();
    public static final String ACTION_LOCATE_ME = "de.hsrm.knx.knxbrowser.browser.action.LOCATE_ME" ;

    public enum BrowseMode {STATIC, DYNAMIC}

    // ===========================================================
    // Fields
    // ===========================================================

    //UI Elements
    private ArrayAdapter<Obj> mObjectListAdapter;
    private final ArrayList<Obj> mObjectList = new ArrayList<>();
    @BindView(R.id.objListView ) ListView mObjectListView;
    @BindView(R.id.swipe_refresh_list)          SwipeRefreshLayout mSwipeRefreshListLayout;
    @BindView(R.id.swipe_refresh_no_connection) SwipeRefreshLayout mSwipeRefreshNoConnectionLayout;
    SwipeRefreshLayout mSwipeRefreshLayout = mSwipeRefreshListLayout;
    @BindView(R.id.floating_map) LinearLayout mFloatingMap;
    MenuItem mShowHideMapCheckbox;
    MenuItem mBrowseModeMenuItem;
    MenuItem mLocateMeMenuItem;

    //Session
    private KnxObixBrowser mBrowser;
    private BrowseMode mBrowseMode = BrowseMode.STATIC;

    //
    private AndroidDeferredManager mDm = new AndroidDeferredManager();

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBrowser(savedInstanceState);
        final SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        sPrefs.registerOnSharedPreferenceChangeListener(this);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBrowser.addView(this);
        if(ACTION_LOCATE_ME.equals(getIntent().getAction()))
            locateMe();
    }

    @Override
    protected void onPause() {
        if(mBrowser != null)
            mBrowser.removeView(this);
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mBrowser != null)
            outState.putParcelable("browser", mBrowser);
        if(mShowHideMapCheckbox != null)
            outState.putBoolean("showMap",  mShowHideMapCheckbox.isChecked());
        if(mBrowseMode != null)
            outState.putSerializable("browseMode", mBrowseMode);
        outState.putBoolean("isConnected", mSwipeRefreshNoConnectionLayout.getVisibility() == View.GONE);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        boolean showMap = savedInstanceState.getBoolean("showMap", true);
        mBrowseMode = (BrowseMode) savedInstanceState.get("browseMode");
        if(mBrowseMode == null)
            mBrowseMode = BrowseMode.STATIC;

        if(!showMap)
            mFloatingMap.setVisibility(View.GONE);

        boolean isConnected = savedInstanceState.getBoolean("isConnected");
        if(!isConnected){
           showNoConnectionView();
        }

    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "SharedPreference changed: "+key);
        if(key.equals(getString(R.string.preference_gatewayip_key))){
            if(mBrowser != null)
                mBrowser.removeView(this);
            initBrowser(null);
            mBrowser.addView(this);
        }
    }

    protected void initBrowser(Bundle savedInstanceState){
        KnxObixBrowser browser = null;
        if(savedInstanceState != null)
            browser = (KnxObixBrowser) savedInstanceState.getParcelable("browser");
        if(browser != null)
            mBrowser = browser;
        else {
            mBrowser = KnxObixBrowser.fromContext(this);
        }
        mDm.when(()->mBrowser.waitForInit(1000)).fail((e)->showNoConnectionView());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mShowHideMapCheckbox = menu.findItem(R.id.action_show_hide_map);
        if(mFloatingMap.getVisibility() != View.VISIBLE)
            mShowHideMapCheckbox.setChecked(false);

        //Locator
        mLocateMeMenuItem = menu.findItem(R.id.action_locate_me);

        //Static/Dynamic
        mBrowseModeMenuItem = menu.findItem(R.id.action_switch_browse_mode);
        if(this.mBrowseMode == BrowseMode.STATIC) {
            mBrowseModeMenuItem.setTitle(R.string.action_static_mode);
            mLocateMeMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        if(this.mBrowseMode == BrowseMode.DYNAMIC) {
            mBrowseModeMenuItem.setTitle(R.string.action_dynamic_mode);
            mLocateMeMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.action_switch_browse_mode:
                if(this.mBrowseMode == BrowseMode.STATIC) {
                    mBrowseMode = BrowseMode.DYNAMIC;
                    item.setTitle(R.string.action_dynamic_mode);
                    mLocateMeMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
                    return true;
                }
                if(this.mBrowseMode == BrowseMode.DYNAMIC) {
                    mBrowseMode = BrowseMode.STATIC;
                    item.setTitle(R.string.action_static_mode);
                    mLocateMeMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    return true;
                }
                return true;
            case R.id.action_show_hide_map:
                boolean isChecked = !item.isChecked();
                item.setChecked(isChecked);

                if(isChecked){
                    mFloatingMap.setVisibility(View.VISIBLE);
                }else {
                    mFloatingMap.setVisibility(View.GONE);
                }

                return true;
            case R.id.action_show_scripts:
                startActivity(new Intent(this, ScriptsActivity.class));
                return true;
            case R.id.action_show_database_debug:
                startActivity(new Intent(this, AndroidDatabaseManager.class));
                return true;
            case R.id.action_locate_me:
                locateMe();
                return true;
            case android.R.id.home:
                if(!mBrowser.isAtHome()){
                    onBackPressed();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void locateMe() {
        if(mCurrentRoom != BeaconId.NONE){
            navigateToBeacon(mCurrentRoom);
        }else{
            mLocationService.requestCurrentRoom((bId)->navigateToBeacon(bId));
        }
    }

    @Override
    public void onRoomChanged(BeaconId newRoom, BeaconId oldRoom) {
//        Snackbar.make(findViewById(android.R.id.content), "Changed room. You are now "+mRoomIdTranslator.getDomainId(newRoom), Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();
        if(mBrowseMode == BrowseMode.DYNAMIC) {
            navigateToBeacon(newRoom);
        }

        if(newRoom != BeaconId.NONE){
            mLocateMeMenuItem.setIcon(R.drawable.ic_gps_fixed_white_24dp);
        }else{
            mLocateMeMenuItem.setIcon(R.drawable.ic_gps_not_fixed_white_24dp);
        }
    }

    private void navigateToBeacon(BeaconId newRoom) {
        Uri uri = getUri(newRoom);
        mBrowser.navigate(uri);
    }

    @NonNull
    private Uri getUri(BeaconId newRoom) {
        //TODO: Installation finden. Pfad finden...?
        final String beaconIdLookupPath =
                PreferenceManager.getDefaultSharedPreferences(this).getString(
                        getString(R.string.preference_beaconlookuppath_key),
                        getString(R.string.preference_beaconlookuppath_default)
                );
        final URL installationRoot = EnvironmentManager.getInstance(this).getActiveEnvironment().getInstallationRoot();
        final URL url = URIUtil.makeURL(installationRoot, beaconIdLookupPath + newRoom.represent());
        Uri uri = new Uri(url.toString());
        uri.setHref(uri);
        return uri;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void initUI() {
        //Apply Layout
        setContentView(R.layout.activity_browse_ws);
        ButterKnife.bind(this);


        //Create Toolbar
        Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mObjectListAdapter = createListAdapter();
        mObjectListView.setAdapter(mObjectListAdapter);

        mSwipeRefreshListLayout.setOnRefreshListener(() -> mBrowser.refresh());
        mSwipeRefreshNoConnectionLayout.setOnRefreshListener(() -> mBrowser.refresh());
        mSwipeRefreshLayout = mSwipeRefreshListLayout;
    }

    @OnItemClick(R.id.objListView)
    public void onChooseObj(AdapterView<?> parent, View view, int position, long id) {
        Obj clicked = mObjectListAdapter.getItem(position);
        if(clicked != null && clicked.isRef()) {
            Log.d(TAG, clicked.toString() + " clicked");
            mBrowser.navigate(clicked);
        }
    }

    @Override
    public void onNewObj(Obj obj){
        mSwipeRefreshLayout.setRefreshing(false);
        hideNoConnectionView();
        Log.d(TAG, "onNewObj() called with "+obj.toString());
        List<Obj> objects = inflate(obj.list());
        for(Obj o:objects)
            Log.v(TAG, ""+o.toString());
        mObjectListAdapter.clear();
        mObjectListAdapter.addAll(objects);
        String displayName = obj.getDisplayName();
        if(displayName != null && !displayName.trim().isEmpty()) {
            getSupportActionBar().setTitle(displayName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }else{
            getSupportActionBar().setTitle(R.string.app_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void onNavigationStart() {
        runOnUiThread(()-> {
            //hideNoConnectionView();
            mSwipeRefreshLayout.setRefreshing(true);
        });
    }

    @Override
    public void onNavigationFailed(KnxNavigationFailedException ex) {
        runOnUiThread(()-> {
            mSwipeRefreshLayout.setRefreshing(false);
            showNoConnectionView();
            //Snackbar.make(findViewById(android.R.id.content), "Navigation failed", Snackbar.LENGTH_LONG).show();
        });
    }

    private void showNoConnectionView(){
        mSwipeRefreshListLayout.setVisibility(View.GONE);
        mSwipeRefreshNoConnectionLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout = mSwipeRefreshNoConnectionLayout;
    }
    private void hideNoConnectionView(){
        mSwipeRefreshListLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshNoConnectionLayout.setVisibility(View.GONE);
        mSwipeRefreshLayout = mSwipeRefreshListLayout;
    }

    @Override
    public void onBackPressed() {
        mBrowser.navigateBack(false).fail((e)->super.onBackPressed());
    }

    @OnClick(R.id.floating_map)
    public void onMapClick(){
        Intent intent = new Intent(this, LocationMapActivity.class);
        startActivity(intent);
    }

    private List<Obj> inflate(Obj[] list) {
        List<Obj> result = new LinkedList<>();
        for(Obj o : list){
            if(o.getElement().equals("list")){
                result.addAll(inflate(o.list()));
            }else{
                result.add(o);
            }
        }
        return result;
    }

    private ArrayAdapter<Obj> createListAdapter() {
        return new ObjAdapter(this, mObjectList);
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
