package de.hsrm.knx.knxbrowser.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.Utils;
import de.hsrm.knx.knxbrowser.db.Entity;
import de.hsrm.knx.knxbrowser.db.EntityChangeReceiver;
import de.hsrm.knx.knxbrowser.db.EntityListener;
import de.hsrm.knx.knxbrowser.script.ExecutionResult;
import de.hsrm.knx.knxbrowser.db.KnxScriptConfiguration;

import static butterknife.ButterKnife.findById;

/**
 * Created by Manuel Hermenau on 15.10.2016.
 */
public class ScriptsActivity extends ListActivity<KnxScriptConfiguration> implements EntityListener {
    private static final int READ_FILE_REQUEST_CODE = 0x1610;

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private EntityChangeReceiver mEntityChangeReceiver;
    @BindView(R.id.create_new_script)
    FloatingActionButton mNewScriptFab;
    @BindView(R.id.load_script)
    FloatingActionButton mLoadScriptFab;

    // ===========================================================
    // Constructors
    // ===========================================================
    public ScriptsActivity() {
        super(R.layout.view_script_row);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onResume() {
        super.onResume();
        reloadData();
        registerListeners();
    }

    private void registerListeners() {
        mEntityChangeReceiver = new EntityChangeReceiver(Utils.<Class<? extends Entity>>lst(KnxScriptConfiguration.class, ExecutionResult.class), this);
        mEntityChangeReceiver.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mEntityChangeReceiver.unregister(this);
    }

    @Override
    protected void onListItemDelete(KnxScriptConfiguration listItem) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                mDb.deleteScript(listItem);
                return null;
            }
        }.execute();
    }

    @Override
    protected void bindListItem(View v, KnxScriptConfiguration item) {
        ImageView icon =        findById(v, R.id.list_icon);
        TextView title =        findById(v, R.id.list_title);
        TextView description =  findById(v, R.id.list_description);
        ImageView lastExecIcon= findById(v, R.id.list_context_icon);
        TextView lastExecution= findById(v, R.id.list_context_text);

        //In case this was a reused view, stop the animation
        lastExecIcon.setAnimation(null);

        ExecutionResult latestExec = item.getLatestExecution();

        title.setText(item.getTitle());
        description.setText(item.getDescription());
        icon.setOnClickListener((ic)->{
            new AsyncTask<Void,Void,Boolean>(){
                @Override
                protected Boolean doInBackground(Void... params) {
                    return mDb.setScriptEnabled(item.getId(), !item.isEnabled());
                }

                @Override
                protected void onPostExecute(Boolean newVal) {
                    super.onPostExecute(newVal);
                    item.setEnabled(newVal);
                    setScriptEnabled(item, icon);
                }
            }.execute();
        });

        setScriptEnabled(item, icon);

        if(latestExec != null) {
            DateTimeFormatter dtf = DateTimeFormat.mediumDateTime();
            final DateTime startTime = latestExec.getStartTime();
            final DateTime endTime = latestExec.getEndTime();
            final String timeStr = startTime.toString(dtf);
            if(endTime != null){
                //final Duration duration = new Duration(startTime, endTime);
                if(latestExec.isSuccess()){
                    lastExecution.setText(timeStr +" - "+getString(R.string.success));
                    lastExecIcon.setImageResource(R.drawable.success_reverse);
                }else{
                    lastExecution.setText(timeStr +" - "+getString(R.string.failed));
                    lastExecIcon.setImageResource(R.drawable.error_reverse);
                }
            }else{
                lastExecution.setText(timeStr +" - "+getString(R.string.running));
                lastExecIcon.setImageResource(R.drawable.running_reverse);
                Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
                lastExecIcon.startAnimation(rotate);

            }
        }else{
            lastExecution.setText(getString(R.string.never));
            lastExecIcon.setImageResource(R.drawable.never_small);
        }
    }

    private void setScriptEnabled(KnxScriptConfiguration item, ImageView icon) {

        if(item.isEnabled()){

            icon.setImageResource(R.drawable.green_doc);
        }else{
            icon.setImageResource(R.drawable.gray_doc);
        }
    }

    @Override
    protected void onItemClicked(KnxScriptConfiguration item) {
        Intent intent = new Intent(this, ScriptEditActivity.class);
        intent.putExtra(ScriptEditActivity.EXTRA_SCRIPT_ID, item.getId());
        startActivity(intent);
    }

    @Override
    public void onEntityDeleted(Class<? extends Entity> type, int id) {
//        if(type == KnxScriptConfiguration.class){
//            KnxScriptConfiguration script = findScriptById(id);
//            if(script != null)
//                mListAdapter.remove(script);
//            return;
//        }
        reloadData();
    }

    @OnClick(R.id.create_new_script)
    public void onNewScriptClicked(View v){
        Intent intent = new Intent(this, ScriptEditActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.load_script)
    public void onLoadScriptClicked(View v){
        performFileSearch();
    }

//    private KnxScriptConfiguration findScriptById(int id) {
//        for(int i=0; i<mList.size(); i++){
//                final KnxScriptConfiguration s = mList.get(i);
//                if(s.getId() == id)
//                    return s;
//        }
//        return null;
//    }

    @Override
    public void onEntityChanged(Class<? extends Entity> type, int id) {
        reloadData();
    }

    @Override
    public void onEntityCreated(Class<? extends Entity> type, int id) {
         reloadData();
    }

    private void reloadData(){
        new AsyncTask<Void,Void,List<KnxScriptConfiguration>>(){
            @Override
            protected List<KnxScriptConfiguration> doInBackground(Void... params) {
                return mDb.getScripts();
            }

            @Override
            protected void onPostExecute(List<KnxScriptConfiguration> knxScriptConfigurations) {
                super.onPostExecute(knxScriptConfigurations);
                mListAdapter.clear();
                Collections.sort(knxScriptConfigurations, (lhs, rhs) -> {
                    if(lhs.isEnabled() && rhs.isEnabled()){
                        return 0;
                    }
                    if(lhs.isEnabled())
                        return -1;
                    return 1;
                });
                mListAdapter.addAll(knxScriptConfigurations);
            }
        }.execute();
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select an image.
     */
    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("*/*");

        startActivityForResult(intent, READ_FILE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            if (resultData != null) {
                final Uri uri  = resultData.getData();
                Log.i(TAG, "Importing Uri: " + uri.toString());
                new AsyncTask<Void,Void,Void>(){

                    @Override
                    protected Void doInBackground(Void... params) {
                        mDb.loadScriptFromUri(uri);
                        return null;
                    }
                }.execute();
            }
        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
