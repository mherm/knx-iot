package de.hsrm.knx.knxbrowser.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import de.hsrm.knx.knxbrowser.db.Entity;

import static butterknife.ButterKnife.findById;

/**
 * Created by Manuel Hermenau on 19.09.2016.
 */
public abstract class EntityAdapter<T extends Entity> extends ArrayAdapter<T> {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private final int mLayout;
    // ===========================================================
    // Constructors
    // ===========================================================

    public EntityAdapter(Context context, int layout) {
        super(context, layout);
        mLayout = layout;
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public T getItem(int pos) {
        try {
            return super.getItem(pos);
        }catch (IndexOutOfBoundsException e){
            return null;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(mLayout, null);
        }

        T item = getItem(position);

        if (item != null) {
            bindView(v, item);
        }

        return v;
    }

    protected abstract void bindView(View view, T item) ;


    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}