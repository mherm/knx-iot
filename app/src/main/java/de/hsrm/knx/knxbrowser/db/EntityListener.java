package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 12.10.2016.
 */

public interface EntityListener {
    public abstract void onEntityDeleted(Class<? extends Entity> type, int id);
    public abstract void onEntityChanged(Class<? extends Entity> type, int id);
    public abstract void onEntityCreated(Class<? extends Entity> type, int id);
}
