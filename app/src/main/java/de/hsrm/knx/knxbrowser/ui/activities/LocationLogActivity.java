package de.hsrm.knx.knxbrowser.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.BeaconId;
import de.hsrm.knx.knxbrowser.MapRoomIdTranslator;
import de.hsrm.knx.knxbrowser.R;
import de.hsrm.knx.knxbrowser.RoomIdTranslator;

public class LocationLogActivity extends AppCompatActivity implements RoomChangedListener{

    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = LocationLogActivity.class.getName();


    // ===========================================================
    // Fields
    // ===========================================================

    //UI Elements
    private ArrayAdapter<String> mBeaconListAdapter;
    private final ArrayList<String> mBeaconList = new ArrayList<>();
    private ListView mBeaconListView;
    private TextView mNumBeaconsTextView;
    private LocationServiceConnector mLocationService;

    protected final RoomIdTranslator mRoomIdTranslator = new MapRoomIdTranslator(createRoomMappings());


    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        mLocationService = new LocationServiceConnector(this, savedInstanceState);
        mLocationService.addRoomChangedListener(this);
        //drawRoom(mLocationService.getCurrentRoom());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocationService.startListening();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationService.stopListening();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList("mBeaconList",mBeaconList);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ArrayList<String> beacons = savedInstanceState.getStringArrayList("mBeaconList");
        mBeaconList.addAll(beacons);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRoomChanged(BeaconId newRoom, BeaconId oldRoom) {
        Log.v(TAG, "onRoomChanged("+newRoom+", "+oldRoom+")");
        if(oldRoom == BeaconId.NONE && oldRoom != newRoom ){
            String youWere = getString(R.string.you_were_in_room, mRoomIdTranslator.getDomainId(oldRoom));
            mBeaconList.add(0, youWere);
            mBeaconListAdapter.notifyDataSetChanged();
        }
        String youAre = getString(R.string.you_are_in_room,   translateRoomId(newRoom));
        mNumBeaconsTextView.setText(youAre);
    }


    // ===========================================================
    // Methods
    // ===========================================================

    private void initUI() {

        //Apply Layout
        setContentView(R.layout.activity_location_log);

        //Create Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Init NumBeacons Label
        mNumBeaconsTextView = (TextView) findViewById(R.id.numBeaconsTextView);

        //Init List
        mBeaconListView = (ListView) findViewById(R.id.beaconListView);
        mBeaconListAdapter = createListAdapter();
        mBeaconListView.setAdapter(mBeaconListAdapter);
        mBeaconListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Snackbar.make(view, mBeaconListAdapter.getItem(position), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

        );
    }

    private void showCaptureMapActivity() {
//        Intent intent = new Intent(this, CaptureMapActivity.class);
//        startActivity(intent);
    }

    private ArrayAdapter<String> createListAdapter() {
        return new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mBeaconList);
    }

    /**
     * Only for use with my testing devices
     * @return
     */
    private Map<BeaconId,String> createRoomMappings() {
        Map<BeaconId, String> mapping = new HashMap<>();
        mapping.put(BeaconId.NONE, "not on the premises");
        mapping.put(new BeaconId(null,1,1), "your office");
        mapping.put(new BeaconId(null,2,2), "the corridor");
        mapping.put(new BeaconId(null,3,3), "the kitchen");

//        mapping.put(3031, "the corridor");
//        mapping.put(12544, "the kitchen");
//        mapping.put(17948, "your office");
        return mapping;
    }

    protected String translateRoomId(BeaconId id){
        return mRoomIdTranslator.getDomainId(id);
    }

}
