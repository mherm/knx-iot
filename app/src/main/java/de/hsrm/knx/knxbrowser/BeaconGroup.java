package de.hsrm.knx.knxbrowser;

import static java.lang.String.format;

import com.estimote.sdk.Beacon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Manuel Hermenau on 22.08.2016.
 */
public final class BeaconGroup implements Comparable<BeaconGroup> {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG = BeaconGroup.class.getName();

    // ===========================================================
    // Fields
    // ===========================================================


    /**
     * The common BeaconId (UUID, Major, Minor) of all beacons in this group
     */
    private final BeaconId mSharedId;

    /**
     * List of the strenghts
     */
    private final List<Double> mBeaconStrengths = new ArrayList<>();
    private double mAverageStrength = 0;

    private double mCutofffPrecent = 0.2d;

    private boolean mIngorePerfectScores = true;
    private boolean mIngoreZeroScores   = true;
    // ===========================================================
    // Constructors
    // ===========================================================

    public BeaconGroup(BeaconId sharedId) {
       mSharedId = sharedId;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public BeaconId getBeaconId() {
        return mSharedId;
    }

    public double getAverageStrength() {
        return mAverageStrength;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public int compareTo(BeaconGroup another) {
        // if(another == null)
        //??
        return Double.compare(this.mAverageStrength, another.mAverageStrength);
    }

    @Override
    public String toString() {
        return String.format("[%s, size: %d, strength: %f]", mSharedId.represent(), mBeaconStrengths.size(), mAverageStrength);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public BeaconGroup add(Beacon beacon) {
        BeaconId newBeaconId = new BeaconId(beacon);
        if (!mSharedId.equals(newBeaconId))
            throw new IllegalArgumentException("The given Beacon does not match this group. Expected: "+mSharedId+" - Got: "+newBeaconId);

        mBeaconStrengths.add(calcStrength(beacon));

        calcAverageStrength();

        return this;
    }

    private void calcAverageStrength() {
        List<Double> strengths = new ArrayList<>(mBeaconStrengths);
        Collections.sort(strengths);

        if(mIngorePerfectScores){
            removeEqual(strengths, 1.0);
        }
        if(mIngoreZeroScores){
            removeEqual(strengths, 0.0);
        }

        int cutoffCount = (int)(mCutofffPrecent * strengths.size());

        //Ignore the lowest and the highest strength
        if(strengths.size() > cutoffCount){
            for(int i =0; i<cutoffCount; i++)
                removeLowest(strengths);
        }
        if(strengths.size() > cutoffCount){
            for(int i =0; i<cutoffCount; i++)
                removeHighest(strengths);
        }

        double sum = 0;
        for (Double d : strengths){
            sum += d.doubleValue();
        }

        mAverageStrength = strengths.size() == 0 ? 0 : sum / strengths.size();
    }

    private void removeLowest(List<Double> strengths) {
        Double lowest = strengths.remove(0);
        //Log.v(TAG, format("Removed lowest value %f. Remaining: %s", lowest, strengths.toString()));
    }

    private void removeHighest(List<Double> strengths) {
        double highest = strengths.remove(strengths.size()-1);
        //Log.v(TAG, format("Removed highest value %f. Remaining: %s", highest, strengths.toString()));
    }
    private void removeEqual(List<Double> strengths, Double strength) {
        Iterator<Double> it = strengths.iterator();
        String str = String.format("%.4f", strength);
        int removed = 0;
        while(it.hasNext()){
            if(String.format("%.4f",it.next()).equals(str)) {
                it.remove();
                removed++;
            }
        }
        //Log.v(TAG, format("Removed %d values because of unrealistic perfect score. Remaining: %s", removed, strengths.toString()));
    }

    private Double calcStrength(Beacon beacon) {
        return Double.valueOf(normalize(beacon.getRssi(), beacon.getMeasuredPower()));
    }

    /**
     * Normalizes the RSSI value to make signal strengthes comparable
     *
     * @param rssi
     * @param measuredPower
     * @return
     */
    public static double normalize(int rssi, int measuredPower) {
        double distance = getDistance(rssi, measuredPower);
        double strength = Math.pow(10d, distance/-10);

        //Log.v(TAG, format("RSSI: %d, MeasuredPower: %d, estimatedDistance: %f, stregth: %f", rssi, measuredPower, distance, strength));
        return strength;
    }

    public static long getUnsignedInt(int x) {
        return x & 0x00000000ffffffffL;
    }

    static double getDistance(int rssi, int txPower) {
        /**
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         *
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */

        //return Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
        return calculateAccuracy(rssi, txPower);
    }

    /**
     * http://stackoverflow.com/questions/20416218/understanding-ibeacon-distancing
     * @param txPower
     * @param rssi
     * @return distance in meters
     */
    public static double calculateAccuracy(int rssi, double txPower) {
        if (rssi == 0) {
            return 500; // if we cannot determine accuracy, return 500.
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return accuracy;
        }
    }
    public static BeaconGroup create(Beacon first, Beacon... rest) {
        BeaconGroup beaconGroup = new BeaconGroup(new BeaconId(first));
        beaconGroup.add(first);
        for (Beacon b : rest) {
            beaconGroup.add(b);
        }
        return beaconGroup;
    }


    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================


}
