package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 04.08.2016.
 */
import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static de.hsrm.knx.knxbrowser.Utils.*;

public abstract class TableContract<T> {

    public abstract ContentValues serialize(T t);
    public abstract T deserialize(Cursor c);

    public abstract String tableName();

    protected static void contract (Map<String, ColumnContract> map, String column, String type, String modifier){
        map.put(column, new ColumnContract(type, modifier));
    }
    protected static void contract (Map<String, ColumnContract> map, String column, String type, String modifier, String constraint){
        map.put(column, new ColumnContract(type, modifier, constraint));
    }

    public static final class ColumnContract {
        public final String dataType,modifier;
        public final List<String> constraints;
        public ColumnContract(String dataType, String modifier) {
            this(dataType, modifier, Collections.emptyList());
        }
        public ColumnContract(String dataType, String modifier, String constraint) {
            this(dataType, modifier, lst(constraint));
        }
        public ColumnContract(String dataType, String modifier, List<String> constraints) {
            this.dataType = dataType;
            this.modifier = modifier;
            this.constraints = constraints;
        }
    }

    public static abstract class Operations {

        protected static String createTable(String tableName, Map<String, ColumnContract> columns) {
            StringBuilder sb = new StringBuilder("CREATE TABLE ");
            sb.append(tableName).append(" ( ");

            sb.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
            for(Map.Entry<String, ColumnContract> column : columns.entrySet()){
                addColumn(sb, column.getKey(), column.getValue().dataType, column.getValue().modifier);
            }
            for(Map.Entry<String, ColumnContract> column : columns.entrySet()){
                final List<String> constraints = column.getValue().constraints;
                for(String constraint : constraints)
                    addConstraint(sb, constraint);
            }
            sb.append(")");
            return sb.toString();
        }

        protected static void addColumn(StringBuilder sb, String name, String type, String modifier){
            sb.append(",\n");
            sb.append(name).append(" ").append(type);
            if(modifier != null){
                sb.append(" ").append(modifier);
            }
        }
        protected static void addConstraint(StringBuilder sb, String constraint){
            sb.append(",\n");
            sb.append(constraint);
        }
    }
}
