package de.hsrm.knx.knxbrowser.ui.activities;


import de.hsrm.knx.knxbrowser.ui.TextHighlighter;

/**
 * Created by Manuel Hermenau on 15.10.2016.
 */
public class Prettifier {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String[] descriptors = new String[]{
      "script", "description", "on", "do"
    };
    private static final String[] control_flow = new String[]{
      "if", "else", "parallel"
    };
    private static final String[] statements = new String[]{
      "notify", "ask", "knx","read","write","http", "wait"
    };
    private static final TextHighlighter highlighter = new TextHighlighter();
    static{
        highlighter.setColorForTheToken(descriptors, "#0b254f");
        highlighter.setColorForTheToken(control_flow, "#0b254f");
        highlighter.setColorForTheToken(statements, "#640a66");
        highlighter.setStyleForTheToken(descriptors, TextHighlighter.BOLD);
        highlighter.setStyleForTheToken(control_flow, TextHighlighter.BOLD);
        highlighter.setStyleForTheToken(statements, TextHighlighter.BOLD);
    }

    public static String toHtml(String str){
        return highlighter.getHighlightedText(str);
    }
}
