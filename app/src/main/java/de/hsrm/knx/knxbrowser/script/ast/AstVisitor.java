package de.hsrm.knx.knxbrowser.script.ast;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Set;

/**
 * Created by Manuel Hermenau on 10.10.2016.
 */

public abstract class AstVisitor<T> {
    public abstract T visit(AskStatement stmt);
    public abstract T visit(Block stmt);
    public abstract T visit(CurrentRoomStatement stmt);
    public abstract T visit(Expression stmt);
    public abstract T visit(EnvironmentFilter stmt);
    public abstract T visit(HttpStatement stmt);
    public abstract T visit(IfStatement stmt);
    public abstract T visit(KnxReadStatement stmt);
    public abstract T visit(KnxScript stmt);
    public abstract T visit(KnxWriteStatement stmt);
    public abstract T visit(Literal stmt);
    public abstract T visit(NotifyStatement stmt);
    public abstract T visit(Op stmt);
    public abstract T visit(ParallelBlock stmt);
    public abstract T visit(RoomTrigger stmt);
    public abstract T visit(Rule stmt);
    public abstract T visit(SwitchNode stmt);
    public abstract T visit(TimerangeFilter stmt);
    public abstract T visit(TimeTrigger stmt);
    public abstract T visit(WaitStatement stmt);

    public T visit(ScriptElement element){
        try {
            final Class<? extends ScriptElement> aClass = element.getClass();
            final Method visit = getMethod(aClass);
            T result = (T) visit.invoke(this, element);
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected  Method getMethod(Class<? extends ScriptElement> aClass) throws NoSuchMethodException {
        Method result;
        try {
            return this.getClass().getMethod("visit", aClass);
        } catch (NoSuchMethodException e) {
            final Class<?> superclass = aClass.getSuperclass();
            if (superclass != ScriptElement.class) {
                aClass = (Class<? extends ScriptElement>) superclass;
                return getMethod(aClass);
            }
        }
        throw new NoSuchMethodException();
    }


    protected T visitChildren(ScriptElement element) {
        final Set<Field> allFieldsUntil = ReflectionUtil.getAllFieldsUntil(element.getClass(), ScriptElement.class);
        T result = null;
            for(Field field : allFieldsUntil){
                if(Modifier.isStatic(field.getModifiers()))
                    continue;
                Class<?> propertyType = field.getType();
                Object value;
                try {
                    value = field.get(element);
                } catch (IllegalAccessException e) {
                   e.printStackTrace();
                    continue;
                }
                if(value == null)
                    continue;
                if (Collection.class.isAssignableFrom(propertyType)) {
                    //Is it a collection?
                    ParameterizedType genericType = ((ParameterizedType) field.getGenericType());
                    Type[] generics = genericType.getActualTypeArguments();
                    if (generics.length == 1 && ScriptElement.class.isAssignableFrom((Class<?>) generics[0])) {
                        //Is it a collection of ScriptElements?
                        for(ScriptElement e : (Collection<ScriptElement>)value) {
                            result = aggregateResult(result, visit(e));
                        }
                    }
                } else if (ScriptElement.class.isAssignableFrom(propertyType)) {
                    //Is it a ScriptElement?
                    result = aggregateResult(result, visit((ScriptElement) value));
                }
            }
        return result;
    }

    /**
     * Chooses always the latest result that is not {@code null}.
     * @param currentResult
     * @param newEvaluatedResult
     * @return
     */
    protected T aggregateResult(T currentResult, T newEvaluatedResult) {
        return newEvaluatedResult == null? currentResult : newEvaluatedResult;
    }
}
