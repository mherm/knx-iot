package de.hsrm.knx.knxbrowser;

import android.content.Context;

import java.net.URL;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import de.hsrm.knx.knxbrowser.net.KnxObixBrowser;
import de.hsrm.knx.knxbrowser.db.SimpleEntity;
import de.hsrm.knx.knxbrowser.maps.OutlineProvider;
import obix.Obj;

/**
 * Created by Manuel Hermenau on 11.10.2016.
 */
public class Environment extends SimpleEntity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private String  mName;
    private URL     mHost;
    private URL     mInstallationRoot;
    private String  mWifiSsid;
    private Integer mNetworkId;

    private OutlineProvider mOutlineProvider;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public URL getHost() {
        return mHost;
    }

    public String getName() {
        return mName;
    }

    public void setHost(URL host) {
        mHost = host;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getWifiSsid() {
        return mWifiSsid;
    }

    public void setWifiSsid(String wifiSsid) {
        mWifiSsid = wifiSsid;
    }

    public Integer getNetworkId() {
        return mNetworkId;
    }

    public void setNetworkId(Integer networkId) {
        mNetworkId = networkId;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public boolean testConnection(Context context){
        return testConnection(mHost, context, this);
    }

    public static boolean testConnection(URL host, Context context){
        return testConnection(host, context, null);
    }
    protected static boolean testConnection(URL host, Context context, Environment e){
        boolean isAvailable;
        try {
            final KnxObixBrowser from = KnxObixBrowser.from(host);
            from.waitForInit(1000);
            BlockingQueue<Boolean> resultQ = new ArrayBlockingQueue(1);
            from.refresh().<Obj>then(o-> {
                if(e != null) {
                    Obj[] list = o.get("installations").list();
                    if (list.length == 1) {
                        Obj installation = list[0];
                        e.mInstallationRoot = from.resolveUri(installation);
                    }
                }
                resultQ.add(true);
            }
            ).fail(t->{
                resultQ.add(false);
            });
            isAvailable = resultQ.take().booleanValue();
        }catch (Exception ex){
            return false;
        }
        return isAvailable;
    }

    public URL getInstallationRoot() {
        return mInstallationRoot;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
