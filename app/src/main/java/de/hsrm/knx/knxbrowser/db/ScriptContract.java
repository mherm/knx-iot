package de.hsrm.knx.knxbrowser.db;

/**
 * Created by Manuel Hermenau on 04.08.2016.
 */
import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import de.hsrm.knx.knxbrowser.Utils;
import de.hsrm.knx.knxbrowser.script.ast.KnxScript;
import de.hsrm.knx.knxbrowser.script.ast.KnxScriptWriter;

public class ScriptContract extends TableContract<KnxScriptConfiguration>{
    public static final String TABLE_NAME = "scripts";

    private static final ScriptContract instance = new ScriptContract();

    private ScriptContract(){};

    /*Below is a subclass that defines the column names*/
    public static abstract class ScriptEntry implements BaseColumns {
        public static final Map<String, ColumnContract> COLUMNS = new HashMap<>();

        //Column Name Constants
        public static final String SCRIPT                = "script";
        public static final String ENABLED               = "enabled";
        public static final String CREATED               = "created";

        //Map creation
        static {
            contract(COLUMNS, SCRIPT,           "TEXT",      "DEFAULT ''");
            contract(COLUMNS, ENABLED,          "INTEGER",   "DEFAULT 1");
            contract(COLUMNS, CREATED,          "DATETIME",  "DEFAULT (datetime('now','localtime'))");
        }
    }

    public static abstract class ScriptOperations extends Operations {
        public static final String CREATE_TABLE =
                createTable(TABLE_NAME, ScriptEntry.COLUMNS);
        public static final String DROP_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public KnxScriptConfiguration deserialize(Cursor c){
        int id              = c.getInt(c.getColumnIndexOrThrow(ScriptEntry._ID));
        String scriptStr    = c.getString(c.getColumnIndexOrThrow(ScriptEntry.SCRIPT));
        boolean enabled     = c.getInt(c.getColumnIndexOrThrow(ScriptEntry.ENABLED)) != 0;
        long createdTs      = c.getLong(c.getColumnIndexOrThrow(ScriptEntry.CREATED));

        KnxScript script = KnxScript.parse(scriptStr);
        final DateTime created = Utils.fromUnixTs(createdTs);

        KnxScriptConfiguration result = new KnxScriptConfiguration();
        result.setId(id);
        result.setEnabled(enabled);
        result.setCreated(created);
        result.setKnxScript(script);
        return result;
    }

    @Override
    public String tableName() {
        return TABLE_NAME;
    }

    public static TableContract<KnxScriptConfiguration> getInstance() {
        return instance;
    }

    public ContentValues serialize(KnxScriptConfiguration e){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        KnxScriptWriter writer = new KnxScriptWriter(bos);
        writer.visit(e.getKnxScript());
        String script = bos.toString();
        ContentValues cv = new ContentValues();
        if(e.getId() >= 0)
            cv.put(ScriptEntry._ID, e.getId());
        cv.put(ScriptEntry.SCRIPT,  script);
        cv.put(ScriptEntry.ENABLED, e.isEnabled());
        return cv;
    }

}
