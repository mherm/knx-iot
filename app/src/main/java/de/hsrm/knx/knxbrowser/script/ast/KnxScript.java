package de.hsrm.knx.knxbrowser.script.ast;

import android.content.Context;
import android.util.Log;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.joda.time.DateTime;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

import de.hsrm.knx.knxbrowser.script.ExecutionResult;
import de.hsrm.knx.knxbrowser.script.grammar.KnxScriptLexer;
import de.hsrm.knx.knxbrowser.script.grammar.KnxScriptParser;


/**
 * Created by Manuel Hermenau on 06.10.2016.
 */
public class KnxScript implements ScriptElement{
    private static final String TAG = KnxScript.class.getName();
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    String mTitle;
    String mDescription;
    List<ActivationFilter> mActivators = new LinkedList<>();
    Rule mRule;

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public List<ActivationFilter> getActivators() {
        return mActivators;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getTitle() {
        return mTitle;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public ExecutionResult run(Context context){
        ExecutionResult result = new ExecutionResult();
        result.setStartTime(new DateTime());
        try {
            final StatementEvaluator evaluator = new StatementEvaluator(context);
            evaluator.visit(mRule.mRunBlock);
            result.setSuccess(true);
        }catch (Throwable e){
            Log.e(TAG, String.format("Error while evaluating '%s'", getTitle()), e);
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            result.getLog().append(errors.toString());
            result.setSuccess(false);
        }
        result.setEndTime(new DateTime());
        return result;
    }

    public static KnxScript read(InputStream inputStream) {
        try {
            final CharStream charStream = new ANTLRInputStream(inputStream);
            final KnxScriptLexer lexer = new KnxScriptLexer(charStream);
            final CommonTokenStream tokenStream = new CommonTokenStream(lexer);
            final KnxScriptParser knxScriptParser = new KnxScriptParser(tokenStream);
            final KnxScriptParser.StartContext start = knxScriptParser.start();
            return (KnxScript) new AstBuilder().visitStart(start);
        }catch(Exception e){
            throw new RuntimeException("Could not read script", e);
        }
    }

    /**
     * Parse a string containing a KnxScript
     * @param script
     * @return
     */
    public static KnxScript parse(String script) {
        return read(new ByteArrayInputStream(script.getBytes()));
    }

    public Rule getRule() {
        return mRule;
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
