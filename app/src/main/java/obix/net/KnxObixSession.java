package obix.net;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Stack;

import obix.Err;
import obix.Obj;
import obix.Uri;
import obix.io.KnxObixDecoder;
import obix.io.KnxObixEncoder;
import obix.net.ObixSession;

/**
 * Created by Manuel Hermenau on 19.09.2016.
 */
public class KnxObixSession extends ObixSession {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final int TIMEOUT = 10 * 1000; //ms

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    public KnxObixSession(Uri lobbyUri) {
        super(lobbyUri, null, null, TIMEOUT);
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    /**
     * Open a session.
     */
    public void open()
            throws Exception
    {
        // read lobby
        lobby = read(lobbyUri);
    }

    public void open(Obj lobby) {
        this.lobby = lobby;
    }


    /**
     * Close a session.
     */
    public void close()
    {
        lobby = null;
    }


    /**
     * Read the obix document as an Obj instance using the
     * specified uri relative to the base address of the sesssion.
     */
    public Obj follow(Obj obj)
            throws Exception
    {

        return new KnxObixDecoder(open(toAbsoluteUri(obj))).decodeDocument();
    }

    /**
     * Write the specified obj back to the server using the
     * obj's href.  Return a new object containing the
     * server's result.
     */
    public Obj write(Obj obj)
            throws Exception
    {
        Uri href = toAbsoluteUri(obj);
        if (href == null) throw new Exception("obj.href is null");

        return send(href, "PUT", obj);
    }

    /**
     * POST or PUT the specified payload to the given uri.
     */
    protected Obj send(Uri href, String method, Obj payload)
            throws Exception
    {
        if (useHttpURLConnection)
        {
            HttpURLConnection conn = setupHttpURLConnection(href,method);
            conn.connect();
            OutputStream out = conn.getOutputStream();
            try
            {
                // encode request
                new KnxObixEncoder(out).encodeDocument(payload);
                out.flush();

                // read response
                int rc = conn.getResponseCode();
                if ((rc / 200) != 1)
                    throw new Exception("Invalid response code " + rc);
                InputStream in = conn.getInputStream();
                Obj result = new KnxObixDecoder(in).decodeDocument();
                try {in.close();} catch (Exception x) {}
                if (result instanceof Err) throw new ErrException((Err)result);

                // if the result doesn't have an href, then use
                // server authority for normalization puroseses
                if (result.getHref() == null)
                    result.setHref(authority);

                return result;
            }
            finally
            {
                try {out.close();} catch (Exception x) {}
            }
        }
        else
        {
            OutputStream out = null;
            int rc = -1;
            try
            {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                new KnxObixEncoder(os).encodeDocument(payload);
                HttpConnection conn = setupHttpConnection(href);
                if (method.equalsIgnoreCase("PUT"))
                    rc = conn.put(href.get(), "text/xml", os.toByteArray());
                else
                    rc = conn.post(href.get(), "text/xml", os.toByteArray());

                if ((rc / 200) != 1)
                    throw new Exception("Invalid response code " + rc);

                InputStream in = conn.getInputStream();
                Obj result = new KnxObixDecoder(in).decodeDocument();
                try {in.close();} catch (Exception x) {}
                if (result instanceof Err) throw new ErrException((Err)result);

                // if the result doesn't have an href, then use
                // server authority for normalization puroseses
                if (result.getHref() == null)
                    result.setHref(authority);

                return result;
            }
            finally
            {
                try {if (out != null) out.close();} catch (Exception x) {}
            }
        }
    }


    // ===========================================================
    // Methods
    // ===========================================================

    public Uri resolveUri(Obj o){
        return toAbsoluteUri(o);
    }

    protected Uri toAbsoluteUri(Obj uri){
        Obj o = uri;
        if(o == null)
            return getLobbyUri();
        if(o.getHref() != null && o.getHref().isAbsolute()) return o.getHref();
        Obj parent = o;

        do{
            parent = parent.getParent();
        }while(parent != null && parent.getHref() == null);
        if(parent != null && !parent.getHref().get().endsWith("/"))
            parent.getHref().set(parent.getHref().get()+"/");
        return o.getHref().normalize(toAbsoluteUri(parent));
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
