grammar knxscript;

TIME:	[012] NUM ':' [012345] NUM;
URL	:	('http://' | '/') (ALPHANUM | DOT | '/' | '%' | MINUS |COLON | '_' | ',')+;
DTP	:	'knx:DPST-'NUM MINUS INTEGER;
BLOCK_START	: NEWLINE? '{' WS*  NEWLINE;
BLOCK_END	: '}' WS* NEWLINE?;
WS	:	('\t' | ' ' |'\f' )+ -> skip;

ENV_ID_TYPE: NAME | SSID;

fragment NAME: 'name';
fragment SSID: 'ssid';
fragment ANY: 'any';

fragment NUM:	[0123456789];

fragment DOT    : '.';
fragment COLON  : ':';
fragment MINUS	: '-';


STRING

: '"' ( ~( '"' | '\\' ) | '\\' . )* '"'
 {
     String s = getText();
     s = s.substring(1, s.length() - 1); // strip the leading and trailing quotes
     s = s.replace("\"\"", "\""); // replace all double quotes with single quotes
     setText(s);
   }
;




start
	:	header activation? rule EOF;

header	:	'script' title NEWLINE ('description' description NEWLINE)?;

title: STRING;
description: STRING;
activation
	:	'active' active_block;


active_block	:	(BLOCK_START filter+ BLOCK_END) | filter;


filter	:	(environment_filter | timerange_filter) NEWLINE;

environment_filter
	:	'environment' ENV_ID_TYPE STRING;

timerange_filter
	:	 'time between' TIME TIME;

rule	: 'on'  trigger_block 'do' statement_block;

trigger_block
	: (BLOCK_START trigger+ BLOCK_END) | (trigger);

trigger: (enter_room_trigger | leave_room_trigger | alert_trigger)  NEWLINE;

enter_room_trigger
	:  'enter room ' room_id;

room_id	:	 ((ALPHANUM | DOT | MINUS)+ | REAL | INTEGER) ;

leave_room_trigger
	: 'leave room' room_id;

alert_trigger: 'time' TIME;
statement_block
	:	(BLOCK_START stmt+ BLOCK_END) | stmt;

stmt	:	(if_stmt | parallel_block | ask_stmt | simple_stmt NEWLINE);
simple_stmt
	:	literal | http_stmt | knx_stmt | notify_stmt | wait_stmt |current_room_stmt;
current_room_stmt: 'current room';
wait_stmt: 'wait' INTEGER;
notify_stmt: 'notify ' STRING STRING?;
http_stmt	:	'http' method URL http_content? ('header='http_header)?;
method: 'get'|'put'|'post'|'delete';
http_content: STRING;
http_header: STRING;

literal	:	 STRING | INTEGER | REAL | BOOL;
INTEGER: NUM+;
REAL: INTEGER'.'INTEGER;
BOOL: TRUE | FALSE;
fragment TRUE: 'true'|'True'|'TRUE';
fragment FALSE	: 'false'|'False'|'FALSE';
GE	: 	'>=';
EQ	:	'=';
LE	: 	'<=';
NEQ	:	'!=';
GT	: 	'>';
LT	:	 '<';
fragment COMMENT
    :   '#' ~[\r\n]*? '\r'?'\n';

NEWLINE	:	(COMMENT | ('\r'?'\n'))+ ;
ALPHANUM:	'A'..'Z'|'a'..'z'|NUM;
ID : ALPHANUM+;

parallel_block
	:	'parallel' BLOCK_START (simple_stmt NEWLINE)+ BLOCK_END;
if_stmt	:	'if ' expression statement_block ('else ' statement_block)?;
expression: 	simple_stmt op simple_stmt;
ask_stmt: 'ask' STRING switch_block;
switch_block: BLOCK_START switch_option+ BLOCK_END;
switch_option: STRING ((BLOCK_START stmt+ BLOCK_END) | ':' simple_stmt NEWLINE);
op	:	EQ |GT | LT |GE|LE | NEQ;

knx_stmt: knx_read_stmt | kmx_write_stmt;
knx_read_stmt: 'read ' DTP URL;
kmx_write_stmt: 'write ' DTP URL literal+;


